/*
 * MainFragment.java com.bottlerocketapps.easynfc
 * 
 * Created by brett.mccormick on May 9, 2013. Copyright (c) 2013 Bottle Rocket
 * Apps. All rights reserved.
 */
package com.bottlerocketapps.easynfc;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bottlerocketapps.easynfc.model.DeviceLoader;
import com.bottlerocketapps.easynfc.model.TestDeviceGroup;
import com.bottlerocketapps.http.BaseLoader;
import com.bottlerocketapps.http.BaseLoaderData;
import com.bottlerocketapps.http.LoaderDataI;

/**
 * @author brett.mccormick
 */
public class MainFragment extends Fragment implements LoaderCallbacks<LoaderDataI> {

    private static final String KEY_NAME = "key_name";
    AlertDialog mAlertDialog;
    Button mOut;
    Button mIn;
    Button mNewUser;
    CheckBox mCheckBox;
    
    private static final int LOAD_DEVICE_LIST = 1000;

    private static final String TAG = MainFragment.class.getSimpleName();

    // JSON tags
    private static final String TAG_DEVICEGROUPS = "devicegroups";
    private static final String TAG_NAME = "name";
    private static final String TAG_ID = "id";
    
    private View mRoot;
    String mUser;

    private ListView mDevGrpListView;
    private DeviceGroupListAdapter mDevGrpListAdapter;
    private List<TestDeviceGroup> mDevGrpList = new ArrayList<TestDeviceGroup>();
    
    public static Fragment newInstance(String name) {
        Fragment frag = new MainFragment();
        if (null != name) {
            Bundle b = new Bundle();
            b.putString(KEY_NAME, name);
            frag.setArguments(b);
        }
        return frag;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
//        mDevGrpListAdapter = new DeviceGroupListAdapter(this, mDevGrpList);
        mDevGrpListAdapter = new DeviceGroupListAdapter(this, getTestDeviceGroup());
        if (getArguments() != null) {
            mUser = getArguments().getString(KEY_NAME);
        }
    }

    private List<TestDeviceGroup> getTestDeviceGroup() {
        PreferencesStore preferenceStore = new PreferencesStore(getActivity());
        Map<String, ?> map = preferenceStore.mSharedPreferences.getAll();
        ArrayList<TestDeviceGroup> group;
        try {
            group = new ArrayList<TestDeviceGroup>();
            for (Map.Entry<String, ?> entry : map.entrySet()) {
                TestDeviceGroup td = new TestDeviceGroup(entry
                        .getValue().toString(), entry.getKey());
                group.add(td);
            }
            return group;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRoot = (View) inflater.inflate(R.layout.fragment_main, container, false);
        mDevGrpListView = (ListView) mRoot.findViewById(R.id.list_view);
        
        View headerView = inflater.inflate(R.layout.list_item_header, null);
        TextView txtHdr = (TextView) headerView.findViewById(R.id.txtName);
        txtHdr.setText(getResources().getString(R.string.device_list));
        
        mDevGrpListView.addHeaderView(headerView);
        mDevGrpListView.setAdapter(mDevGrpListAdapter);
        
        mOut = (Button) mRoot.findViewById(R.id.button1);
        mIn = (Button) mRoot.findViewById(R.id.button2);
        mNewUser = (Button) mRoot.findViewById(R.id.button3);
        mCheckBox = (CheckBox) mRoot.findViewById(R.id.checkBox1);
        
        mNewUser.setOnClickListener(mNewUserClickList);
        mIn.setOnClickListener(mDeviceHint);
        mOut.setOnClickListener(mCardHint);
        
        if (!TextUtils.isEmpty(mUser)) {
            getUserInfo(mUser);
        }
        
        return mRoot;
    }
    
    @Override
    public void onStart() {
        super.onStart();
        getLoaderManager().initLoader(LOAD_DEVICE_LIST, null, this);
    }
    
    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (mAlertDialog != null) {
            mAlertDialog.dismiss();
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        mDevGrpListAdapter.updateList(getTestDeviceGroup());
    }

    public static class DeviceGroupListAdapter extends BaseAdapter {

        private WeakReference<Fragment> mFragRef;
        private List<TestDeviceGroup> mDevGrpList;
        
        public DeviceGroupListAdapter(Fragment fragment, List<TestDeviceGroup> devGrpList) {
            mFragRef = new WeakReference<Fragment>(fragment);
            mDevGrpList = devGrpList;
            
        }
        
        @Override
        public int getCount() {
            return mDevGrpList.size();
        }

        @Override
        public Object getItem(int position) {
            if (mDevGrpList == null || mDevGrpList.size() < position) return null;
            
            return mDevGrpList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView txtName = null;
            if (convertView == null) {
                Fragment fragment = mFragRef.get();
                if (fragment != null) {
                    LayoutInflater inflater = LayoutInflater.from(fragment.getActivity());
                    convertView = inflater.inflate(R.layout.list_item_device, null);
                    txtName = (TextView) convertView.findViewById(R.id.txtName);
                    convertView.setTag(R.id.txtName, txtName);
                }
            } else {
                txtName = (TextView) convertView.getTag(R.id.txtName);
            }
            
            TestDeviceGroup devGrp = (TestDeviceGroup) getItem(position);
            if (txtName != null && devGrp != null && !TextUtils.isEmpty(devGrp.getName())) {
                txtName.setText(devGrp.getName());
            }
            return convertView;
        }

        public void updateList(List<TestDeviceGroup> list) {
//            mDevGrpList.addAll(list);
            mDevGrpList = list;
            notifyDataSetChanged();
        }
        
    }

    private void processDeviceGroupsJson(JSONObject jsonObject) {
        if (jsonObject == null) return;
        List<TestDeviceGroup> list = new ArrayList<TestDeviceGroup>();
        JSONObject deviceGrpJson;
        try {
            JSONArray jsonArray = jsonObject.getJSONArray(TAG_DEVICEGROUPS);
            for (int i=0;i<jsonArray.length();i++) {
                deviceGrpJson = jsonArray.getJSONObject(i);
                list.add(new TestDeviceGroup(deviceGrpJson.getString(TAG_ID), deviceGrpJson.getString(TAG_NAME)));
            }
            if (list.size() > 0 && mDevGrpListAdapter.getCount() <= 0) {
                mDevGrpListAdapter.updateList(list);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private OnClickListener mNewUserClickList = new OnClickListener() {
        
        @Override
        public void onClick(View v) {
//            startActivity(new Intent(v.getContext(), NewUserActivity.class));
            startActivityForResult(new Intent(v.getContext(), NewUserActivity.class), 0);
        }
    }; 
    
    private OnClickListener mDeviceHint = new OnClickListener() {
        
        @Override
        public void onClick(View v) {
            showHintDialog(true);
        }
    }; 
    
    private OnClickListener mCardHint = new OnClickListener() {
        
        @Override
        public void onClick(View v) {
            showHintDialog(false);
        }
    }; 
    
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && data != null) {
            String result = data.getStringExtra(NewUserActivity.USERNAME_RESULT);
            if (null != result) {
                getUserInfo(result);
            }
        }
    }
    
    private void getUserInfo(String result) {
        Log.d(TAG,"");
        if (getActivity() != null && !TextUtils.isEmpty(result)) {
            TextView currentUser = (TextView) getActivity().getActionBar().getCustomView().findViewById(R.id.current_user);
            currentUser.setText(result);
        }
    }
    
    private void showHintDialog(boolean device) {
        if (getActivity() == null) return;
        
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (device) {
            builder.setTitle("Tap your Test Device");
        } else {
            builder.setTitle("Tap your NFC Card");
        }
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_hint, null);
        ImageView hintsImageView = (ImageView) view.findViewById(R.id.hints);
        builder.setView(view).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        hintsImageView.setVisibility(View.VISIBLE);

        if (device) {
            hintsImageView.setBackgroundResource(R.drawable.hint_device);
        } else {
            hintsImageView.setBackgroundResource(R.drawable.hint_card);
        }
        
        AnimationDrawable frameAnimation = (AnimationDrawable) hintsImageView.getBackground();
        frameAnimation.start();
        Animation animation;
        if (device) {
            animation = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.device_hint_anim);
        } else {
            animation = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.new_user_hint_anim);
        }
        hintsImageView.startAnimation(animation);
        mAlertDialog = builder.create();
        mAlertDialog.show();
    }

    @Override
    public void onLoadFinished(
            android.support.v4.content.Loader<LoaderDataI> loader,
            LoaderDataI data) {
        
        if (data != null && data.isComplete()) {
            if (data.isSuccess()) {
                JSONObject jsonObject = data.getResultData(JSONObject.class);
                Log.d(TAG, "json = " + jsonObject);
                processDeviceGroupsJson(jsonObject);
            }
        }
    }

    @Override
    public void onLoaderReset(
            android.support.v4.content.Loader<LoaderDataI> arg0) {
        
    }

    @Override
    public android.support.v4.content.Loader<LoaderDataI> onCreateLoader(
            int arg0, Bundle arg1) {
      BaseLoaderData data = new DeviceLoader(getActivity());
      BaseLoader loader = null;
      if (data != null) {
          loader = new BaseLoader(getActivity(), data);
      }
      return loader;
    }

}
