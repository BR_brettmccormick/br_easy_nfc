/*
* NewDeviceActivity.java
* com.bottlerocketapps.easynfc
*
* Created by brett.mccormick on May 10, 2013.
* Copyright (c) 2013 Bottle Rocket Apps. All rights reserved.
*/
package com.bottlerocketapps.easynfc;

import java.io.IOException;
import java.nio.charset.Charset;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.TagLostException;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * @author brett.mccormick
 *
 */
public class NewDeviceActivity extends FragmentActivity {
    private static final String TAG = NewDeviceActivity.class.getName();
    NfcAdapter mNfcAdapter;
    IntentFilter tagDetected;
    PendingIntent pi;
    
    AlertDialog mAlertDialog;
    EditText deviceName;
    EditText deviceNumber;
    EditText deviceVersion;
    Spinner deviceType;
    Button mDone;
    
    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.fragment_add_device);
        
        deviceName = (EditText) findViewById(R.id.device_name);
        deviceNumber= (EditText) findViewById(R.id.device_number);
        deviceVersion = (EditText) findViewById(R.id.device_version);
        deviceType = (Spinner) findViewById(R.id.device_type);
        mDone = (Button) findViewById(R.id.done);
        mDone.setOnClickListener(mDoneClickListener);
        
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        Intent nfcIntent = new Intent(this, NewDeviceActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        pi = PendingIntent.getActivity(this, 0, nfcIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        mNfcAdapter.enableForegroundDispatch(this, pi, new IntentFilter[] {tagDetected}, null);
    }
    
    private OnClickListener mDoneClickListener = new OnClickListener() {
        
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(deviceVersion.getWindowToken(), 0);
            showHintDialog();
        }
    };
    
    private void showHintDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Tap the Test Device");
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_hint, null);
        ImageView hintsImageView = (ImageView) view.findViewById(R.id.hints);
        builder.setView(view).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        hintsImageView.setVisibility(View.VISIBLE);
        hintsImageView.setBackgroundResource(R.drawable.hint_device);
        AnimationDrawable frameAnimation = (AnimationDrawable) hintsImageView.getBackground();
        frameAnimation.start();
        Animation animation = AnimationUtils.loadAnimation(inflater.getContext(), R.anim.device_hint_anim);
        hintsImageView.startAnimation(animation);
        mAlertDialog = builder.create();
        mAlertDialog.show();
    }
    
    private void makeToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
    
    @Override
    protected void onNewIntent(Intent intent) {
     // When an NFC tag is being written, call the write tag function when an intent is
        // received that says the tag is within range of the device and ready to be written to
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        if (writeTag(this, tag)) {
            Intent i = new Intent();
            setResult(0, i);
            PreferencesStore store = new PreferencesStore(this);
            String data = "dev_" + getSpinnerPrefix() + "_" + deviceNumber.getText().toString();
            store.storeDeviceState(data, false);
            finish();
        }
    }
    
    private String getSpinnerPrefix() {
        int text = deviceType.getSelectedItemPosition();
        switch(text){
            case 0: return "ip";
            case 1: return "it";
            case 2: return "ap";
            case 3: return "at";
            case 4: return "op";
        }
        return null;
    }
    
    public boolean writeTag(Context context, Tag tag) {     
        // Record to launch Play Store if app is not installed
        NdefRecord appRecord = NdefRecord.createApplicationRecord(context.getPackageName());
        String data = "dev_" + getSpinnerPrefix() + "_" + deviceNumber.getText().toString();
        if (TextUtils.isEmpty(deviceName.getText().toString())
                || TextUtils.isEmpty(deviceNumber.getText().toString())
                || TextUtils.isEmpty(deviceVersion.getText().toString())) {
            makeToast("Invalid Form Data");
            return false;
        }
        // Record with actual data we care about
        NdefRecord relayRecord = new NdefRecord(NdefRecord.TNF_MIME_MEDIA, new String(data).getBytes(Charset.forName("US-ASCII")), null, data.getBytes());
        
        // Complete NDEF message with both records
        NdefMessage message = new NdefMessage(new NdefRecord[] {relayRecord, appRecord});
        try {
            // If the tag is already formatted, just write the message to it
            Ndef ndef = Ndef.get(tag);
            if(ndef != null) {
                ndef.connect();

                // Make sure the tag is writable
                if(!ndef.isWritable()) {
                    Log.e(TAG,"Tag in Read Only mode");
                    makeToast("Tag in Read Only");
                    return false;
                }

                // Check if there's enough space on the tag for the message
                int size = message.toByteArray().length;
                if(ndef.getMaxSize() < size) {
                    Log.e(TAG,"Not enough space on tag");
                    makeToast("Not enough space on Tag");
                    return false;
                }

                try {
                    // Write the data to the tag
                    ndef.writeNdefMessage(message);
                    makeToast("Tagged device: " + data);
                    Log.d(TAG,"Tag writing success");
                    return true;
                } catch (TagLostException tle) {
                    Log.e(TAG,"Tag Lost", tle);
                    makeToast("Tag Lost");
                    return false;
                } catch (IOException ioe) {
                    Log.e(TAG,"Tag error", ioe);
                    makeToast("Tag IO Exception");
                    return false;
                } catch (FormatException fe) {
                    Log.e(TAG,"Tag error", fe);
                    makeToast("Tag Formatting Error");
                    return false;
                }
            // If the tag is not formatted, format it with the message
            } else {
                NdefFormatable format = NdefFormatable.get(tag);
                if(format != null) {
                    try {
                        format.connect();
                        format.format(message);
                        
                        Log.d(TAG,"Unformatted Tag Write success");
                        makeToast("Super Success");
                        return true;
                    } catch (TagLostException tle) {
                        Log.e(TAG,"Tag error", tle);
                        makeToast("Tag Lost");
                        return false;
                    } catch (IOException ioe) {
                        Log.e(TAG,"Tag error", ioe);
                        makeToast("Tag IO Exception");
                        return false;
                    } catch (FormatException fe) {
                        Log.e(TAG,"Tag error", fe);
                        makeToast("Tag Formatting error");
                        return false;
                    }
                } else {
                    Log.e(TAG,"Tag error no Ndef error");
                    makeToast("Tag error no Ndef");
                    return false;
                }
            }
        } catch(Exception e) {
            Log.e(TAG,"Tag error unknown");
            makeToast("Some other error: " + e.toString());
        }

        return false;
    }
    

}
