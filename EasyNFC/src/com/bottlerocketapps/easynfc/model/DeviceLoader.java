package com.bottlerocketapps.easynfc.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.bottlerocketapps.http.BRHttpRequest;
import com.bottlerocketapps.http.BRHttpResponse;
import com.bottlerocketapps.http.BasePojoLoaderData;
import com.bottlerocketapps.tools.Log;

public class DeviceLoader extends BasePojoLoaderData {

    private JSONObject mData;

    //public static final String DEVLIST_URL = "http://192.168.201.204:7777/inventory";
    private final static String DEVGROUPSLIST_URL = "http://sandbox.bottlerocketapps.com/EasyNFC/devicegroups";
    private static String JSON_SEARCH_RESULTS = "searchResults";
    private static String JSON_SEARCH_TEXT = "searchText";
    private static String JSON_SEARCH_RESULTS_LIST = "list";
    private static String JSON_SEARCH_RESULTS_TOPIC = "topic";
    private static String JSON_SEARCH_RESULTS_DETAILS = "details";

    private static final String TAG = DeviceLoader.class.getSimpleName();
    public DeviceLoader(Context context) {
        String url = DEVGROUPSLIST_URL;
        
        BRHttpRequest request = new BRHttpRequest(url);
        setHttpRequest(request);
    }
    
    @Override
    public <D> D getResultData(Class<D> type) {
        if (type.equals(JSONObject.class) && mData != null) {
            return type.cast(mData);
        }
        return null;
    }

    @Override
    public boolean processResponse(Context context) {
        BRHttpResponse httpResponse = getHttpResponse();
        mData = null;
        if (httpResponse == null) return false;
        String responseData = httpResponse.getResponseData();
        Log.d(TAG, "responseJSON = " + responseData);
        
        try {
            mData = new JSONObject(responseData);
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
            return false;
        }
        
        return true;
    }

    @Override
    public boolean isCacheOk(Context context) {
        return false;
    }

    @Override
    public boolean loadCachedResultData(Context context) {
        return false;
    }

    @Override
    public void postDeliveryCleanup(Context context) {
        
    }

}
