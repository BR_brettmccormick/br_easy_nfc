package com.bottlerocketapps.easynfc.model;

public class TestDevice {
    private String name;
    private String id;
    
    public TestDevice(String name, String id) {
        this.name = name;
        this.id = id;
    }
    
    public final String getName() {
        return name;
    }
    public final String getId() {
        return id;
    }


}
