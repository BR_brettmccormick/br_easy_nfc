package com.bottlerocketapps.easynfc.model;

public class TestDeviceGroup {
    private String id;
    private String name;
    
    public TestDeviceGroup(String id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public final String getId() {
        return id;
    }
    public final String getName() {
        return name;
    }
}
