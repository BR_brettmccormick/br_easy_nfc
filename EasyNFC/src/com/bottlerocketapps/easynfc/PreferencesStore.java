/*
* PreferencesStore.java
* com.bottlerocketapps.easynfc
*
* Created by brett.mccormick on May 10, 2013.
* Copyright (c) 2013 Bottle Rocket Apps. All rights reserved.
*/
package com.bottlerocketapps.easynfc;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author brett.mccormick
 *
 */
public class PreferencesStore {

    
    SharedPreferences mSharedPreferences;
    Context mContext;
    
    public PreferencesStore (Context c) {
        mContext = c;
        mSharedPreferences = mContext.getSharedPreferences("Prefs", 0);
        
    }
    
    public boolean deviceIsCheckedOut(String device_key) {
        return mSharedPreferences.getBoolean(device_key, false);
    }
    
    public void storeDeviceState(String device, boolean checkedOutState) {
        mSharedPreferences.edit().putBoolean(device, checkedOutState).commit();
    }
}
