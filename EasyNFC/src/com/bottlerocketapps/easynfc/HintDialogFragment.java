/*
* HintDialogFragment.java
* com.bottlerocketapps.easynfc
*
* Created by brett.mccormick on May 9, 2013.
* Copyright (c) 2013 Bottle Rocket Apps. All rights reserved.
*/
package com.bottlerocketapps.easynfc;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * @author brett.mccormick
 *
 */
public class HintDialogFragment extends DialogFragment{
    public static final int TYPE_CARD = 4;
    public static final int TYPE_DEVICE = 5;
    public static final String KEY_TYPE = "type";
    private int mType;
    
    ImageView mHintsImageView;
    View mRoot;
    
    public static HintDialogFragment newInstance(int type) {
        HintDialogFragment frag = new HintDialogFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_TYPE, type);
        frag.setArguments(args);
        return frag;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mType = getArguments().getInt(KEY_TYPE);
        int style = DialogFragment.STYLE_NORMAL;
        setStyle(style, R.style.Theme_Easynfc);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        mRoot = inflater.inflate(R.layout.fragment_hint, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
}
