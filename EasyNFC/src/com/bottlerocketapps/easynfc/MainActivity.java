package com.bottlerocketapps.easynfc;

import java.util.List;

import org.ndeftools.Message;
import org.ndeftools.Record;
import org.ndeftools.externaltype.AndroidApplicationRecord;

import android.app.ActionBar;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity {

    private static final String TAG = MainActivity.class.getName();
    public static final String FRAG_MAIN = "MainFrag";
    private NfcAdapter nfcAdapter;
    PendingIntent nfcPendingIntent;
    PreferencesStore mPreferences;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generic_fragment);
        getActionBar().setHomeButtonEnabled(false);
        getActionBar().setDisplayHomeAsUpEnabled(false);
//        getActionBar().setdisplay
        ActionBar ab = getActionBar();
        if (null != ab) {
            ab.setDisplayShowCustomEnabled(true);
            View cView = getLayoutInflater().inflate(R.layout.actionbar, null);
            ab.setCustomView(cView);
        }
        if (savedInstanceState == null) {
            Fragment mainFragment = MainFragment.newInstance(null);
            FragmentTransaction t = getSupportFragmentManager().beginTransaction().replace(R.id.main_root, mainFragment,FRAG_MAIN);
            t.commit();

        }
        mPreferences = new PreferencesStore(this);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, NewDeviceActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED); // filter for tags
        IntentFilter[] writeTagFilters = new IntentFilter[] {tagDetected};
        nfcAdapter.enableForegroundDispatch(this, nfcPendingIntent, writeTagFilters, null);
    }
    
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }
    
    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        
        Parcelable[] messages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (messages != null) { 
            Log.d(TAG, "Found " + messages.length + " NDEF messages");

            // parse to records
            for (int i = 0; i < messages.length; i++) {
                try {
                    List<Record> records = new Message((NdefMessage)messages[i]);
                    
                    Log.d(TAG, "Found " + records.size() + " records in message " + i);

                    for(int k = 0; k < records.size(); k++) {
                        Log.d(TAG, " Record #" + k + " is of class " + records.get(k).getClass().getSimpleName());
                        Record record = records.get(k);
                        byte[] payload = record.getNdefRecord().getPayload();
                        //It is really late. sometimes the nth record may contain the package name in the payload
                        String ss = new String(payload, "UTF-8");
                        if (!ss.contains("com.bottlerocketapps")) {
                            successfulScan(ss);
                        }
                        if(record instanceof AndroidApplicationRecord) {
                            AndroidApplicationRecord aar = (AndroidApplicationRecord)record;
                            Log.d(TAG, "Package is " + aar.getPackageName());
                        }
                    }
                } catch (Exception e) {
                    makeToast("OH NO! " + e.toString());
                    Log.e(TAG, "Problem parsing message", e);
                }

            }
        }

    }
    
    private void successfulScan(String content) {
        if (content.startsWith("dev_")) {
            TextView tv = (TextView) getActionBar().getCustomView().findViewById(R.id.current_user); 
            if (TextUtils.equals((tv.getText().toString()), "Current Username")) {
                makeToast("Swipe your NFC Tag and try again");
            } else {
                if (mPreferences.deviceIsCheckedOut(content)) {
                    makeToast("Checked in " + content);
                    mPreferences.storeDeviceState(content, false);
                } else {
                    makeToast("You checked out the device!");
                    mPreferences.storeDeviceState(content, true);
                }
            }
        } else if (content.split("\\.").length == 2) {
            makeToast("Welcome, " + content);
            Fragment mainFragment = MainFragment.newInstance(content);
            FragmentTransaction t = getSupportFragmentManager().beginTransaction().replace(R.id.main_root, mainFragment,FRAG_MAIN);
            t.commit();
        } else {
            makeToast("Invalid Tag");
        }
    }
    
    private void makeToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
    
}
