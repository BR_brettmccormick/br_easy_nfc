package com.bottlerocketapps.http.ssl;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.http.conn.ssl.SSLSocketFactory;

import android.content.Context;

import com.bottlerocketapps.tools.Log;

/**
 * Wrapper for Apache SSLContext that will load additional certificates from a Bouncy Castle keystore.<br/> 
 * Requests will be processed with both built-in and additional certificates.
 * <br/><br/>
 * http://stackoverflow.com/a/6378872/581839
 * 
 * @author adam.newman
 */
public class CustomKeyStoreSSLSocketFactory extends SSLSocketFactory {
    private static final String TAG = CustomKeyStoreSSLSocketFactory.class.getSimpleName();
    
    protected SSLContext sslContext = SSLContext.getInstance("TLS");

    /**
     * Create a new instance of the CustomKeyStoreSSLSocketFactory by loading the provided raw keystore resource file using
     * the provided password string resource id. 
     * 
     * @param context                       Context to use when opening resources.
     * @param keystoreRawResourceId         Resource id of the keystore file placed in the res/raw folder.
     * @param keystorePassStringResourceId  Resource id of the string resource containing the password for the keystore file. 
     * @return
     */
    public static SSLSocketFactory createInstance(Context context, int keystoreRawResourceId, int keystorePassStringResourceId) {
        try {
            final KeyStore ks = KeyStore.getInstance("BKS");

            // the bks file we generated which is stored in the raw resource identified by keystoreRawResourceId.
            final InputStream in = context.getResources().openRawResource(keystoreRawResourceId);  
            try {
                // don't forget to put the password used above in strings.xml/mystore_password
                ks.load(in, context.getString(keystorePassStringResourceId).toCharArray());
            } catch (Exception e) {
                Log.w(TAG, "Failed to load keystore.", e);
            } finally {
                in.close();
            }

            return new CustomKeyStoreSSLSocketFactory(ks);

        } catch( Exception e ) {
            throw new RuntimeException(e);
        }
    }
    
    public CustomKeyStoreSSLSocketFactory(KeyStore keyStore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
        super(null, null, null, null, null, null);
        sslContext.init(null, new TrustManager[]{new AdditionalKeyStoresTrustManager(keyStore)}, null);
    }

    @Override
    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException {
        return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
    }

    @Override
    public Socket createSocket() throws IOException {
        return sslContext.getSocketFactory().createSocket();
    }
    
}
