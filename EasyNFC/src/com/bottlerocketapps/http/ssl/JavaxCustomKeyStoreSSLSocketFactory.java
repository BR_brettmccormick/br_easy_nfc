package com.bottlerocketapps.http.ssl;

import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import android.content.Context;

import com.bottlerocketapps.tools.Log;

/**
 * Instantiates a JavaX SSLSocketFactory with additional certificates from a Bouncy Castle keystore. <br/> 
 * Requests will be processed with both built-in and additional certificates.
 * 
 * @author adam.newman
 */
public class JavaxCustomKeyStoreSSLSocketFactory {

    public static final String TAG = JavaxCustomKeyStoreSSLSocketFactory.class.getSimpleName();    

    /**
     * Load the keystore using the provided resources and return an SSLSocketFactory. 
     *
     * @param context                       Context to use when opening resources.
     * @param handshakeTimeout              SSL handshake timeout
     * @param keystoreRawResourceId         Resource id of the keystore file placed in the res/raw folder.
     * @param keystorePassStringResourceId  Resource id of the string resource containing the password for the keystore file.
     * @return
     */
    public static SSLSocketFactory createInstance(Context context, int handshakeTimeout, int keystoreRawResourceId, int keystorePassStringResourceId) {
        try {
            final KeyStore ks = KeyStore.getInstance("BKS");

            // the bks file we generated which is stored in the raw resource identified by keystoreRawResourceId.
            final InputStream in = context.getResources().openRawResource(keystoreRawResourceId);  
            try {
                // don't forget to put the password used above in strings.xml/mystore_password
                ks.load(in, context.getString(keystorePassStringResourceId).toCharArray());
            } catch (Exception e) {
                Log.w(TAG, "Failed to load keystore.", e);
            } finally {
                in.close();
            }

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{new AdditionalKeyStoresTrustManager(ks)}, null);
            return sslContext.getSocketFactory();
            
        } catch( Exception e ) {
            throw new RuntimeException(e);
        }
    }
    
}
