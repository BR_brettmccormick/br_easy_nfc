package com.bottlerocketapps.http;


public abstract class BaseLoaderData implements LoaderDataI, Cloneable {

    private LoaderStatus status = LoaderStatus.INITIALIZING;
    private boolean success;
    private BRHttpRequest request;
    private BRHttpResponse response;
    private BRHttpProgress progress;
    
    @Override
    public BRHttpRequest getHttpRequest() {
        return request;
    }

    @Override
    public void setHttpRequest(BRHttpRequest request) {
        this.request = request;
    }

    @Override
    public BRHttpResponse getHttpResponse() {
        return response;
    }

    @Override
    public void setHttpResponse(BRHttpResponse response) {
        this.response = response;
    }

    @Override
    public BRHttpProgress getHttpProgress() {
        return progress;
    }

    @Override
    public void setHttpProgress(BRHttpProgress progress) {
        this.progress = progress;
    }

    @Override
    public boolean isSuccess() {
        return success;
    }

    @Override
    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public boolean isComplete() {
        return status == LoaderStatus.COMPLETE;
    }

    @Override
    public LoaderStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(LoaderStatus status) {
        this.status = status;
    }
        
    @Override
    public void reset() {
        setStatus(LoaderStatus.INITIALIZING);
        setSuccess(false);
        setHttpResponse(null);
        setHttpProgress(null);
    }
    
    @Override
    public LoaderDataI clone() {
        try {
            return (LoaderDataI) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
