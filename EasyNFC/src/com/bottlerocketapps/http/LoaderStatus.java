package com.bottlerocketapps.http;

public enum LoaderStatus {
    INITIALIZING,
    PREQUERY_COMPLETE,
    TRANSFERRING,
    PROCESSING,
    COMPLETE
}
