package com.bottlerocketapps.http;

import android.content.Context;
import android.net.Uri;

/**
 * Same as BaseLoader, but ignores cursor related methods.
 * @author adam.newman
 *
 */
public abstract class BasePojoLoaderData extends BaseLoaderData {
    
    //Override abstract cursor methods and do nothing by default.
    
    @Override
    public Uri getContentUri() {
        return null;
    }
    
    @Override
    public boolean query(Context context) {
        return true;
    }
    
    @Override
    public boolean shouldPreQuery() {
        return false;
    }    
}
