package com.bottlerocketapps.http;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.database.ContentObserver;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.Loader;

import com.bottlerocketapps.http.HttpClientService.HttpClientListener;
import com.bottlerocketapps.http.HttpClientService.RequestResult;
import com.bottlerocketapps.tools.Log;

/**
 * Base loader is a general purpose loader for data from web services. 
 * <p>
 * The loader will perform the following steps:
 * <ol>
 * <li>Check if loader has already been completed successfully and deliver that result immediately if so.</li>
 * <li>If the LoaderData object implements local caching and reports true in isCacheOk, a background task will call
 * loadCachedResultData then query() and deliver the result.</li>
 * <li>If the LoaderData object returns true from shouldPrequery(), a background task will run the query and deliver 
 * the result with a status of PREQUERY_COMPLETE then continue the download/parse process.</li>
 * <li>The HttpClientService is started with the BRHttpRequest object provided by the LoaderData object. It it has 
 * a cache-hit, it will return immediately without performing the request and start processResponse with cached data including
 * the entire original response with headers and other metadata. Once processing is complete a result will be delivered.</li>
 * <li>If an immediate cache-hit does not occur, the HttpClientService will be started. A result will be delivered with the status
 * of TRANSFERRING only if the PREQUERY_COMPLETE response was skipped. The service will perform the request and call back with the response
 * at which point the processResponse task will occur. Once processing is complete a final result will be delivered.</li>
 * </ol>
 * </p>
 * 
 * @author adam.newman
 *
 */
public class BaseLoader extends Loader<LoaderDataI> implements HttpClientListener {
    
    public static final String TAG = BaseLoader.class.getSimpleName();

    private LoaderDataI loaderData;
    private ProcessInBackgroundTask mProcessInBackgroundTask;
    private ContentObserver mContentObserver;
    private Handler mHandler;
    private boolean mHasDeliveredOnce = false;

    public BaseLoader(Context context, LoaderDataI loaderData) {
        super(context);
        this.loaderData = loaderData;
        Log.v(TAG, "BaseLoader Constructor");
    }
    
    @Override
    protected void onAbandon() {
        /**
         * Subclasses implement this to take care of being abandoned. This is an optional intermediate state prior to onReset() -- it means that
         * the client is no longer interested in any new data from the loader, so the loader must not report any further updates. 
         * However, the loader must keep its last reported data valid until the final onReset() happens. You can retrieve the current abandoned state with isAbandoned().
         */
        Log.v(TAG, "onAbandon()");
        //TODO: Abandon things? 
        super.onAbandon();
    }

    @Override
    protected void onForceLoad() {
        //TODO: Abandon any in-progress load and start a new load.
        Log.v(TAG, "onForceLoad()");        
        super.onForceLoad();
    }

    @Override
    protected void onReset() {
        if (!isReset()) {
            Log.v(TAG, "onReset()");
            //Discard all data and reset to unstarted state
            if (loaderData != null) {
                if (!loaderData.isComplete()) {
                    HttpClientService.cancelHttpRequest(getContext(), loaderData.getHttpRequest());
                }
                loaderData.reset();            
            }
            if (mContentObserver != null) {
                unregisterObserver();
            }  
        }
        super.onReset();
    }

    @Override
    protected void onStartLoading() {
        Log.v(TAG, "onStartLoading()");
        if (loaderData == null) {
            Log.e(TAG, "LoaderData was null when onStartLoading was called");
            return;
        }
        
        if (loaderData.isSuccess() && loaderData.isComplete()) {
            Log.v(TAG, "Result is already ready locally");
            deliverResult(loaderData);
            return;
        }
        
        if (loaderData.isCacheOk(getContext())) {
            Log.v(TAG, "LoaderDataI indicates cache ok, processing in background.");
            processInBackground();
            return;
        }
        
        if (loaderData.shouldPreQuery()) {
            Log.v(TAG, "LoaderDataI indicates that we should pre-query and return result before downloading.");
            prequery();
        }
        
        RequestResult result = HttpClientService.performHttpRequest(getContext(), this, loaderData.getHttpRequest());
        if (result == RequestResult.TRANSFER_REQUIRED) {
            Log.v(TAG, "Transfer was started.");
            loaderData.setStatus(LoaderStatus.TRANSFERRING);
            if (!loaderData.shouldPreQuery()) {
                //Don't send back-to-back deliveries if prequery is enabled.
                deliverResult(loaderData);
            }
        }
    }

    @Override
    protected void onStopLoading() {
        //TODO: Evaluate aborting the download at this point. 
        Log.v(TAG, "onStopLoading()");
        super.onStopLoading();
    }

    @Override
    public void deliverResult(LoaderDataI data) {
        if (isStarted()) {
            LoaderDataI newLoaderData = data;
            if (mHasDeliveredOnce) {
                //Loader manager is checking pointer equality and will not re-deliver if equal, so we have to clone on subsequent deliveries.
                newLoaderData = data.clone();
            }
            loaderData = newLoaderData;
            super.deliverResult(loaderData);
            mHasDeliveredOnce = true;
            loaderData.postDeliveryCleanup(getContext());
        }
    }
    
    /*****LoaderDataI operations*****/
    
    public void setLoaderData(LoaderDataI loaderData) {
        this.loaderData = loaderData;
    }
    
    public LoaderDataI getLoaderData() {
        return loaderData;
    }
    
    private void processInBackground() {
        Log.v(TAG, "processInBackground");
        loaderData.setStatus(LoaderStatus.PROCESSING);
        if (mProcessInBackgroundTask != null) {
            mProcessInBackgroundTask.cancel(false);
        }
        mProcessInBackgroundTask = new ProcessInBackgroundTask();
        mProcessInBackgroundTask.execute(loaderData);
    }
    
    private class ProcessInBackgroundTask extends AsyncTask<LoaderDataI, Long, Boolean> {

        @Override
        protected Boolean doInBackground(LoaderDataI... params) {
            Log.v(TAG, "ProcessInBackgroundTask - doInBackground()");
            LoaderDataI loaderData = params[0];
            boolean result = false;
            if (loaderData.isCacheOk(getContext())) {
                result = loaderData.loadCachedResultData(getContext());
                result = result && loaderData.query(getContext());
            } else {
                result = loaderData.processResponse(getContext());
                result = result && loaderData.query(getContext());                
            }   
            return result;
        }
        
        @Override
        protected void onPostExecute(Boolean result) {
            Log.v(TAG, "ProcessInBackgroundTask - onPostExecute()");
            loaderData.setSuccess(result);
            loaderData.setStatus(LoaderStatus.COMPLETE);
            deliverResult(loaderData);
            //Register the content observer after intial delivery to avoid sending data twice after insert.
            registerObserver();
        }
    }
        
    private void reportFailure() {
        Log.v(TAG, "reportFailure()");
        loaderData.setStatus(LoaderStatus.COMPLETE);
        loaderData.setSuccess(false);
        deliverResult(loaderData);
    }
    
    private void reportProgress(BRHttpProgress progress) {
        loaderData.setHttpProgress(progress);
        deliverResult(loaderData);
    }
    
    /*****HTTP Service Listener Interface*****/
    
    @Override
    public void onHttpClientResult(int requestId, boolean success, BRHttpResponse response) {
        Log.v(TAG, "onHttpClientResult()");
        loaderData.setHttpResponse(response);        
        if (success) {
            processInBackground();
        } else {
            reportFailure();
        }
    }
    
    @Override
    public void onHttpClientProgress(int requestId, BRHttpProgress progress) {
        Log.v(TAG, "onHttpClientProgress()");
        reportProgress(progress);
    }
    
    /******PRE QUERY TASK********/
    
    private void prequery() {
        PreQueryTask pqt = new PreQueryTask();
        pqt.execute(loaderData);
    }
    
    private class PreQueryTask extends AsyncTask<LoaderDataI, Long, Boolean> {

        @Override
        protected Boolean doInBackground(LoaderDataI... params) {
            Log.v(TAG, "PreQueryTask - doInBackground()");
            LoaderDataI loaderData = params[0];
            return loaderData.query(getContext());
        }
        
        @Override
        protected void onPostExecute(Boolean result) {
            Log.v(TAG, "PreQueryTask - onPostExecute()");
            loaderData.setStatus(LoaderStatus.PREQUERY_COMPLETE);
            loaderData.setSuccess(result);
            deliverResult(loaderData);
        }
        
    }
    
    /******CONTENT OBSERVER******/
    
    private Handler getHandler() {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        return mHandler;
    }
    
    public void registerObserver() {
        if (loaderData != null && loaderData.getContentUri() != null && mContentObserver == null) {
            mContentObserver = new CursorContentObserver(getHandler(), this);
            getContext().getContentResolver().registerContentObserver(loaderData.getContentUri(), false, mContentObserver);
        }
    }
    
    public void unregisterObserver() {
        if (mContentObserver != null) { 
            getContext().getContentResolver().unregisterContentObserver(mContentObserver);
            mContentObserver = null;
        }        
    }
    
    private static class CursorContentObserver extends ContentObserver {
        WeakReference<BaseLoader> mLoaderRef;

        public CursorContentObserver(Handler handler, BaseLoader loader) {
            super(handler);
            mLoaderRef = new WeakReference<BaseLoader>(loader);
        }
        
        @Override
        public void onChange(boolean selfChange) {
            BaseLoader loader = mLoaderRef.get();
            if (loader != null) {
                loader.onContentObserverChange();
            }
        }
    }
    
    public void onContentObserverChange() {
        Log.v(TAG, "onContentObserverChange()");
        ContentObserverRequeryTask corTask = new ContentObserverRequeryTask();
        corTask.execute(loaderData);
    }
    
    private class ContentObserverRequeryTask extends AsyncTask<LoaderDataI, Long, Boolean> {

        @Override
        protected Boolean doInBackground(LoaderDataI... params) {
            Log.v(TAG, "ContentObserverRequeryTask - doInBackground()");
            LoaderDataI loaderData = params[0];
            return loaderData.query(getContext());
        }
        
        @Override
        protected void onPostExecute(Boolean result) {
            Log.v(TAG, "PreQueryTask - onPostExecute()");
            if (result) {
                loaderData.setSuccess(result);
                deliverResult(loaderData);
            }
        }
    }
    
}
