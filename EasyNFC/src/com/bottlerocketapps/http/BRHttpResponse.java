package com.bottlerocketapps.http;

import java.io.Serializable;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.protocol.HTTP;

public class BRHttpResponse implements Serializable {
    private static final long serialVersionUID = 7288943728956595974L;
    
    private String responseData;
    private HttpResponseStatus status;
    private int httpStatus;
    private Map<String, ArrayList<String>> responseHeaders;
    private String key;
    
    public BRHttpResponse(String key) {
        status = HttpResponseStatus.INTERNAL_ERROR;
        this.key = key;        
    }

    public String getKey() {
        return key;
    }
    
    public String getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    public HttpResponseStatus getStatus() {
        return status;
    }

    public void setStatus(HttpResponseStatus status) {
        this.status = status;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }
    
    public boolean isHttpStatusOk() {
        return httpStatus == HttpURLConnection.HTTP_OK || httpStatus == HttpURLConnection.HTTP_CREATED || httpStatus == HttpURLConnection.HTTP_ACCEPTED || httpStatus == HttpURLConnection.HTTP_NO_CONTENT;
    }
    
    public long getContentLength() {
        if (responseHeaders != null) {
            if (responseHeaders.containsKey(HTTP.CONTENT_LEN)) {
                try {                    
                    return Long.parseLong(responseHeaders.get(HTTP.CONTENT_LEN).get(0));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }

    public Map<String, ArrayList<String>> getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(Map<String, ArrayList<String>> responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public void storeResponseHeaders(Header[] headers) {        
        if (headers != null && headers.length > 0) {
            if (responseHeaders == null) {
                responseHeaders = new HashMap<String, ArrayList<String>>();
            }
            for (Header header: headers) {
                if (responseHeaders.containsKey(header.getName())) {
                    responseHeaders.get(header.getName()).add(header.getValue());
                } else {
                    ArrayList<String> list = new ArrayList<String>();
                    list.add(header.getValue());
                    responseHeaders.put(header.getName(), list);
                }
            }
        }
    }

    public void storeResponseHeaders(Map<String, List<String>> headerFields) {
        if (responseHeaders == null) {
            responseHeaders = new HashMap<String, ArrayList<String>>();
        }
        for (String key: headerFields.keySet()) {
            responseHeaders.put(key, new ArrayList<String>(headerFields.get(key)));
        }
    }
}
