package com.bottlerocketapps.http;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Build;
import android.util.Log;

import com.bottlerocketapps.tools.BRSerializer;
import com.bottlerocketapps.tools.NetworkTools.BRMultipartPostDataParameter;

public class BRHttpRequest implements Serializable {
    private static final long serialVersionUID = 1301407413572861796L;

    private static final String TAG = BRHttpRequest.class.getSimpleName();    
    
    public static final long DEFAULT_CACHE_LIFETIME = 5 * 60 * 1000;
    public static final int DEFAULT_TIMEOUT = 30 * 1000;
    public static final int DEFAULT_MAX_ATTEMPTS = 1;
    public static final int DEFAULT_RETRY_DELAY = 2 * 1000;
    public static final boolean DEFAULT_FORCE_DOWNLOAD = false;
    public static final boolean DEFAULT_DO_NOT_CACHE = false;
    public static final boolean DEFAULT_DO_NOT_RETURN_DATA = false;
    public static final BRHttpMethod DEFAULT_METHOD = BRHttpMethod.AUTO;
    public static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Linux; Android " + Build.VERSION.RELEASE + "; " + Build.MODEL + " Build/" + Build.DISPLAY +")";
    public static final boolean DEFAULT_CHUNKED_UPLOAD_DISABLED = false;
    public static final int DEFAULT_PRIORITY = 0;
    
    //setMethod will set forceDownload to true if the method is POST per RFC 2616
    public static final boolean FORCE_DOWNLOAD_POST_METHOD = true;
    
    private int requestId;
    private String url;
    private long cacheLifetime;
    private int timeout;
    private boolean forceDownload;
    private int maxAttempts;
    private int retryDelay;
    private boolean doNotCache;
    private boolean doNotReturnData;
    private Map<String, String> headers;
    private Map<String, String> queryParameters;
    private Map<String, String> postParameters;
    private List<BRMultipartPostDataParameter> multipartPostDataParameters; 
    private BRHttpMethod method;
    private String userAgent;
    private String key;
    private String downloadFilePath;
    private boolean chunkedUploadDisabled;
    private int priority;
    private int customKeystoreRawResourceId;
    private int customKeystorePasswordResourceId;
    
    public BRHttpRequest() {
        this(null);
    }
    
    public BRHttpRequest(String url) {        
        setCacheLifetime(DEFAULT_CACHE_LIFETIME);
        setTimeout(DEFAULT_TIMEOUT);
        setMaxAttempts(DEFAULT_MAX_ATTEMPTS);
        setRetryDelay(DEFAULT_RETRY_DELAY);
        setForceDownload(DEFAULT_FORCE_DOWNLOAD);
        setDoNotCache(DEFAULT_DO_NOT_CACHE);
        setDoNotReturnData(DEFAULT_DO_NOT_RETURN_DATA);
        setMethod(DEFAULT_METHOD);
        setUserAgent(DEFAULT_USER_AGENT);
        setChunkedUploadDisabled(DEFAULT_CHUNKED_UPLOAD_DISABLED);
        setPriority(DEFAULT_PRIORITY);
        setUrl(url);
        setCustomKeystoreRawResourceId(0);
        setCustomKeystorePasswordResourceId(0);        
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }
    
    public long getCacheLifetime() {
        return cacheLifetime;
    }

    public void setCacheLifetime(long cacheLifetime) {
        this.cacheLifetime = cacheLifetime;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public boolean isForceDownload() {
        return forceDownload;
    }

    public void setForceDownload(boolean forceDownload) {
        this.forceDownload = forceDownload;
    }

    public int getMaxAttempts() {
        return maxAttempts;
    }

    public void setMaxAttempts(int maxAttempts) {
        this.maxAttempts = maxAttempts;
    }

    public int getRetryDelay() {
        return retryDelay;
    }

    public void setRetryDelay(int retryDelay) {
        this.retryDelay = retryDelay;
    }

    public boolean isDoNotCache() {
        return doNotCache;
    }

    public void setDoNotCache(boolean doNotCache) {
        this.doNotCache = doNotCache;
    }

    public boolean isDoNotReturnData() {
        return doNotReturnData;
    }

    public void setDoNotReturnData(boolean doNotReturnData) {
        this.doNotReturnData = doNotReturnData;
    }

    public BRHttpMethod getMethod() {
        return method;
    }

    public void setMethod(BRHttpMethod method) {
        if (FORCE_DOWNLOAD_POST_METHOD && method == BRHttpMethod.POST) {
            Log.v(TAG, "Setting forceDownload=true for POST.");
            setForceDownload(true);
        } else if (method == BRHttpMethod.DELETE || method == BRHttpMethod.PUT){
            Log.v(TAG, "Setting forceDownload=true for DELETE or PUT.");
            setForceDownload(true);
        }
        
        if (method == BRHttpMethod.DELETE || method == BRHttpMethod.PUT) {
            Log.v(TAG, "Will not cache DELETE or PUT");
            setDoNotCache(true);
        }
        
        this.method = method;
    }

    public Map<String, String> getQueryParameters() {
        return queryParameters;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public void putHeader(String key, String value) { 
        if (headers == null) {
            headers = new HashMap<String, String>();
        }
        headers.put(key, value);
    }

    public void setQueryParameters(Map<String, String> queryParameters) {
        this.queryParameters = queryParameters;
    }

    public void putQueryParameter(String key, String value) {
        if (queryParameters == null) {
            queryParameters = new HashMap<String, String>();
        }
        queryParameters.put(key, value);
    }

    public Map<String, String> getPostParameters() {
        return postParameters;
    }

    public void setPostParameters(Map<String, String> postParameters) {
        this.postParameters = postParameters;
        updateAutoMethod();
    }

    public void putPostParameter(String key, String value) {
        if (postParameters == null) {
            postParameters = new HashMap<String, String>();
        }
        postParameters.put(key, value);
        updateAutoMethod();
    }

    public List<BRMultipartPostDataParameter> getMultipartPostParameters() {
        return multipartPostDataParameters;
    }

    public void setMultipartPostDataParameters(List<BRMultipartPostDataParameter> multipartPostDataParameters) {
        this.multipartPostDataParameters = multipartPostDataParameters;
        updateAutoMethod();
    }

    public void addMultipartPostDataParameter(BRMultipartPostDataParameter parameter) {
        if (multipartPostDataParameters == null) {
            multipartPostDataParameters = new ArrayList<BRMultipartPostDataParameter>();
        }
        multipartPostDataParameters.add(parameter);
        updateAutoMethod();
    }

    /**
     * If method is AUTO and POST parameters are present, assume it is a POST. Prior or later calls to setMethod() will
     * override or preempt this automatic setting. 
     */
    private void updateAutoMethod() {
        if (getMethod() == BRHttpMethod.AUTO) {
            if (multipartPostDataParameters != null || postParameters != null) {
                setMethod(BRHttpMethod.POST);
            }
        }
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getDownloadFilePath() {
        return downloadFilePath;
    }

    public void setDownloadFilePath(String downloadFilePath) {
        setForceDownload(true);
        setDoNotCache(true);
        this.downloadFilePath = downloadFilePath;
    }
    
    /**
     * Retrieve or generate key for this request.
     * @note DO NOT CALL UNTIL ALL URL/QUERY/POST/MULTIPART/METHOD PARAMETERS HAVE BEEN SET.
     * @return MD5 Digest based on the method and url combined with any query, post, and multipart post parameters.
     */
    public String getKey() {
        if (key == null) {
            key = getKeyFromParameters(url, queryParameters, postParameters, multipartPostDataParameters, method);
        }
        return key;
    }

    /*******Key Generation*******/    
    /**
     * Generate a hex String of an MD5 hash from the provided data.
     * @param data
     * @return 
     */
    public static String getKeyFromData(byte[] data) {
        MessageDigest digest;
        try {
            digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(data);
            return byteArrayToHexString(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            //Famous last words: this shouldn't happen
            Log.e(TAG, "MD5 algorithm is missing!");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Convert a byte array into a hex string
     * @param data
     * @return 
     */
    public static String byteArrayToHexString(byte[] data) {
        StringBuilder hex = new StringBuilder(data.length * 2);
        for (byte b: data) {
            hex.append(String.format("%02x", b));
        }
        return hex.toString();
    }

    /**
     * Combine two byte arrays into one and return it.
     * @param a
     * @param b
     * @return
     */
    public static byte[] concatByteArrays(byte[] a, byte[] b) {
        byte[] c= new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);

        return c;
    }

    /**
     * Generate an MD5 hash of url and method plus query and/or post parameters
     * @param uri
     * @param queryParameters
     * @param postParameters
     * @param multipartPostDataParameters
     * @param method
     * @return
     */
    public static String getKeyFromParameters(String url, Map<String, String> queryParameters, Map<String, String> postParameters, List<BRMultipartPostDataParameter> multipartPostDataParameters, BRHttpMethod method) {

        //Do not use the bytes[] of the file to be uploaded. That could cause an unacceptable amount of memory allocation.
        String multipartPostDataParametersString = null;        
        if (multipartPostDataParameters != null) {
            StringBuilder sb = new StringBuilder();
            for (BRMultipartPostDataParameter parameter: multipartPostDataParameters) {
                sb.append(parameter.fieldName + parameter.fileName + parameter.contentType);
            }
        }

        return getKeyFromParameters(url, BRSerializer.serializeToByteArray(queryParameters), BRSerializer.serializeToByteArray(postParameters), multipartPostDataParametersString, method);
    }

    /**
     * Generate an MD5 hash of url and method plus query and/or post parameters
     * @param url
     * @param queryParametersRaw
     * @param postParametersRaw
     * @return MD5 hash of the raw bytes of the parameters and url
     */
    public static String getKeyFromParameters (String url, byte[] queryParametersRaw, byte[] postParametersRaw, String multipartPostDataParametersString, BRHttpMethod method) {
        byte[] methodBytes = Integer.toString(method.hashCode()).getBytes();

        if (queryParametersRaw == null) {
            queryParametersRaw = new byte[] {0};            
        }

        if (postParametersRaw == null) {
            postParametersRaw = new byte[] {0};
        }

        byte[] multipartPostDataParametersRaw;

        if (multipartPostDataParametersString == null) {
            multipartPostDataParametersRaw = new byte[] {0};            
        } else {
            multipartPostDataParametersRaw = multipartPostDataParametersString.getBytes();
        }

        //Combine this all into a single byte array and compute the hash.
        return getKeyFromData(concatByteArrays(methodBytes,
                concatByteArrays(url.getBytes(),
                        concatByteArrays(postParametersRaw,
                                concatByteArrays(queryParametersRaw, multipartPostDataParametersRaw)))));        
    }

    public boolean isChunkedUploadDisabled() {
        return chunkedUploadDisabled;
    }

    /**
     * <p>Disable chunked streaming mode when uploading files. This setting only has an effect when multipart posts are being performed.</p>
     * <p><strong>WARNING:</strong> Unless the destination server absolutely cannot support chunked streaming, do not use this. The entire
     * upload will be loaded into memory in advance of transmission. Depending on available memory and upload size, this is likely to crash
     * the application.</p>
     * @param chunkedUploadDisabled
     */
    public void setChunkedUploadDisabled(boolean chunkedUploadDisabled) {
        this.chunkedUploadDisabled = chunkedUploadDisabled;
    }

    public int getPriority() {
        return priority;
    }

    /**
     * A higher priority value will be pulled from the queue faster once MAX_CONCURRENT_TASKS is hit. 
     * Specify a priority lower than DEFAULT_PRIORITY for very low priority tasks.
     * @param priority
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getCustomKeystoreRawResourceId() {
        return customKeystoreRawResourceId;
    }

    public void setCustomKeystoreRawResourceId(int customKeystoreRawResourceId) {
        this.customKeystoreRawResourceId = customKeystoreRawResourceId;
    }

    public int getCustomKeystorePasswordResourceId() {
        return customKeystorePasswordResourceId;
    }

    public void setCustomKeystorePasswordResourceId(int customKeystorePasswordResourceId) {
        this.customKeystorePasswordResourceId = customKeystorePasswordResourceId;
    }

}
