package com.bottlerocketapps.http;

import android.content.Context;
import android.net.Uri;

/**
 * This provides the interface used by the BaseLoader to handle both Cursor and POJO data types in HTTP response parsing.
 * @author adam.newman
 */
public interface LoaderDataI {
        
    /**
     * Get the resulting object after processResponse, query, or loadCachedResultData is complete.
     * @param <D>
     * @return  Object containing data or null if unimplemented or failure.
     */
    public <D> D getResultData(Class<D> type);
    
    /**
     * <p>Process the httpResponse object. This will be performed on a background thread.</p>
     * <p>All inserts into content providers or local caching (if implemented) should be performed in this step.</p>
     * <p><strong>IMPORTANT:</strong> Always create a new instance of your result. Modifying the data source for an
     * Adapter while the Adapter is using it outside of the UI thread will cause a runtime exception. Classes that 
     * extend BaseCursorLoaderData do not have to worry about this as the cursor creation/replacement is handled for you and 
     * your result data is not created in this step.
     * 
     * @return Boolean success value for processing.
     */
    public boolean processResponse(Context context);
    
    /**
     * Only return true from this method if caching is performed in the object that implements this interface.
     * Returning true will result in a background thread call to loadCachedResultData.
     * 
     * @param context
     * @return True for a cache hit in the cache locally managed by the object implementing this interface.
     */
    public boolean isCacheOk(Context context);
    
    /**
     * This method will only be called if isCacheOk returns true. This will be performed on a background thread.
     */
    public boolean loadCachedResultData(Context context);
    
    public BRHttpRequest getHttpRequest();
    
    public void setHttpRequest(BRHttpRequest request);

    public BRHttpResponse getHttpResponse();
    
    public void setHttpResponse(BRHttpResponse response);
    
    public BRHttpProgress getHttpProgress();
    
    public void setHttpProgress(BRHttpProgress progress);
    
    /**
     * Returns true when the data has been processed successfully and getResultData() should return valid data.
     * @return
     */
    public boolean isSuccess();
    
    /**
     * Should only be called with true once data is ready to be retrieved in getResultData()
     * @param success
     */
    public void setSuccess(boolean success);
    
    /**
     * Returns true when the request has been completed.
     * @return
     */
    public boolean isComplete();
    
    /**
     * Determine current loader status.
     * @return
     */
    public LoaderStatus getStatus();
       
    public void setStatus(LoaderStatus status);
    
    /**
     * New data has been delivered, destroy the old instance of your data if any (close the old cursor, etc).
     */
    public void postDeliveryCleanup(Context context);
    
    /**
     * Content Uri to be monitored by the loader's ContentObserver. 
     * This should return null if monitoring is undesired or this loader data is not backed by a content provider.
     * 
     * @return Uri for content to be observed or null.
     */
    public Uri getContentUri();
        
    /**
     * Performs a query of the content provider. If the implementing class is not cursor based, always return true.
     * 
     * @return True if query was successful or this is not a cursor-based implementation
     */
    public boolean query(Context context);
    
    /**
     * Close any cursor and destroy result data.
     */
    public void reset();
    
    /**
     * Return true if the loader should perform a query operation before fetching from the web. 
     * <p><strong>NOTE:</strong> You will not receive the typical result delivery if the loader 
     * enters TRANSFERRING status immediately before/after prequery completion to avoid redundancy.</p> 
     *  
     * @return True if a call to query should be performed before the web request.
     */
    public boolean shouldPreQuery();
    
    /**
     * Because the loader manager will not re-deliver an object that it considers to be identical (not using .equals())
     * we must perform a shallow copy of the object. 
     * 
     * @return Shallow copy of the objects. 
     */
    public LoaderDataI clone();
}
