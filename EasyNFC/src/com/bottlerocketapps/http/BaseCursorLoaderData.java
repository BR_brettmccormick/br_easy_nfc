package com.bottlerocketapps.http;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/**
 * Same as BaseLoader with sensible defaults for cursor management.
 * @author adam.newman
 *
 */
public abstract class BaseCursorLoaderData extends BaseLoaderData {
    
    private Cursor mCursor;
    private Cursor mOldCursor;
    private Uri mUri;
    private String[] mProjection;
    private String mSelection;
    private String[] mSelectionArgs;
    private String mSortOrder;
    
    public BaseCursorLoaderData(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        mUri = uri;
        mProjection = projection;
        mSelection = selection;
        mSelectionArgs = selectionArgs;
        mSortOrder = sortOrder;
    }
    
    @Override
    public <D> D getResultData(Class<D> type) {        
        if (type.equals(Cursor.class) && mCursor != null) {
            return type.cast(mCursor);
        }
        return null;
    }
    
    @Override
    public Uri getContentUri() {
        return mUri;
    }
    
    @Override
    public void postDeliveryCleanup(Context context) {
        if (mOldCursor != null && !mOldCursor.isClosed()) {
            mOldCursor.close();
        }
    }

    @Override
    public boolean query(Context context) {
        mOldCursor = mCursor;        
        mCursor = context.getContentResolver().query(mUri, mProjection, mSelection, mSelectionArgs, mSortOrder);
        return mCursor != null;
    }

    @Override
    public void reset() {
        super.reset();
        if (mCursor != null && !mCursor.isClosed()) {
            mCursor.close();
        }
        if (mOldCursor != null && !mOldCursor.isClosed()) {
            mOldCursor.close();
        }
        mCursor = null;
        mOldCursor = null;
    }
    
}
