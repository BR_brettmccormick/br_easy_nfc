package com.bottlerocketapps.http;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.PriorityBlockingQueue;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.text.TextUtils;

import com.bottlerocketapps.easynfc.BuildConfig;
import com.bottlerocketapps.http.ssl.CustomKeyStoreSSLSocketFactory;
import com.bottlerocketapps.http.ssl.JavaxCustomKeyStoreSSLSocketFactory;
import com.bottlerocketapps.tools.BRSerializer;
import com.bottlerocketapps.tools.Log;
import com.bottlerocketapps.tools.NetworkTools;
import com.bottlerocketapps.tools.NetworkTools.BRMultipartPostDataParameter;
import com.jakewharton.DiskLruCache;
import com.jakewharton.DiskLruCache.Editor;
import com.jakewharton.DiskLruCache.Snapshot;

/**
 * HTTP protocol service.<br/>
 * You must use the convenience method performRequest() in order to provide immediate cache response callbacks.
 * @author adam.newman
 *
 */
public class HttpClientService extends Service {
    private static final String TAG = HttpClientService.class.getSimpleName();    
    
    //This is private to avoid clients directly starting the service.
    private static final String EXTRA_REQUEST = "request";
    private static final String EXTRA_RECEIVER = "receiver";
    private static final String EXTRA_CANCEL_REQUEST = "cancelRequest";
    
    private static final String CACHE_DIRECTORY = "/httpClientService";
    private static final long CACHE_SIZE_LIMIT = 2 * 1024 * 1024;    
    private static final int CACHE_APP_VERSION = 1;
    private static final int CACHE_VALUE_COUNT = 2;
    private static final int CACHE_INDEX_RESPONSE = 0;
    private static final int CACHE_INDEX_TIMESTAMP = 1;
    
    private static final String MULTIPART_BOUNDARY = "0xKhTmLbOuNdArY"; //String unlikely to appear as --MULTIPART_BOUNDARY in the data.
    private static final int BUFFER_SIZE = 8 * 1024;                    //File read buffer size in bytes.
    private static final int UPDATE_INTERVAL = 1 * 1000;                //Interval to send update to result receiver in ms. 
    private static final int UPLOAD_CHUNK_SIZE = 16 * 1024;             //Upload size per chunk. Smaller chunk size allows more frequent progress updates on slow networks 16KB is a good middle ground.
    
    private static final int INACTIVITY_SHUTDOWN_LIMIT = 60 * 1000;             //Stop the service after running for this many ms without a new request.
    private static final int INACTIVITY_SHUTDOWN_CHECK_INTERVAL = 20 * 1000;    //Check service inactivity interval in ms.
    
    private static final int MAX_CONCURRENT_TASKS = 5;  //Limit for concurrently executing async tasks.
    
    public enum RequestResult {
        INVALID_REQUEST,
        CACHE_HIT,
        TRANSFER_REQUIRED
    };
    
    private static boolean sCacheEnabled = true;
    private static DiskLruCache sDiskLruCache;
    
    private static Map<String, List<RequestWithReceiver>> sInProgressRequests;
    private static Map<String, HttpClientTask> sInProgressTasks;
    private PriorityBlockingQueue<RequestWithTask> mPendingTasks;
    
    private static long sInactivityTimestamp;
    private ShutdownRunnable mShutdownRunnable;
    
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");
        sInProgressRequests = new HashMap<String, List<RequestWithReceiver>>();
        sInProgressTasks = new HashMap<String, HttpClientTask>();
        mPendingTasks = new PriorityBlockingQueue<RequestWithTask>();
        touchInactivityTimestamp();
        mShutdownRunnable = new ShutdownRunnable(this);
        mShutdownRunnable.scheduleCheck(this);
        initCache(getApplicationContext());
    }
    
    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy()");
        if (sCacheEnabled && sDiskLruCache != null) {
            try {
                sDiskLruCache.close();
                
            } catch (IOException e) {
                Log.e(TAG, "Failed to close cache");
            }
        }
        sDiskLruCache = null;
        sInProgressRequests = null;
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG, "onStartCommand()");
        
        touchInactivityTimestamp();
        
        BRHttpRequest request = (BRHttpRequest) intent.getSerializableExtra(EXTRA_REQUEST);
        if (intent.getBooleanExtra(EXTRA_CANCEL_REQUEST, false)) {

            //Cancel the in-progress download and discard the task. 
            Log.v(TAG, "Attempting to cancel request for " + request.getKey());
            if (sInProgressRequests.containsKey(request.getKey())) {
                List<RequestWithReceiver> list = sInProgressRequests.get(request.getKey());
                
                //Only delete single receiver tasks. For tasks with multiple receivers it is hard to uniquely identify the source of the cancel request.
                if (list != null && list.size() == 1) {                    
                    //Remove any queued transfers for this key first.
                    cancelQueuedItems(request.getKey());
                    
                    //Stop any in-progress transfers for this key.
                    HttpClientTask task = sInProgressTasks.get(request.getKey());
                    if (task != null) {
                        Log.v(TAG, "Cancelling " + request.getKey());
                        task.cancel(false);
                    } else {
                        Log.w(TAG, "Task was not available despite key match in current requests for " + request.getKey());
                    }
                    
                    //Then remove the record of in-progress transfers.
                    sInProgressTasks.remove(request.getKey());
                    sInProgressRequests.remove(request.getKey());
                } else {
                    Log.w(TAG, "Cancel aborted, more than one receiver exists for " + request.getKey());
                }
            }
        } else {
            //Queue the download or add a new receiver to an in-progress download.
            ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra(EXTRA_RECEIVER);
            RequestWithReceiver requestWithReceiver = new RequestWithReceiver(request, receiver);
            
            if (sInProgressRequests.containsKey(request.getKey())) {
                Log.v(TAG, "Redundant download request queued.");
                sInProgressRequests.get(request.getKey()).add(requestWithReceiver);
            } else {
                List<RequestWithReceiver> requestList = new ArrayList<RequestWithReceiver>();
                requestList.add(requestWithReceiver);
                sInProgressRequests.put(request.getKey(), requestList);
                HttpClientTask task = new HttpClientTask();
                mPendingTasks.put(new RequestWithTask(request, task));
                processQueue();
            }
        }
        
        return START_NOT_STICKY;
    }
    
    private void cancelQueuedItems(String key) {
        List<RequestWithTask> hitList = new ArrayList<RequestWithTask>();
        Iterator<RequestWithTask> iterator = mPendingTasks.iterator();
        
        while (iterator.hasNext()) {
            RequestWithTask rwt = iterator.next();
            if (rwt.request.getKey().equals(key)) {
                hitList.add(rwt);
            }
        }
        
        if (hitList.size() > 0) mPendingTasks.removeAll(hitList);
    }
    
    private void processQueue() {
        if (sInProgressTasks.size() < MAX_CONCURRENT_TASKS && mPendingTasks.size() > 0) {
            try {
                RequestWithTask rwt = mPendingTasks.remove();
                sInProgressTasks.put(rwt.request.getKey(), rwt.task);
                Log.v(TAG, "Dequeuing task " + rwt.request.getKey());
                rwt.task.execute(rwt.request);
            } catch (NoSuchElementException e) {
                Log.e(TAG, "Race condition: Tried to remove an item from the queue while simultaneously being canceled.", e);
            }
        }
    }
    
    public static void cancelHttpRequest(Context context, BRHttpRequest request) {
        Intent intent = new Intent(context, HttpClientService.class);
        intent.putExtra(EXTRA_REQUEST, request);
        intent.putExtra(EXTRA_CANCEL_REQUEST, true);
        context.startService(intent);
    }
    
    public static RequestResult performHttpRequest(Context context, HttpClientListener listener, String feedUrl, int requestId) {
        return performHttpRequest(context, listener, feedUrl, requestId, BRHttpRequest.DEFAULT_CACHE_LIFETIME);
    }
    
    public static RequestResult performHttpRequest(Context context, HttpClientListener listener, String feedUrl, int requestId, long cacheLifetime) {
        BRHttpRequest request = new BRHttpRequest();
        request.setCacheLifetime(cacheLifetime);
        request.setUrl(feedUrl);
        request.setRequestId(requestId);
        return performHttpRequest(context, listener, request);
    }
    
    public static RequestResult performHttpRequest(Context context, HttpClientListener listener, BRHttpRequest request) {
        //Even if this results in a cache-hit and doesn't send a start intent, the app is still actively requesting data so lets stick around.
        touchInactivityTimestamp(); 
        
        if (context == null || request == null || listener == null) {
            Log.e(TAG, "Parameters cannot be null. (" + Boolean.toString(context == null) + ", " + Boolean.toString(listener == null) + ", " + Boolean.toString(request == null) + ")");
            return RequestResult.INVALID_REQUEST;
        }
        
        CacheCheckResponse ccr = cacheCheck(context, request);
        
        if (ccr.cacheOk) {
            if (sendCachedResult(listener, ccr.snapshot, request)) {
                Log.v(TAG, "Sent immediate cache hit result.");
                return RequestResult.CACHE_HIT;
            }
            Log.w(TAG, "Cache hit but load failed. Downloading...");
        }

        Intent intent = new Intent(context, HttpClientService.class);
        intent.putExtra(EXTRA_RECEIVER, new HttpClientResultReceiver(listener));
        intent.putExtra(EXTRA_REQUEST, request);
        context.startService(intent);
        return RequestResult.TRANSFER_REQUIRED;
    }
    
    private void onDownloadCompleted(BRHttpResponse response, HttpClientTask task) {
        Log.v(TAG, "On Download Completed");
        
        sInProgressTasks.remove(response.getKey());        
        
        if (sInProgressRequests.containsKey(response.getKey())) {
            
            List<RequestWithReceiver> rwrList = sInProgressRequests.get(response.getKey());
            
            for (RequestWithReceiver rwr: rwrList) {
                Log.v(TAG, "Sending result to " + rwr.receiver);
                Bundle resultData = new Bundle();
                resultData.putSerializable(HttpClientResultReceiver.BUNDLE_REQUEST_ID, rwr.request.getRequestId());
                resultData.putSerializable(HttpClientResultReceiver.BUNDLE_RESPONSE, response);
                rwr.receiver.send(HttpClientResultReceiver.RESULT_CODE_COMPLETE, resultData);            
            }
            
            sInProgressRequests.remove(response.getKey());
        } else {
            Log.w(TAG, "There were no matching in-progress requests for the key: " + response.getKey());
        }
        
        //Fire up the next queued task if any.
        processQueue();
    }
    
    private void onTransferProgress(String key, BRHttpProgress progress) {
        List<RequestWithReceiver> rwrList = sInProgressRequests.get(key);
        for (RequestWithReceiver rwr: rwrList) {
            Bundle resultData = new Bundle();
            resultData.putSerializable(HttpClientResultReceiver.BUNDLE_REQUEST_ID, rwr.request.getRequestId());
            resultData.putSerializable(HttpClientResultReceiver.BUNDLE_PROGRESS, progress);
            rwr.receiver.send(HttpClientResultReceiver.RESULT_CODE_PROGRESS, resultData);
        }
    }
    
    public class HttpClientTask extends AsyncTask<BRHttpRequest, Long, BRHttpResponse> {
        
        private BRHttpRequest mRequest;
        private BRHttpResponse mResponse;
        private long mAverageRate = 0;
        private long mLastUpdateTime = 0;
        private long mLastUpdateBytes = 0;
       
        private synchronized void syncWait(int delay) throws InterruptedException {
            wait(delay);
        }
        
        @Override
        protected BRHttpResponse doInBackground(BRHttpRequest... params) {
            mRequest = params[0];
            mResponse = new BRHttpResponse(mRequest.getKey());
            
            if (TextUtils.isEmpty(mRequest.getUrl())) {
                Log.e(TAG, "Url is empty");
                return mResponse;
            }
            
            String fullUrl = NetworkTools.buildURL(mRequest.getUrl(), mRequest.getQueryParameters());
            
            Log.v(TAG, "Starting URL: " + fullUrl);
            
            //If the method has not been set either automatically by the BRHttpRequest object, or by the client, set it to GET. 
            if (mRequest.getMethod() == BRHttpMethod.AUTO) {
                mRequest.setMethod(BRHttpMethod.GET);
            }
            
            BufferedInputStream inputStream = null;
            
            for (int attempt=0; attempt < mRequest.getMaxAttempts(); attempt++) {
                
                //Split request behavior for file uploads or normal transactions
                if (mRequest.getMultipartPostParameters() != null && mRequest.getMultipartPostParameters().size() > 0) {
                    inputStream = performFileUpload(fullUrl);
                } else {
                    inputStream = performHttpRequest(URI.create(fullUrl));
                }
                
                //Perform download phase
                if (!isCancelled() && inputStream != null) {
                    BufferedOutputStream outputStream = null;
                    ByteArrayOutputStream byteArrayOutputStream = null;
                    boolean fileMode = false;
                    try {
                        long totalBytes = mResponse.getContentLength();
                        
                        if (!TextUtils.isEmpty(mRequest.getDownloadFilePath())) {
                            //Download the result into a file
                            File file = new File(mRequest.getDownloadFilePath());
                            
                            //Remove any existing copy of the file. 
                            if (file.exists()) {
                                file.delete();
                            }                            
                            outputStream = new BufferedOutputStream(new FileOutputStream(file));
                            fileMode = true;
                        } else {
                            //Download the result into a string
                            if (totalBytes > Integer.MAX_VALUE) {
                                Log.e(TAG, "You can't download this many bytes into memory: " + totalBytes);
                                break;
                            } else if (totalBytes > 0) {
                                byteArrayOutputStream = new ByteArrayOutputStream((int)(totalBytes % Integer.MAX_VALUE));
                            } else {
                                byteArrayOutputStream = new ByteArrayOutputStream();
                            }
                            outputStream = new BufferedOutputStream(byteArrayOutputStream);
                            fileMode = false;
                        }
                        
                        byte buffer[] = new byte[BUFFER_SIZE];                        
                        long totalBytesRead = 0;
                        int bufferBytesRead = 0;
                        long now;
                        long lastProgressTimestamp = System.currentTimeMillis();
                        while ((bufferBytesRead = inputStream.read(buffer)) >= 0 && !isCancelled()) {
                            if (bufferBytesRead > 0) {
                                outputStream.write(buffer, 0, bufferBytesRead);
                                totalBytesRead += bufferBytesRead;
                            } 
                            
                            //Only publish progress when UPDATE_INTERVAL is exceeded. Too much IPC overhead can effectively tie download speed to CPU speed.
                            now = System.currentTimeMillis();
                            if (now - lastProgressTimestamp > UPDATE_INTERVAL) {                                        
                                this.publishProgress(0l, totalBytesRead, totalBytes);
                                lastProgressTimestamp = now;
                            }
                        }                        

                        if (!fileMode) {
                            outputStream.flush();
                            mResponse.setResponseData(new String(byteArrayOutputStream.toByteArray()));
                        }
                        
                        mResponse.setStatus(HttpResponseStatus.SUCCESS);
                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                    } catch (FileNotFoundException e) {
                        Log.e(TAG, "Error downloading file", e);
                    } catch (SocketTimeoutException e) {
                        Log.d(TAG, "Download timed out");
                        mResponse.setStatus(HttpResponseStatus.TIMEOUT);
                    } catch (IOException e) {
                        Log.e(TAG, "IOException when downloading data", e);
                    } finally {
                        try {
                            if (outputStream != null) {
                                outputStream.close();
                            }
                            if (inputStream != null) {
                                inputStream.close();
                            }
                        } catch (IOException e) {
                            Log.e(TAG, "This just isn't our day.", e);
                        }
                    }                                        
                }
                
                //Everything worked out, finish up and optionally cache data
                if (!isCancelled() && mResponse.getStatus() == HttpResponseStatus.SUCCESS) {
                    if (!mRequest.isDoNotCache()) {
                        //Write to cache
                        storeHttpResponse(mResponse);                        
                    }
                    return mResponse;
                }
                
                //If it makes it to this point, download has failed.
                //If retry attempts remain, wait retryDelay before attempting.
                if (!isCancelled() && attempt < mRequest.getMaxAttempts() - 1) {
                    try {
                        Log.d(TAG, "Retrying in " + mRequest.getRetryDelay() + "ms");
                        syncWait(mRequest.getRetryDelay());
                    } catch (InterruptedException e) {
                        mResponse.setStatus(HttpResponseStatus.INTERNAL_ERROR);
                        Log.e(TAG, "Interrupted while waiting to retry.");
                    }
                }
                
                if (isCancelled()) {
                    Log.v(TAG, "Transfer cancelled, breaking out of retry loop.");
                    break;
                }
            }
            
            if (!isCancelled() && mResponse.getStatus() != HttpResponseStatus.SUCCESS && !mRequest.isForceDownload()) {
                //Request has failed, send stale cache if available and we aren't forcing a download.
                Snapshot snapshot = getCachedSnapshot(getApplicationContext(), mRequest.getKey());
                if (snapshot != null) {
                    mResponse = loadFromSnapshot(snapshot);
                    mResponse.setStatus(HttpResponseStatus.FAILED_WITH_STALE_CACHE);
                }
            }
            
            if (isCancelled()) {
                Log.w(TAG, "Transfer was cancelled");
            }
            
            return mResponse;
        }
        
        /**
         * Send all data to the server and then return a buffered reader to read response for normal small non-multipart posts, gets, puts, and deletes.
         * @param fullUrl
         * @param response
         * @return
         */
        private BufferedInputStream performHttpRequest(URI uri) {
            HttpUriRequest request = null;
            
            switch(mRequest.getMethod()) {
                case PUT:
                case POST:
                    HttpEntityEnclosingRequestBase tmpRequest;
                    if (mRequest.getMethod() == BRHttpMethod.POST) {
                        tmpRequest = new HttpPost(uri);
                    } else {
                        tmpRequest = new HttpPut(uri);
                    }
                    try {
                        if (mRequest.getPostParameters() != null && mRequest.getPostParameters().size() > 0) {
                            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                            for (String key : mRequest.getPostParameters().keySet()) {
                                nameValuePairs.add(new BasicNameValuePair(key, mRequest.getPostParameters().get(key)));
                            }                        
                            tmpRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));                        
                        } else {
                            Log.e(TAG, "An empty set of post or multipartpost parameters were supplied.");
                            return null;
                        }
                    } catch (UnsupportedEncodingException e) {
                        Log.e(TAG, "Unsupported Encoding ", e);
                        return null;
                    }
                    request = tmpRequest;
                    break;
                case DELETE:
                    request = new HttpDelete(uri);
                    break;
                case GET:
                    request = new HttpGet(uri);
                    break;
                default:
                    Log.e(TAG, "Unexpected method");
                    return null;
            }
            
            request.setHeader(HTTP.USER_AGENT, mRequest.getUserAgent());
            
            //Add user-supplied headers possibly overriding user agent
            if (mRequest.getHeaders() != null && mRequest.getHeaders().size() > 0) {
                for (String key: mRequest.getHeaders().keySet()) {
                    request.setHeader(key, mRequest.getHeaders().get(key));
                }
            }
            
            //Create a connection with the provided timeouts
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, mRequest.getTimeout());
            HttpConnectionParams.setSoTimeout(httpParams, mRequest.getTimeout());
            DefaultHttpClient httpClient = null;
            if (mRequest.getCustomKeystoreRawResourceId() != 0) {
                final SchemeRegistry schemeRegistry = new SchemeRegistry();
                schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
                schemeRegistry.register(
                        new Scheme(
                                "https", 
                                CustomKeyStoreSSLSocketFactory.createInstance(
                                        getApplicationContext(), 
                                        mRequest.getCustomKeystoreRawResourceId(),
                                        mRequest.getCustomKeystorePasswordResourceId()),
                                443)
                );                
                ClientConnectionManager ccm = new ThreadSafeClientConnManager(httpParams, schemeRegistry);
                httpClient = new DefaultHttpClient(ccm, httpParams);
            } else {
                httpClient = new DefaultHttpClient(httpParams);
            }
    
            HttpResponse httpResponse;
            BufferedInputStream bis = null;
            
            try {
                httpResponse = httpClient.execute(request);
                mResponse.setHttpStatus(httpResponse.getStatusLine().getStatusCode());
                mResponse.storeResponseHeaders(httpResponse.getAllHeaders());
                if (!mResponse.isHttpStatusOk()) {
                    mResponse.setStatus(HttpResponseStatus.SERVER_ERROR);
                } else {
                    bis = new BufferedInputStream(httpResponse.getEntity().getContent());
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (SocketTimeoutException e) {
                Log.d(TAG, "Request timed out");
                mResponse.setStatus(HttpResponseStatus.TIMEOUT);
            } catch (IOException e) {
                e.printStackTrace();
            }
            
            return bis;
        }
        
        private long writeToBuffer(BufferedOutputStream bos, byte[] buffer) throws IOException {
            return writeToBuffer(bos, buffer, 0, buffer.length);
        }
        
        private long writeToBuffer(BufferedOutputStream bos, byte[] buffer, int offset, int length) throws IOException {
            bos.write(buffer, offset, length);
            return length;
        }
        
        /**
         * Perform a multipart post using chunked streaming to avoid loading the entire upload into memory. If the request specifies that chunked
         * streaming should be disabled, the entire upload will be loaded into memory and large files are not supported.
         * 
         * @param fullUrl
         * @return
         */
        private BufferedInputStream performFileUpload(String fullUrl) {
            InputStream responseInStream;
            HttpURLConnection connection = null;
            BufferedInputStream bis = null;
            long contentLength = 0;

            try {
                URL url = new URL(fullUrl);
                connection = (HttpURLConnection) url.openConnection();
                if (connection instanceof HttpsURLConnection && mRequest.getCustomKeystoreRawResourceId() != 0) {
                    ((HttpsURLConnection) connection).setSSLSocketFactory(
                            JavaxCustomKeyStoreSSLSocketFactory.createInstance(
                                    getApplicationContext(), 
                                    mRequest.getTimeout(), 
                                    mRequest.getCustomKeystoreRawResourceId(), 
                                    mRequest.getCustomKeystorePasswordResourceId()));
                }
                switch (mRequest.getMethod()) {
                    case POST:
                        connection.setRequestMethod("POST");
                        break;
                    case PUT:
                        connection.setRequestMethod("PUT");
                        break;
                    default:
                        Log.e(TAG, "Multipart post method must be PUT or POST");
                        return null;
                }
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setConnectTimeout(mRequest.getTimeout());
                connection.setReadTimeout(mRequest.getTimeout());

                //Without this, write operations to the output stream will be buffered in memory and crash depending on available memory and bandwidth.
                if (!mRequest.isChunkedUploadDisabled()) {
                    connection.setChunkedStreamingMode(UPLOAD_CHUNK_SIZE);    
                } else {
                    Log.d(TAG, "Performing non-chunked upload. For large files, this will cause an out of memory error. Progress updates will not be sent.");
                }
                
                //Set User Agent
                connection.setRequestProperty(HTTP.USER_AGENT, mRequest.getUserAgent());

                //Add client supplied headers (cookies/etc)
                if (mRequest.getHeaders() != null) {
                    for (String key: mRequest.getHeaders().keySet()) {
                        connection.setRequestProperty(key, mRequest.getHeaders().get(key));
                    }
                }
                
                //This will always be a mulitpart post
                connection.setRequestProperty(HTTP.CONTENT_TYPE, String.format("multipart/form-data; boundary=%s", MULTIPART_BOUNDARY));
                
                BufferedOutputStream requestOutStream = null;
                ByteArrayOutputStream requestBuffer = null;
                if (!mRequest.isChunkedUploadDisabled()) {
                    //Connection opened now. No further modification to connection options or request headers allowed.
                    connection.connect();
                    requestOutStream = new BufferedOutputStream(connection.getOutputStream());
                } else {
                    //Oh man, this is going to take a chunk of memory. Too bad this server is so old.
                    requestBuffer = new ByteArrayOutputStream();
                    requestOutStream = new BufferedOutputStream(requestBuffer);
                }
                
                //Write normal post values
                if (mRequest.getPostParameters() != null) {
                    for (String key: mRequest.getPostParameters().keySet()) {
                        contentLength += writeToBuffer(requestOutStream, String.format("--%s\r\n", MULTIPART_BOUNDARY).getBytes());
                        contentLength += writeToBuffer(requestOutStream, String.format("Content-Disposition: form-data; name=\"%s\"\r\n\r\n", key).getBytes());
                        contentLength += writeToBuffer(requestOutStream, mRequest.getPostParameters().get(key).getBytes());
                        contentLength += writeToBuffer(requestOutStream, "\r\n".getBytes());
                    }
                }
                
                byte buffer[] = new byte[BUFFER_SIZE];
                
                //Upload files
                if (mRequest.getMultipartPostParameters() != null) {
                    for (BRMultipartPostDataParameter parameter: mRequest.getMultipartPostParameters()) {
                        File inputFile = new File(parameter.fileName);
                        if (inputFile.exists()) {
                            long startTime = System.currentTimeMillis();
                            long now = System.currentTimeMillis();
                            long lastProgressTimestamp = now;
                                                            
                            int bytesRead = 0;
                            long bytesTotal = inputFile.length();
                            long bytesSent = 0;
                            
                            contentLength += writeToBuffer(requestOutStream, String.format("--%s\r\n", MULTIPART_BOUNDARY).getBytes());
                            contentLength += writeToBuffer(requestOutStream, String.format("Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n", parameter.fieldName, parameter.fileName).getBytes());
                            contentLength += writeToBuffer(requestOutStream, String.format("Content-Type: %s\r\n\r\n", parameter.contentType).getBytes());

                            //Write the file to the server.
                            InputStream fileInStream = new BufferedInputStream(new FileInputStream(inputFile), BUFFER_SIZE);
                            while ((bytesRead = fileInStream.read(buffer)) >= 0 && !isCancelled()) {
                                if (bytesRead > 0) {
                                    contentLength += writeToBuffer(requestOutStream, buffer, 0, bytesRead);            
                                    bytesSent += bytesRead;                                        
                                }
                                
                                //Only publish progress when UPDATE_INTERVAL is exceeded. Too much IPC overhead can effectively tie upload speed to CPU speed.
                                now = System.currentTimeMillis();
                                if (now - lastProgressTimestamp > UPDATE_INTERVAL) {                                        
                                    this.publishProgress(1l, bytesSent, bytesTotal);
                                    lastProgressTimestamp = now;
                                }
                            }
                            fileInStream.close();
                            contentLength += writeToBuffer(requestOutStream, "\r\n".getBytes());
                            
                            float duration = (float)(System.currentTimeMillis() - startTime) / 1000.0f;
                            Log.v(TAG, "Uploaded " + bytesSent + "B @ " + ((float)bytesSent / 1024.0f / duration) + "KB/s");
                        } else {
                            Log.w(TAG, "File did not exist: " + parameter.fileName);
                        }
                    }
                } else {
                    Log.w(TAG, "No files were specified for upload.");
                }
                
                //If aborted, do not complete boundary so server will reject this post.
                if (!isCancelled()) contentLength += writeToBuffer(requestOutStream, String.format("--%s--\r\n", MULTIPART_BOUNDARY).getBytes());                    
                
                if (mRequest.isChunkedUploadDisabled()) {
                    //Chunked transfer was disabled, this server probably also expects a Content-Length header.
                    connection.setRequestProperty(HTTP.CONTENT_LEN, Long.toString(contentLength));
                    //Now that we know content length, we can connect.
                    connection.connect();
                    //Copy the buffer to the output stream.
                    requestOutStream.flush();
                    BufferedOutputStream bos = new BufferedOutputStream(connection.getOutputStream());
                    bos.write(requestBuffer.toByteArray());
                    requestOutStream.close();
                    bos.close();
                } else {
                    //Nothing left to send. 
                    requestOutStream.close();
                }
                
                //Open response reader
                responseInStream = connection.getInputStream();
                mResponse.setHttpStatus(connection.getResponseCode());
                mResponse.storeResponseHeaders(connection.getHeaderFields());
                if (!mResponse.isHttpStatusOk()) {
                    mResponse.setStatus(HttpResponseStatus.SERVER_ERROR);
                } else {
                    bis = new BufferedInputStream(responseInStream);
                }
            } catch (ClientProtocolException e) {
                Log.e(TAG, "ClientProtocolException during upload", e);
            } catch (MalformedURLException e) {
                Log.e(TAG, "MalformedURLException during upload", e);
            } catch (IllegalStateException e) {
                Log.e(TAG, "IllegalStateException during upload", e);
            } catch (SocketTimeoutException e) {
                mResponse.setStatus(HttpResponseStatus.TIMEOUT);
                Log.e(TAG, "Upload timeout", e);
            } catch (IOException e) {
                if (connection != null) {
                    logErrorStream(connection);
                }
                Log.e(TAG, "IOException on upload", e);
            } 
            return bis;
        }
        
        /**
         * Log the error stream from the provided HttpURLConnection. As this is only useful for troubleshooting purposes it casts a wide
         * net for exception handling and will only execute on a debug build.
         * 
         * @param connection
         */
        private void logErrorStream(HttpURLConnection connection) {
            if (!BuildConfig.DEBUG) {
                return;
            }
            BufferedInputStream bis = null;
            ByteArrayOutputStream baos = null;
            try {
                InputStream eis = connection.getErrorStream();
                bis = new BufferedInputStream(eis);
                baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[BUFFER_SIZE];
                int bufferBytesRead = 0;
                while ((bufferBytesRead = bis.read(buffer)) >= 0 && !isCancelled()) {
                    if (bufferBytesRead > 0) {
                        baos.write(buffer, 0, bufferBytesRead);
                    } 
                }
                String message = new String(baos.toByteArray());
                Log.e(TAG, "Received error response from server: HTTP " + connection.getResponseCode() + "\n" + message) ;
            } catch (Throwable t) {
                Log.e(TAG, "Exception logging error stream on IO Exception", t);
            } finally {
                try {
                    if (bis != null) {
                        bis.close();
                    }
                    if (baos != null) {
                        baos.close();
                    }
                } catch (Throwable t) {
                    Log.e(TAG, "Exception closing streams on IO Exception", t);
                }
            }
        }
        
        @Override
        protected void onPostExecute(BRHttpResponse response) {
            onDownloadCompleted(response, this);
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            BRHttpProgress progress = new BRHttpProgress();
            progress.upload = values[0] == 1;
            progress.bytesComplete = values[1];
            progress.bytesTotal = values[2];
            
            long currentTime = System.currentTimeMillis();
            long bytesPerSecond = (long)((float)(progress.bytesComplete - mLastUpdateBytes) / ((float)(currentTime - mLastUpdateTime) / 1000.0f));
            
            mLastUpdateBytes = progress.bytesComplete;
            mLastUpdateTime = currentTime;

            //Weight speed 9 parts running average, 1 part current rate after initial value is recorded.
            mAverageRate = (mAverageRate > 0) ? (mAverageRate * 9 + bytesPerSecond) / 10 : bytesPerSecond;
            progress.rateBps = mAverageRate;
            
            onTransferProgress(mRequest.getKey(), progress);
        }
    }
    
    private class RequestWithReceiver {
        public ResultReceiver receiver;
        public BRHttpRequest request;
        
        public RequestWithReceiver(BRHttpRequest request, ResultReceiver receiver) {
            this.request = request;
            this.receiver = receiver;
        }
    }
    
    private class RequestWithTask implements Comparable<RequestWithTask> {
        public BRHttpRequest request;
        public HttpClientTask task;
        
        public RequestWithTask(BRHttpRequest request, HttpClientTask task) {
            this.request = request;
            this.task = task;
        }

        @Override
        public int compareTo(RequestWithTask another) {
            //Descending "natural order"
            return another.request.getPriority() - this.request.getPriority();
        }
    }
    
    /******Connection to calling object******/
    
    /**
     * Listen for notifications from HttpClientService upon download completion/faliure
     * @author adam.newman
     *
     */
    public interface HttpClientListener {
        /**
         * Handle the result of an http client operation.
         * 
         * @param requestId
         * @param success
         * @param response
         */
        void onHttpClientResult(int requestId, boolean success, BRHttpResponse response);
        
        /**
         * Handle a progress update of an upload or download operation
         * @param requestId
         * @param progress
         */
        void onHttpClientProgress(int requestId, BRHttpProgress progress);
    }
    
    /**
     * Extend ResultReceiver to Handle IPC between service and listener
     * 
     * @author adam.newman
     */
    public static class HttpClientResultReceiver extends ResultReceiver {
        
        public static final int RESULT_CODE_PROGRESS = 1;
        public static final int RESULT_CODE_COMPLETE = 2;
        
        public static final String BUNDLE_REQUEST_ID = "requestId";
        public static final String BUNDLE_RESPONSE = "response";
        public static final String BUNDLE_PROGRESS = "progress";
        
        WeakReference<HttpClientListener> mListenerRef;
        
        public HttpClientResultReceiver(HttpClientListener listener) {
            this(new Handler(), listener);
        }

        public HttpClientResultReceiver(Handler handler, HttpClientListener listener) {
            super(handler);            
            mListenerRef = new WeakReference<HttpClientService.HttpClientListener>(listener);
        }
        
        protected void onReceiveResult (int resultCode, Bundle resultData) {
            Log.v(TAG, "onReceiveResult()");
            HttpClientListener listener = null;
            if (mListenerRef != null) {
                listener = mListenerRef.get();
            }
            
            if (mListenerRef == null || listener == null) {
                Log.w(TAG, "Listener was missing or null");
            }
            
            int requestId = resultData.getInt(BUNDLE_REQUEST_ID);
            
            switch (resultCode) {
                case RESULT_CODE_PROGRESS:
                    BRHttpProgress progress = (BRHttpProgress) resultData.getSerializable(BUNDLE_PROGRESS);
                    listener.onHttpClientProgress(requestId, progress);
                    break;
                case RESULT_CODE_COMPLETE:
                    BRHttpResponse response = (BRHttpResponse) resultData.getSerializable(BUNDLE_RESPONSE);
                    listener.onHttpClientResult(requestId, response.getStatus() == HttpResponseStatus.SUCCESS || response.getStatus() == HttpResponseStatus.FAILED_WITH_STALE_CACHE, response);
            }
        }
    }
    
    /****** CACHING OPERATIONS *******/
    
    /**
     * Initialize sCacheEnabled object for future use.
     * @param context
     */
    private static void initCache(Context context) {
        if (sCacheEnabled && sDiskLruCache == null) {
            File cacheDir = getCacheDirectory(context);
            try {            
                sDiskLruCache = DiskLruCache.open(cacheDir, CACHE_APP_VERSION, CACHE_VALUE_COUNT, CACHE_SIZE_LIMIT);
            } catch (IOException e) {
                Log.e(TAG, "Could not open cache");
                sCacheEnabled = false;
            }
        }
    }
    
    public static File getCacheDirectory(Context context) {
        return new File(context.getCacheDir() + CACHE_DIRECTORY);
    }
    
    public static Snapshot getCachedSnapshot(Context context, String key) {
        if (!sCacheEnabled) return null;
        initCache(context); //Probably redundant, low cost to verify on a static call.
        
        try {
            if (sDiskLruCache != null) {
                return sDiskLruCache.get(key);
            }            
        } catch (IOException e) {
            Log.e(TAG, "Exception when getting cached snapshot", e);
        }
        return null;
    }

    private static boolean sendCachedResult(HttpClientListener listener, Snapshot snapshot, BRHttpRequest request) {
        BRHttpResponse response = loadFromSnapshot(snapshot);
        if (response != null) {
            listener.onHttpClientResult(request.getRequestId(), true, response);
            return true;
        }
        return false;
    }
    
    public static boolean isSnapshotFresh(Snapshot snapshot, long maxAge) {
        long timestamp = getSnapshotTimestamp(snapshot);
        return System.currentTimeMillis() - timestamp < maxAge;
    }
    
    public static long getSnapshotTimestamp(Snapshot snapshot) {        
        try {
            String timestamp = snapshot.getString(CACHE_INDEX_TIMESTAMP);
            if (!TextUtils.isEmpty(timestamp)) {
                return Long.parseLong(timestamp);
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException when reading cache timestamp", e);
        } catch (NumberFormatException e) {
            Log.e(TAG, "NumberFormatException on cache timestamp", e);
        }
        return 0;
    }
    
    public static boolean deleteCachedResponse(Context context, String key) {
        if (!sCacheEnabled) return false;
        initCache(context); //Probably redundant, low cost to verify on a static call.
        
        try {
            if (sDiskLruCache != null) {
                return sDiskLruCache.remove(key);
            }            
        } catch (IOException e) {
            Log.e(TAG, "Exception when deleting cached snapshot", e);
        }
        return false;
    }
    
    public static BRHttpResponse loadFromSnapshot(Snapshot snapshot) {
        InputStream is = snapshot.getInputStream(CACHE_INDEX_RESPONSE);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (is != null) {
            byte[] buffer = new byte[4096];
            try {
                while (is.read(buffer) >= 0) {
                    baos.write(buffer);
                }
                is.close();
                BRHttpResponse response = (BRHttpResponse) BRSerializer.deserializeFromByteArray(baos.toByteArray());
                Log.v(TAG, "Cache loaded for key " + response.getKey());
                return response;
            } catch (IOException e) {
                Log.e(TAG, "Exception when reading snapshot stream.", e);
                return null;
            } catch (ClassCastException e) {
                Log.e(TAG, "Exception casting cached data to BRHttpResponse", e);
            }
        }
        return null;
    }
    
    public static boolean storeHttpResponse(BRHttpResponse response) {
        if (!sCacheEnabled) return false;
        
        if (response == null || sDiskLruCache == null) {
            Log.e(TAG, "Unable to store cache, invalid state or data.");
            return false;
        }
        
        try {
            Editor editor = sDiskLruCache.edit(response.getKey());
            if (editor != null) {
                editor.set(CACHE_INDEX_TIMESTAMP, Long.toString(System.currentTimeMillis()));
                
                byte[] data = BRSerializer.serializeToByteArray(response);
                OutputStream os = editor.newOutputStream(CACHE_INDEX_RESPONSE);
                os.write(data);
                os.close();
                
                editor.commit();
                Log.v(TAG, "Cache written for key " + response.getKey());
                return true;
            } else {
                Log.w(TAG, "Edit failed, concurrent edit is in progress");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }        
        return false;
    }
    
    /**
     * Created an object to return from cacheCheck so that the static method to start downloads can
     * receive the snapshot along with the cache check pass/fail.
     * @author adam.newman
     *
     */
    public static class CacheCheckResponse {
        public boolean cacheOk;
        public Snapshot snapshot;
    }
    
    /**
     * Based on the request parameters, determine if the locally cached data is good enough to return
     * instantly.
     * 
     * @param context
     * @param request
     * @return
     */
    public static CacheCheckResponse cacheCheck(Context context, BRHttpRequest request) {
        CacheCheckResponse ccr = new CacheCheckResponse();
        ccr.cacheOk = false;
        if (!request.isForceDownload()) {
            ccr.snapshot = getCachedSnapshot(context, request.getKey());
            if (ccr.snapshot != null && isSnapshotFresh(ccr.snapshot, request.getCacheLifetime())) {
                ccr.cacheOk = true;
                return ccr;
            } else if (ccr.snapshot != null) {
                Log.v(TAG, "Cache is too old.");
            }
        }
        return ccr;
    }
    
    @Override
    public IBinder onBind(Intent intent) {
        //The binder it does nothing.
        return null;
    }
    
    /***** INACTIVITY SHUTDOWN TIMER *****/
    
    private Handler mHandler;
    
    private Handler getHandler() {
        if (mHandler == null) {
            mHandler = new Handler();
        }
        return mHandler;
    }
    
    public static void touchInactivityTimestamp() {
        HttpClientService.sInactivityTimestamp = System.currentTimeMillis();
    }

    private static class ShutdownRunnable implements Runnable {
        private WeakReference<HttpClientService> mServiceRef;
        
        public ShutdownRunnable(HttpClientService service) {
            mServiceRef = new WeakReference<HttpClientService>(service);
        }
        
        public void scheduleCheck(HttpClientService service) {
            service.getHandler().removeCallbacks(this);
            service.getHandler().postDelayed(this, INACTIVITY_SHUTDOWN_CHECK_INTERVAL);
        }
        
        @Override
        public void run() {
            HttpClientService service = mServiceRef.get();
            if (service != null) {
                if (System.currentTimeMillis() - HttpClientService.sInactivityTimestamp > INACTIVITY_SHUTDOWN_LIMIT
                        && HttpClientService.sInProgressRequests.size() == 0) {
                    Log.v(TAG, "Inactivity timeout, shutting down...");
                    service.stopSelf();
                } else {
                    if (HttpClientService.sInProgressRequests.size() > 0) {
                        touchInactivityTimestamp();
                    }
                    Log.v(TAG, "Heartbeat");
                    scheduleCheck(service);
                }
            }
        }
    }
}

