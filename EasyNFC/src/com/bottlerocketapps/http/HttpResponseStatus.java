package com.bottlerocketapps.http;

public enum HttpResponseStatus {
    SUCCESS,
    FAILED_WITH_STALE_CACHE,
    SERVER_ERROR,
    TIMEOUT,
    INTERNAL_ERROR,
    NETWORK_OFFLINE
}
