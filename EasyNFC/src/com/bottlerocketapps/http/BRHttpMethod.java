package com.bottlerocketapps.http;

public enum BRHttpMethod {
    AUTO,
    GET,
    PUT,
    POST,
    DELETE
}
