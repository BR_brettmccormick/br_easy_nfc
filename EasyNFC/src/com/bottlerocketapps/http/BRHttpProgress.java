package com.bottlerocketapps.http;

import java.io.Serializable;

public class BRHttpProgress implements Serializable{
    private static final long serialVersionUID = 5049956902177153269L;
    
    public boolean upload;
    public long bytesComplete;
    public long bytesTotal;
    public long rateBps;
}
