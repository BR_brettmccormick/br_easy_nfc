package com.bottlerocketapps.service;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import com.bottlerocketapps.tools.BRSerializer;
import com.bottlerocketapps.tools.NetworkTools;
import com.bottlerocketapps.tools.NetworkTools.BRMultipartPostDataParameter;

/**
 * Perform download of feeds to be stored in cache. 
 * If an feed is present in cache and is within its specified lifetime, return the cached feed.
 */
public class FeedDownloadService extends Service {
    
    public static final String TAG = "FeedDownloadService";

    public static final String CANONICAL_NAME = FeedDownloadService.class.getCanonicalName();
    public static final String EXTRA_URL = CANONICAL_NAME + ".URL";
    public static final String EXTRA_CACHE_LIFETIME = CANONICAL_NAME + ".lifetime";
    public static final String EXTRA_TIMEOUT = CANONICAL_NAME + ".timeout";
    public static final String EXTRA_FORCE_DOWNLOAD = CANONICAL_NAME + ".forceDownload";
    public static final String EXTRA_MAX_ATTEMPTS = CANONICAL_NAME + ".maxAttempts";
    public static final String EXTRA_RETRY_DELAY = CANONICAL_NAME + ".retryDelay";
    public static final String EXTRA_DO_NOT_CACHE = CANONICAL_NAME + ".doNotCache";
    public static final String EXTRA_DO_NOT_SEND_DATA_BACK = CANONICAL_NAME + ".doNotSendDataBack";
    public static final String EXTRA_RECEIVER = CANONICAL_NAME + ".receiver";
    public static final String EXTRA_HEADERS = CANONICAL_NAME + ".headers";
    public static final String EXTRA_QUERY = CANONICAL_NAME + ".query";
    public static final String EXTRA_POST = CANONICAL_NAME + ".post";
    public static final String EXTRA_MULTIPART_POST = CANONICAL_NAME + ".multipart";
    public static final String EXTRA_REQUEST_ID = CANONICAL_NAME + ".id";
    public static final String EXTRA_METHOD = CANONICAL_NAME + ".method";
    public static final String EXTRA_USER_AGENT = CANONICAL_NAME + ".userAgent";
    
    public static final int METHOD_AUTO = 0;   //If (post|multipartPost)Parameters are present, assume POST, otherwise GET
    public static final int METHOD_GET = 1;
    public static final int METHOD_PUT = 2;
    public static final int METHOD_POST = 3;
    public static final int METHOD_DELETE = 4;
    
    // Keys to get values out of the "resultData" bundle after a feed has been downloaded
    public static final String BUNDLE_REQUEST_ID = "requestId";
    public static final String BUNDLE_KEY = "key";
    public static final String BUNDLE_HTTP_STATUS = "httpStatus";
    public static final String BUNDLE_FEED = "feed";
    public static final String BUNDLE_RESPONSE_HEADERS = "responseHeaders";
    public static final String BUNDLE_EXTRAS = "extras";  // Arbitrary data depending on use case. Used in the ContentDownloadService to allow Writers to return data
    
    public static final int STATUS_SUCCESS = 1;
    public static final int STATUS_FAILED_WITH_STALE_CACHE = 2;
    public static final int STATUS_SERVER_ERROR = 11;
    public static final int STATUS_TIMEOUT = 12;
    public static final int STATUS_INTERNAL_ERROR = 13;
    public static final int STATUS_NETWORK_OFFLINE = 14;
    
    private static final String CACHE_FILE = "FeedCache"; 
    private static final String CACHE_FIELD_TIMESTAMP = "FDS_Timestamp";
    private static final String CACHE_FIELD_FILE = "FDS_File";
    
    private static final long DEFAULT_CACHE_LIFETIME = 60 * 1000;
    private static final int DEFAULT_TIMEOUT = 10 * 1000;
    private static final int DEFAULT_MAX_ATTEMPTS = 1;
    private static final int DEFAULT_RETRY_DELAY = 2 * 1000;
    private static final boolean DEFAULT_FORCE_DOWNLOAD = false;
    private static final boolean DEFAULT_DO_NOT_CACHE = false;
    private static final boolean DEFAULT_DO_NOT_SEND_DATA_BACK = false;
    private static final int DEFAULT_METHOD = METHOD_AUTO;
    
    //Maximum length of a feed to be stored in the shared preferences.
    private static final int MAX_SHARED_PREF_SIZE = 6000;
    
    public static final int REQUEST_ID_UNKNOWN = Integer.MIN_VALUE;
    
    /**
     * Debug flag to always force feeds to download
     */
    private static boolean sUseCachedFeeds = true;
    
    private Context mAppContext;
    
    private HashMap<String, List<DownloadFeedTask>> mDownloadTasks;
    
    @Override
    public void onCreate() {
        super.onCreate();
        mDownloadTasks = new HashMap<String, List<DownloadFeedTask>>();        
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        
        mAppContext = getApplicationContext();
        
        //Check for empty extras
        Bundle extras = intent.getExtras();
        if (extras == null) {
            Log.i(TAG, "Service started without extras.");
            sendDownloadResult(createDownloadResult(
                    null, 
                    null,
                    STATUS_INTERNAL_ERROR
                    ));
            return START_NOT_STICKY;
        }
        
        //Parse extras
        DownloadSettings downloadSettings = createDownloadSettings(extras);
        
        //Delete is never something that should be cached. 
        if (downloadSettings.method == METHOD_DELETE) {
            downloadSettings.forceDownload = true;
            downloadSettings.doNotCache = true;
        }
        
        /**
         * Put should always be sent to the server, even if identical data is sent twice because put should be idempotent.
         * 
         * POST should always be sent as well, but many web APIs treat POST as equivalent to GET so to avoid caching, client
         * must specify EXTRA_FORCE_DOWNLOAD=true
         */
        if (downloadSettings.method == METHOD_PUT) {
            downloadSettings.forceDownload = true;
        }
        
        if (!downloadSettings.forceDownload && isCacheOk(mAppContext, downloadSettings.key, downloadSettings.cacheLifetime)) {
            //Send cached result
            Log.d(TAG, "Used Cache");
            sendDownloadResult(createDownloadResult(
                    downloadSettings, 
                    getCachedFeed(mAppContext, downloadSettings.key), 
                    STATUS_SUCCESS
                    ));
        } else {
            if (!NetworkTools.isNetworkConnected(this)) {
                //No connection, try for stale cache                
                downloadCompleted(createDownloadResult(
                        downloadSettings,
                        null, 
                        STATUS_NETWORK_OFFLINE
                        ), null);
            } else {
                //Perform download
                DownloadFeedTask downloadFeedTask = new DownloadFeedTask();
                if (!mDownloadTasks.containsKey(downloadSettings.key)) {
                    List<DownloadFeedTask> list = new ArrayList<DownloadFeedTask>();
                    list.add(downloadFeedTask);
                    mDownloadTasks.put(downloadSettings.key, list);
                } else {
                    mDownloadTasks.get(downloadSettings.key).add(downloadFeedTask);
                }
                downloadFeedTask.execute(downloadSettings);
            }
        }
            
        return START_NOT_STICKY;
    }
    
    
    /**
     * Create a new Download Results object. Hook to all subclasses to create their
     * own type
     * 
     * @param downloadSettings
     * @param jsonFeed
     * @param statusCode
     * @return
     */
    protected DownloadResult createDownloadResult(DownloadSettings downloadSettings, String jsonFeed, int statusCode) {
       return new DownloadResult(downloadSettings, jsonFeed, statusCode);
    }

    
    /**
     * Create a new Download Settings object. Hook to all subclasses to create their
     * own type
     * 
     * @param extras
     * @return
     */
    protected DownloadSettings createDownloadSettings(Bundle extras) {
        return new DownloadSettings(extras);
    }

    
    /**
     * Static method to perform simple feed download operation using defaults
     * 
     * @param context
     * @param listener
     * @param feedUrl
     * @param requestId
     */
    public static void downloadFeed(Context context, FeedDownloadListener listener, String feedUrl, int requestId) {
        downloadFeed(context, listener, feedUrl, requestId, DEFAULT_CACHE_LIFETIME);
    }
    
    /**
     * Static method to perform simple feed download operation with specific cache lifetime and defaults
     * 
     * @param context
     * @param listener
     * @param feedUrl
     * @param requestId
     * @param cacheLifetime
     */
    public static void downloadFeed(Context context, FeedDownloadListener listener, String feedUrl, int requestId, long cacheLifetime) {
        Intent intent = new Intent(context, FeedDownloadService.class);
        intent.putExtra(EXTRA_URL, feedUrl);
        intent.putExtra(EXTRA_REQUEST_ID, requestId);
        intent.putExtra(EXTRA_CACHE_LIFETIME, cacheLifetime);        
        if (listener != null) intent.putExtra(EXTRA_RECEIVER, new FeedDownloadResultReceiver(listener));        
        context.startService(intent);
    }
    
    /**
     * Static method to perform simple feed download operation with specific cache lifetime and defaults
     * 
     * @param context
     * @param listener
     * @param feedUrl
     * @param requestId
     * @param cacheLifetime
     * @param userAgent
     */
    public static void downloadFeed(Context context, FeedDownloadListener listener, String feedUrl, int requestId, long cacheLifetime, String userAgent) {
        Intent intent = new Intent(context, FeedDownloadService.class);
        intent.putExtra(EXTRA_URL, feedUrl);
        intent.putExtra(EXTRA_REQUEST_ID, requestId);
        intent.putExtra(EXTRA_CACHE_LIFETIME, cacheLifetime);        
        if (listener != null) intent.putExtra(EXTRA_RECEIVER, new FeedDownloadResultReceiver(listener));        
        intent.putExtra(EXTRA_USER_AGENT, userAgent);
        context.startService(intent);
    }

    /**
     * Static method to perform advanced feed download operation
     * 
     * @param context
     * @param listener
     * @param feedUrl
     * @param requestId
     * @param cacheLifetime
     * @param timeout
     * @param maxRetries
     * @param retryDelay
     * @param forceDownload
     * @param doNotCache
     */
    public static void downloadFeed(Context context, FeedDownloadListener listener, String feedUrl, int requestId, long cacheLifetime, int timeout, int maxRetries, int retryDelay, boolean forceDownload, boolean doNotCache) {
        Intent intent = new Intent(context, FeedDownloadService.class);
        intent.putExtra(EXTRA_URL, feedUrl);
        intent.putExtra(EXTRA_REQUEST_ID, requestId);
        intent.putExtra(EXTRA_CACHE_LIFETIME, cacheLifetime);
        intent.putExtra(EXTRA_TIMEOUT, timeout);
        intent.putExtra(EXTRA_MAX_ATTEMPTS, maxRetries);
        intent.putExtra(EXTRA_RETRY_DELAY, retryDelay);
        intent.putExtra(EXTRA_FORCE_DOWNLOAD, forceDownload);
        intent.putExtra(EXTRA_DO_NOT_CACHE, doNotCache);
        if (listener != null) intent.putExtra(EXTRA_RECEIVER, new FeedDownloadResultReceiver(listener));
        context.startService(intent);
    }
    
    /**
     * Send broadcast to receivers containing feed data.
     * @param downloadResult
     */
    public void sendDownloadResult(DownloadResult downloadResult) {
        if (downloadResult.resultReceiver == null) {
            Log.d(TAG, "resultReceiver was null");
            return;
        }
        
        Bundle data = new Bundle();
        data.putInt(BUNDLE_REQUEST_ID, downloadResult.requestId);
        data.putInt(BUNDLE_HTTP_STATUS, downloadResult.httpStatus);
        data.putString(BUNDLE_KEY, downloadResult.key);
        data.putByteArray(BUNDLE_RESPONSE_HEADERS, BRSerializer.serializeToByteArray(downloadResult.responseHeaders));
        
        if (downloadResult.extras != null) {
            data.putBundle(BUNDLE_EXTRAS, downloadResult.extras);
        }
        
        if (!downloadResult.doNotSendDataBack) {
            data.putString(BUNDLE_FEED, downloadResult.jsonFeed);
        }
        
        downloadResult.resultReceiver.send(downloadResult.statusCode, data);
    }
    
    /**
     * Remove from download hashmap, store in cache if required, and send result broadcast. 
     * @param result
     * @param downloadFeedTask
     */
    private void downloadCompleted(DownloadResult result, DownloadFeedTask downloadFeedTask) {

        //Remove this download task from the list if present.
        List<DownloadFeedTask> tasks = mDownloadTasks.get(result.key);
        if (tasks != null) {
            tasks.remove(downloadFeedTask);
            if (tasks.isEmpty()) {
                mDownloadTasks.remove(result.key);
            }
        }
        
        //On failure, try to get stale cached result.
        if (result.statusCode != STATUS_SUCCESS) {
            readFromStaleCache(result, downloadFeedTask);
        } 
        
        /**
         * Note: Earlier versions of FeedDownloadService stored the feed to cache at this point.
         * This was shifted to DownloadFeedTask to be done in the background thread
         * 
         * Maintaining code here in case that thread shift becomes a problem for some projects
         * 
        else if (!result.doNotCache) {
            storeFeed(result.key, result.jsonFeed);
        } **/
        
        sendDownloadResult(result);
    }
    
    /**
     * Reads a feeds from a stale cache
     * 
     * Subclasses can override caching behavior
     * 
     * @param result
     * @param downloadFeedTask
     */
    protected void readFromStaleCache(DownloadResult result, DownloadFeedTask downloadFeedTask) {
        result.jsonFeed = getCachedFeed(mAppContext, result.key);
        if (result.jsonFeed != null) {
            Log.d(TAG, "Sending Stale Cache");
            result.statusCode = STATUS_FAILED_WITH_STALE_CACHE;
        }
    }
    
    /**
     * Write the feed to cache with timestamp metadata.
     * @param key
     * @param json
     */
    public boolean storeFeed(DownloadResult result) {
        
        final String json = result.jsonFeed;
        final String key = result.key;
        
        SharedPreferences settings = this.getSharedPreferences(CACHE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        
        //Feeds over the size limit will not be stored in a shared preferences file, and will instead be written to cache files.
        if (json.length() > MAX_SHARED_PREF_SIZE) {
            String filePath = getLocalFilePath(getApplicationContext(), key);
            File file = new File(filePath);
            try {
                OutputStream out =  new BufferedOutputStream(new FileOutputStream(file), 8192);
                out.write(json.getBytes());
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            editor.putString(key + CACHE_FIELD_FILE, filePath);
            editor.remove(key);
        } else {
            editor.putString(key, json);
            editor.remove(key + CACHE_FIELD_FILE);
        }
        editor.putLong(key + CACHE_FIELD_TIMESTAMP, System.currentTimeMillis());
        editor.commit();
        return true;
    }
    
    /**
     * Manage deletion from SharedPreferences given context and a key.
     * External convenience method.
     * 
     * @param context
     * @param key
     */
    public static void deleteFeed(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(CACHE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        deleteFeed(settings, editor, key);
        editor.commit();
    }
    
    /**
     * Delete the feed specified by key from the cache
     * 
     * @param settings
     * @param editor
     * @param key
     */
    private static void deleteFeed(SharedPreferences settings, SharedPreferences.Editor editor, String key) {
        editor.remove(key + CACHE_FIELD_TIMESTAMP);
        if (settings.contains(key + CACHE_FIELD_FILE)) {
            File file = new File(settings.getString(key + CACHE_FIELD_FILE, null));
            if (file.exists()) file.delete();            
            editor.remove(key + CACHE_FIELD_FILE);
        }
        editor.remove(key);
    }
    
    /**
     * Delete cached feeds which have expired.
     * 
     * @param context
     * @param deleteLifetime
     */
    public static void deleteExpiredCache(Context context, long cacheLifetime) {
        SharedPreferences settings = context.getSharedPreferences(CACHE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        Map<String, ?> allSettings = settings.getAll();
        
        long now = System.currentTimeMillis();
        
        for(String key: allSettings.keySet()) {
            if (key.contains(CACHE_FIELD_TIMESTAMP)) {
                long lastUpdate = settings.getLong(key, 0);
                String rootKey = key.substring(0, key.length() - CACHE_FIELD_TIMESTAMP.length());
                if ((now - lastUpdate) > cacheLifetime) {
                    deleteFeed(settings, editor, rootKey);                    
                }
            }
        }
        editor.commit();
    }
    
    /**
     * Delete all cached feeds
     * 
     * @param context
     * @param deleteLifetime
     */
    public static void deleteAllCache(Context context) {
        SharedPreferences settings = context.getSharedPreferences(CACHE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        Map<String, ?> allSettings = settings.getAll();

        //Delete any cache files first
        for(String key: allSettings.keySet()) {
            if (key.contains(CACHE_FIELD_FILE)) {
                File file = new File(settings.getString(key, null));
                if (file.exists()) file.delete();
            }
        }
        
        //Clear all SharedPreferences and save
        editor.clear();
        editor.commit();
    }
    
    /**
     * Read the feed specified by the key from cache.
     * 
     * @param context
     * @param key
     * @return
     */
    public static String getCachedFeed(Context context, String key) {
        SharedPreferences settings = context.getSharedPreferences(CACHE_FILE, Context.MODE_PRIVATE);
        if (settings.contains(key + CACHE_FIELD_FILE)) {
            String filePath = settings.getString(key + CACHE_FIELD_FILE, null);
            FileReader fileReader;
            try {
                fileReader = new FileReader(filePath);
                StringBuilder sb = new StringBuilder();
                try {                
                    BufferedReader br = new BufferedReader(fileReader, 8 * 1024); 
                    String line;
                    
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    
                    return sb.toString();
                } catch (IOException e) {
                    deleteFeed(context, key);
                    Log.e(TAG, "IOException when reading cached file", e);
                    return null;
                } finally {
                    //Closing the file can throw an IOException which will be caught below.
                    fileReader.close();                    
                }                
            } catch (FileNotFoundException e) {
                deleteFeed(context, key);
                Log.e(TAG, "FileNotFoundException when reading cached file", e);
                return null;
            } catch (IOException e) {
                deleteFeed(context, key);
                Log.e(TAG, "IOException when closing cached file", e);
                return null;
            }            
        }
        return settings.getString(key, null);        
    }
    
    /**
     * Verify that the feed specified by the key is under cacheLifetime milliseconds old
     * 
     * @param context
     * @param key
     * @param cacheLifetime
     * @return
     */
    public static boolean isCacheOk(Context context, String key, long cacheLifetime) {
        if (!sUseCachedFeeds ) {
            return false;
        }
        
        SharedPreferences settings = context.getSharedPreferences(CACHE_FILE, Context.MODE_PRIVATE);
        long lastUpdate = settings.getLong(key + CACHE_FIELD_TIMESTAMP, 0);
        long now = System.currentTimeMillis();
        if ((now - lastUpdate) < cacheLifetime) {
            //Cache lifetime OK, check if cache file is OK.
            if (settings.contains(key + CACHE_FIELD_FILE)) {
                File file = new File(settings.getString(key + CACHE_FIELD_FILE, null));
                return file.exists();
            }
            return settings.contains(key);
        } else {
            return false;
        }
    }
    
    /**
     * Create local file path given the key for the feed
     * @param context
     * @param key
     * @return
     */
    private static String getLocalFilePath(Context context, String key) {
        return context.getCacheDir().getAbsolutePath() + "/" + key + ".feed";
    }
    
    /**
     * Download a feed with the specified DownloadSettings for timeout, cache, etc.
     */
    protected class DownloadFeedTask extends AsyncTask<DownloadSettings, Integer, DownloadResult> {

        private synchronized void syncWait(int delay) throws InterruptedException {
            wait(delay);
        }
        
        @Override
        protected DownloadResult doInBackground(DownloadSettings... params) {
            DownloadSettings downloadSettings = params[0];
            
            DownloadResult result = downloadFeed(downloadSettings);
            
            if (result.statusCode == STATUS_SUCCESS && !result.doNotCache) {
                storeFeed(result);
            }
            
            return result;
        }
        
        private DownloadResult downloadFeed(DownloadSettings downloadSettings) {
            
            DownloadResult result = createDownloadResult(
                    downloadSettings, 
                    null, 
                    STATUS_INTERNAL_ERROR
                    );
            
            Log.d(TAG, "Downloading: " + downloadSettings.url + " Method: " + downloadSettings.method);

            if (downloadSettings.url == null) {
                Log.i(TAG, "Null URL parameter");
                return result;
            }
            
            String fullUrl = NetworkTools.buildURL(downloadSettings.url, downloadSettings.queryParameters);
            
            HttpUriRequest request = null;
            
            //Auto detect method based on presence of postParameters
            if (downloadSettings.method == METHOD_AUTO) {
                if  (downloadSettings.postParameters != null || downloadSettings.multipartPostParameters != null) {                
                    downloadSettings.method = METHOD_POST;
                } else {
                    downloadSettings.method = METHOD_GET;
                }
            }
            
            switch (downloadSettings.method) {
                case METHOD_PUT:
                case METHOD_POST:
                    HttpEntityEnclosingRequestBase tmpRequest;
                    if (downloadSettings.method == METHOD_POST) {
                        tmpRequest = new HttpPost(URI.create(fullUrl));
                    } else {
                        tmpRequest = new HttpPut(URI.create(fullUrl));
                    }
                    try {
                        if (downloadSettings.multipartPostParameters != null && downloadSettings.multipartPostParameters.size() > 0) {
                            //Multipart Post/Put
                            String stringBoundary = "0xKhTmLbOuNdArY";
                            tmpRequest.setHeader("Content-Type", String.format("multipart/form-data; boundary=%s", stringBoundary));                
                            tmpRequest.setEntity(NetworkTools.dataForMultipartFormPostWithParameters(downloadSettings.postParameters, downloadSettings.multipartPostParameters, stringBoundary));                        
                        } else if (downloadSettings.postParameters != null && downloadSettings.postParameters.size() > 0) {
                            //Regular Post/Put
                            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                            for (String key : downloadSettings.postParameters.keySet()) {
                                nameValuePairs.add(new BasicNameValuePair(key, downloadSettings.postParameters.get(key)));
                            }                        
                            tmpRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));                        
                        } else {
                            Log.e(TAG, "An empty set of post or multipartpost parameters were supplied.");
                            return result;
                        }
                    } catch (UnsupportedEncodingException e) {
                        Log.e(TAG, "Unsupported Encoding ", e);
                        return result;
                    }
                    request = tmpRequest;
                    break;
                case METHOD_GET:
                    request = new HttpGet(URI.create(fullUrl));
                    break;
                case METHOD_DELETE:
                    request = new HttpDelete(URI.create(fullUrl));
                    break;
                default:
                    Log.e(TAG, "Unsupported method: " + downloadSettings.method);
                    return result;
            }
            
            //Always start with the proper user agent
            if (TextUtils.isEmpty(downloadSettings.userAgent)) {
                request.setHeader("User-Agent", NetworkTools.getUserAgentString(FeedDownloadService.this.getApplicationContext()));
            } else {
                request.setHeader("User-Agent", downloadSettings.userAgent);
            }
            
            //Add user-supplied headers possibly overriding user agent
            if (downloadSettings.headers != null && downloadSettings.headers.size() > 0) {
                for (String key: downloadSettings.headers.keySet()) {
                    request.setHeader(key, downloadSettings.headers.get(key));
                }
            }

            //Create a connection with the provided timeouts
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, downloadSettings.timeout);
            HttpConnectionParams.setSoTimeout(httpParams, downloadSettings.timeout);
            DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
    
            HttpResponse response;
            InputStream data;
            BufferedReader reader;
            //Retry failed download maxRetries times.
            for (int attempt=0; attempt < downloadSettings.maxAttempts; attempt++) {
                data = null;
                reader = null;
                
                try {
                    response = httpClient.execute(request);
                    result.httpStatus = response.getStatusLine().getStatusCode();
                    result.storeResponseHeaders(response.getAllHeaders());
                    if (result.httpStatus != 200) {
                        result.statusCode = STATUS_SERVER_ERROR;
                    } else {
                        data = response.getEntity().getContent();
                        reader = new BufferedReader(new InputStreamReader(data));
                    }
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (SocketTimeoutException e) {
                    Log.d(TAG, "Download timed out");
                    result.statusCode = STATUS_TIMEOUT;
                } catch (IOException e) {
                    e.printStackTrace();
                }
    
                //If connection was OK, perform copy of feed.
                if (reader != null) {
                    StringBuilder builder = new StringBuilder();
                    String line;
                    try {
                        while((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                        result.statusCode = STATUS_SUCCESS;
                        result.jsonFeed = builder.toString();
                        return result;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                
                //If it makes it to this point, download has failed.
                //If retry attempts remain, wait retryDelay before attempting.
                if (attempt < downloadSettings.maxAttempts - 1) {
                    try {
                        Log.d(TAG, "Retrying in " + downloadSettings.retryDelay + "ms");
                        syncWait(downloadSettings.retryDelay);
                    } catch (InterruptedException e) {
                        result.statusCode = STATUS_INTERNAL_ERROR;
                        e.printStackTrace();
                    }
                }
            }
            
            return result;
        }
        
        @Override
        protected void onPostExecute(DownloadResult result) {
             downloadCompleted(result, this);
        }
    }

    /**
     * Settings to provide to AsyncTask
     */
    protected static class DownloadSettings {
        public String url;
        public long cacheLifetime;
        public int timeout;
        public int maxAttempts;
        public int retryDelay;
        public boolean forceDownload;
        public boolean doNotCache;
        public boolean doNotSendDataBack;
        public ResultReceiver resultReceiver;
        public Map<String, String> headers;
        public Map<String, String> queryParameters;
        public Map<String, String> postParameters;
        public List<BRMultipartPostDataParameter> multipartPostParameters;
        public int requestId;
        public String key;
        public int method;
        public String userAgent;
        
        @SuppressWarnings("unchecked")
        public DownloadSettings (Bundle extras) {
            url = extras.getString(EXTRA_URL);
            cacheLifetime = extras.getLong(EXTRA_CACHE_LIFETIME, DEFAULT_CACHE_LIFETIME);
            timeout = extras.getInt(EXTRA_TIMEOUT, DEFAULT_TIMEOUT);
            maxAttempts = extras.getInt(EXTRA_MAX_ATTEMPTS, DEFAULT_MAX_ATTEMPTS);
            retryDelay = extras.getInt(EXTRA_RETRY_DELAY, DEFAULT_RETRY_DELAY);
            forceDownload = extras.getBoolean(EXTRA_FORCE_DOWNLOAD, DEFAULT_FORCE_DOWNLOAD);
            doNotCache = extras.getBoolean(EXTRA_DO_NOT_CACHE, DEFAULT_DO_NOT_CACHE);
            doNotSendDataBack = extras.getBoolean(EXTRA_DO_NOT_SEND_DATA_BACK, DEFAULT_DO_NOT_SEND_DATA_BACK);
            resultReceiver = (ResultReceiver) extras.get(EXTRA_RECEIVER);
            requestId = extras.getInt(EXTRA_REQUEST_ID);
            method = extras.getInt(EXTRA_METHOD, DEFAULT_METHOD);
            userAgent = extras.getString(EXTRA_USER_AGENT);
            
            if (extras.containsKey(EXTRA_HEADERS)) {
                headers = (Map<String, String>) BRSerializer.deserializeFromByteArray(extras.getByteArray(EXTRA_HEADERS)); 
            }
            if (extras.containsKey(EXTRA_POST)) {
                postParameters = (Map<String, String>) BRSerializer.deserializeFromByteArray(extras.getByteArray(EXTRA_POST)); 
            }
            if (extras.containsKey(EXTRA_QUERY)) {
                queryParameters = (Map<String, String>) BRSerializer.deserializeFromByteArray(extras.getByteArray(EXTRA_QUERY)); 
            }
            if (extras.containsKey(EXTRA_MULTIPART_POST)) {
                multipartPostParameters = (List<BRMultipartPostDataParameter>) BRSerializer.deserializeFromByteArray(extras.getByteArray(EXTRA_MULTIPART_POST));
                if (multipartPostParameters != null) {
                    for (BRMultipartPostDataParameter param: multipartPostParameters) {
                        if (param.data == null) {
                            //Load and serialize file here because it is too large to send in an Intent
                            param.data = BRSerializer.serializeFileToByteArray(param.fileName);
                            if (param.data != null) {
                                Log.v(TAG, "Param " + param.fileName + " is " + param.data.length + " bytes ");
                            }
                        }
                    }
                } else {
                    Log.e(TAG, "An EXTRA_MULTIPART_POST was provided, but is null");
                }
            }
            
            key = getKeyFromParameters(url, extras.getByteArray(EXTRA_QUERY), extras.getByteArray(EXTRA_POST), method);
        }
    }
    
    /**
     * Result from AsyncTask
     */
    protected static class DownloadResult {
        public String jsonFeed;
        public int statusCode;
        public boolean doNotCache;
        public boolean doNotSendDataBack;
        public ResultReceiver resultReceiver;
        public int requestId;
        public int httpStatus;      
        public String key;
        public Map<String, String> responseHeaders;
        public String url;
        public Bundle extras;
        
        public DownloadResult (DownloadSettings downloadSettings, String jsonFeed, int statusCode) {            
            this.jsonFeed = jsonFeed;
            this.statusCode = statusCode;
            if (downloadSettings != null) {
                this.resultReceiver = downloadSettings.resultReceiver;
                this.requestId = downloadSettings.requestId;
                this.doNotCache = downloadSettings.doNotCache;
                this.doNotSendDataBack = downloadSettings.doNotSendDataBack;
                this.key = downloadSettings.key;
                this.url = downloadSettings.url;
            }
            this.httpStatus = 0;
            responseHeaders = new HashMap<String, String>();
        }
        
        public void storeResponseHeaders(Header[] headers) {
            if (headers != null && headers.length > 0) {
                for (Header header: headers) {
                    responseHeaders.put(header.getName(), header.getValue());
                }
            }
        }
    }
    
    /* Default required binder */
    
    public class CachingDownloadServiceBinder extends Binder {
        FeedDownloadService getService() {
            return FeedDownloadService.this;
        }
    }

    private final IBinder mCachingDownloadServiceBinder = new CachingDownloadServiceBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return mCachingDownloadServiceBinder;
    }
    
    /******Connection to calling object******/
    
    /**
     * Listen for notifications from FeedDownloadService upon download completion/faliure
     * @author adam.newman
     *
     */
    public interface FeedDownloadListener {
        /**
         * Handle the result of a download operation
         * @param success
         * @param resultCode
         * @param resultData
         */
        void onFeedDownloadResult(int requestId, boolean success, int resultCode, Bundle resultData);
    }
    
    /**
     * Client process' side of download communication extends ResultReceiver to provide IPC.
     * @author adam.newman
     *
     */
    public static class FeedDownloadResultReceiver extends ResultReceiver {
        
        WeakReference<FeedDownloadListener> mListener;
        
        public FeedDownloadResultReceiver(FeedDownloadListener listener) {
            this(new Handler(), listener);
        }
        
        public FeedDownloadResultReceiver(Handler handler, FeedDownloadListener listener) {
            super(handler);
            mListener = new WeakReference<FeedDownloadListener>(listener);           
        }
        
        protected void onReceiveResult (int resultCode, Bundle resultData) {
            FeedDownloadListener listener = null;
            if (mListener != null) {
                listener = mListener.get();
            } 
            
            if (mListener == null || listener == null) {
                Log.w(TAG, "FeedDownloadResultReceiver.onReceiveResult() - null listener error.");
                return;
            }
            
            int requestId = FeedDownloadService.REQUEST_ID_UNKNOWN;
            if (resultData != null) {
                requestId = resultData.getInt(FeedDownloadService.BUNDLE_REQUEST_ID);
            }
            
            switch (resultCode) {
                case FeedDownloadService.STATUS_SUCCESS:
                case FeedDownloadService.STATUS_FAILED_WITH_STALE_CACHE:
                    mListener.get().onFeedDownloadResult(requestId, true, resultCode, resultData);
                    break;
                default:
                    mListener.get().onFeedDownloadResult(requestId, false, resultCode, resultData);
                    break;
            }
        }
    }
    
    /*******Key Generation*******/    
    /**
     * Generate a hex String of an MD5 hash from the provided data.
     * @param data
     * @return 
     */
    public static String getKeyFromData(byte[] data) {
        MessageDigest digest;
        try {
            digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(data);
            return byteArrayToHexString(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            //Famous last words: this shouldn't happen
            Log.e(TAG, "MD5 algorithm is missing!");
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Convert a byte array into a hex string
     * @param data
     * @return 
     */
    public static String byteArrayToHexString(byte[] data) {
        StringBuilder hex = new StringBuilder(data.length * 2);
        for (byte b: data) {
            hex.append(String.format("%02x", b));
        }
        return hex.toString();
    }
    
    /**
     * Combine two byte arrays into one and return it.
     * @param a
     * @param b
     * @return
     */
    public static byte[] concatByteArrays(byte[] a, byte[] b) {
       byte[] c= new byte[a.length + b.length];
       System.arraycopy(a, 0, c, 0, a.length);
       System.arraycopy(b, 0, c, a.length, b.length);
    
       return c;
    }
    
    /**
     * Generate an MD5 hash of url plus query and/or post parameters
     * @param url
     * @param queryParametersRaw
     * @param postParametersRaw
     * @return MD5 hash of the raw bytes of the parameters and url
     */
    public static String getKeyFromParameters (String url, byte[] queryParametersRaw, byte[] postParametersRaw, int method) {
        byte[] methodBytes = Integer.toString(method).getBytes();
        
        if (queryParametersRaw != null && postParametersRaw != null) {
            return getKeyFromData(concatByteArrays(methodBytes, concatByteArrays(url.getBytes(), concatByteArrays(queryParametersRaw, postParametersRaw))));
        } else if (postParametersRaw != null) {
            return getKeyFromData(concatByteArrays(methodBytes, concatByteArrays(url.getBytes(), postParametersRaw)));
        } else if (queryParametersRaw != null) {
            return getKeyFromData(concatByteArrays(methodBytes, concatByteArrays(url.getBytes(), queryParametersRaw)));
        }
        
        return getKeyFromData(concatByteArrays(methodBytes, url.getBytes()));
    }
    
    /**
     * Convert a byte array into a Map<String, String> object
     * @param b
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map<String, String> deserializeMapFromByteArray(byte[] b) {
        return (Map<String, String>) BRSerializer.deserializeFromByteArray(b);
    }
}

