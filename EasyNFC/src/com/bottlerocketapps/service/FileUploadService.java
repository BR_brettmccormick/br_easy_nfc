package com.bottlerocketapps.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import com.bottlerocketapps.tools.BRSerializer;
import com.bottlerocketapps.tools.NetworkTools;
import com.bottlerocketapps.tools.NetworkTools.BRMultipartPostDataParameter;

/**
 * Perform HTTP Multipart POST with files of arbitrarily large size without requiring a large memory footprint and providing
 * upload progress indication.
 * 
 * @author adam.newman
 */
public class FileUploadService extends Service {

    private static final String TAG = FileUploadService.class.getSimpleName();    

    public static final String CANONICAL_NAME = FileUploadService.class.getCanonicalName();
    public static final String EXTRA_URL = CANONICAL_NAME + ".URL";
    public static final String EXTRA_TIMEOUT = CANONICAL_NAME + ".timeout";
    public static final String EXTRA_MAX_ATTEMPTS = CANONICAL_NAME + ".maxAttempts";
    public static final String EXTRA_RETRY_DELAY = CANONICAL_NAME + ".retryDelay";
    public static final String EXTRA_DO_NOT_SEND_DATA_BACK = CANONICAL_NAME + ".doNotSendDataBack";
    public static final String EXTRA_RECEIVER = CANONICAL_NAME + ".receiver";
    public static final String EXTRA_HEADERS = CANONICAL_NAME + ".headers";
    public static final String EXTRA_POST = CANONICAL_NAME + ".post";
    public static final String EXTRA_MULTIPART_POST = CANONICAL_NAME + ".multipart";
    public static final String EXTRA_REQUEST_ID = CANONICAL_NAME + ".id";
    public static final String EXTRA_USER_AGENT = CANONICAL_NAME + ".userAgent";    
    public static final String EXTRA_ABORT_UPLOAD_KEY = CANONICAL_NAME + ".abortUploadKey";

    public static final String BUNDLE_REQUEST_ID = "requestId";
    public static final String BUNDLE_KEY = "key";
    public static final String BUNDLE_HTTP_STATUS = "httpStatus";
    public static final String BUNDLE_FEED = "feed";
    public static final String BUNDLE_RESPONSE_HEADERS = "responseHeaders";
    public static final String BUNDLE_PROGRESS_PERCENT = "progressPercent";
    public static final String BUNDLE_PROGRESS_RATE = "progressRate";

    public static final int STATUS_SUCCESS = 1;    
    public static final int STATUS_SERVER_ERROR = 11;
    public static final int STATUS_TIMEOUT = 12;
    public static final int STATUS_INTERNAL_ERROR = 13;
    public static final int STATUS_NETWORK_OFFLINE = 14;
    public static final int STATUS_CLIENT_ABORTED = 15;
    public static final int STATUS_PROGRESS_UPDATE = 21;

    private static final int DEFAULT_TIMEOUT = 10 * 1000;
    private static final int DEFAULT_MAX_ATTEMPTS = 1;
    private static final int DEFAULT_RETRY_DELAY = 2 * 1000;
    private static final boolean DEFAULT_DO_NOT_SEND_DATA_BACK = false;    

    private static final String MULTIPART_BOUNDARY = "0xKhTmLbOuNdArY"; //String unlikely to appear as --MULTIPART_BOUNDARY in the data.
    private static final int BUFFER_SIZE = 8 * 1024;                    //File read buffer size in bytes.
    private static final int UPDATE_INTERVAL = 1 * 1000;                //Interval to send update to result receiver in ms. 
    private static final int UPLOAD_CHUNK_SIZE = 16 * 1024;             //Upload size per chunk. Smaller chunk size allows more frequent progress updates on slow networks 16KB is a good middle ground.

    private HashMap<String, List<FileUploadTask>> mUploadTasks;

    @Override
    public void onCreate() {
        super.onCreate();
        mUploadTasks = new HashMap<String, List<FileUploadTask>>();        
    }
    
    /**
     * Static method to initiate upload. 
     * 
     * @param context               Context used to send the startService intent
     * @param feedUrl               URL to post against
     * @param listener              Listener for file upload status and completion events
     * @param requestId             ID for client tracking of multiple requests
     * @param multipartParameters   List of multipart post objects, i.e. file meta info.
     * @param postParameters        Normal form fields to post. Can be null.
     * @param headers               Additional headers. Can be null.
     */
    public static void uploadFile(Context context,  FileUploadListener listener, String feedUrl, int requestId, 
            List<BRMultipartPostDataParameter> multipartParameters, HashMap<String,String> postParameters, HashMap<String, String> headers) {        
        Intent intent = new Intent(context, FileUploadService.class);
        intent.putExtra(EXTRA_URL, feedUrl);
        intent.putExtra(EXTRA_REQUEST_ID, requestId);        
        intent.putExtra(EXTRA_MULTIPART_POST, BRSerializer.serializeToByteArray(multipartParameters));
        if (postParameters != null) intent.putExtra(EXTRA_POST, BRSerializer.serializeToByteArray(postParameters));
        if (headers != null) intent.putExtra(EXTRA_HEADERS, BRSerializer.serializeToByteArray(headers));        
        if (listener != null) intent.putExtra(EXTRA_RECEIVER, new FileUploadResultReceiver(listener));
        
        context.startService(intent);
    }
    
    /**
     * Abort upload for the supplied URL. 
     * 
     * @param context               Context used to send startService intent
     * @param feedUrl               URL to cancel 
     */
    public static void abortUpload(Context context, String feedUrl) {
        String key = createKey(feedUrl);
        Intent intent = new Intent(context, FileUploadService.class);
        intent.putExtra(FileUploadService.EXTRA_ABORT_UPLOAD_KEY, key);
        context.startService(intent);
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        
        //Check for empty extras
        Bundle extras = intent.getExtras();
        if (extras == null) {
            Log.e(TAG, "Service started without extras.");
            shutdownIfNothingLeft();
            return START_NOT_STICKY;            
        }

        //Abort all uploads for supplied key if requested
        String abortUploadKey = intent.getStringExtra(EXTRA_ABORT_UPLOAD_KEY);
        if (abortUploadKey != null) {
            abortUploadOfKey(abortUploadKey);
            return START_NOT_STICKY;
        }

        //Parse extras
        UploadSettings uploadSettings = new UploadSettings(extras);

        if (!NetworkTools.isNetworkConnected(this)) {
            sendUploadResult(new UploadResult(uploadSettings, null, STATUS_NETWORK_OFFLINE));
        } else {
            FileUploadTask fileUploadTask = new FileUploadTask();
            if (!mUploadTasks.containsKey(uploadSettings.key)) {
                List<FileUploadTask> list = new ArrayList<FileUploadTask>();
                list.add(fileUploadTask);
                mUploadTasks.put(uploadSettings.key, list);
            } else {
                mUploadTasks.get(uploadSettings.key).add(fileUploadTask);
            }
            fileUploadTask.execute(uploadSettings);
        }        

        return START_NOT_STICKY;
    }
    
    /**
     * If there are no pending upload tasks, stop the service.
     */
    private void shutdownIfNothingLeft() {
        if (mUploadTasks == null || mUploadTasks.isEmpty()) {
            stopSelf();
        }
    }

    /**
     * Cancel all uploads matching key.
     * @param key
     */
    private void abortUploadOfKey(String key) {
        List<FileUploadTask> tasks = mUploadTasks.get(key);
        if (tasks != null) {
            for (FileUploadTask task : tasks) {
                task.cancel(false);
            }
            mUploadTasks.remove(key);
        }        
        shutdownIfNothingLeft();
    }
    
    /**
     * Send the result of the upload to the client
     * 
     * @param result
     */
    private void sendUploadResult(UploadResult result) {
        if (result.resultReceiver == null) return;

        Bundle data = new Bundle();
        data.putInt(BUNDLE_REQUEST_ID, result.requestId);
        data.putInt(BUNDLE_HTTP_STATUS, result.httpStatus);
        data.putString(BUNDLE_KEY, result.key);
        data.putByteArray(BUNDLE_RESPONSE_HEADERS, BRSerializer.serializeToByteArray(result.responseHeaders));

        if (!result.doNotSendDataBack && result.jsonFeed != null) {
            data.putString(BUNDLE_FEED, result.jsonFeed);
        }

        result.resultReceiver.send(result.statusCode, data);
        
        shutdownIfNothingLeft();
    }
    
    /**
     * Remove this upload task from the list and send the result.
     * 
     * @param result
     * @param fileUploadTask
     */
    private void uploadCompleted(UploadResult result, FileUploadTask fileUploadTask) {

        //Remove this download task from the list if present.
        List<FileUploadTask> tasks = mUploadTasks.get(result.key);
        if (tasks != null) {
            tasks.remove(fileUploadTask);
            if (tasks.isEmpty()) {
                mUploadTasks.remove(result.key);
            }
        }

        sendUploadResult(result);
    }

    private class FileUploadTask extends AsyncTask<UploadSettings, Long, UploadResult> {

        private UploadSettings mUploadSettings;
        private long mAverageRate = 0;
        private long mLastUpdateTime = 0;
        private long mLastUpdateBytes = 0;

        private synchronized void syncWait(int delay) throws InterruptedException {
            wait(delay);
        }
        
        @Override
        protected UploadResult doInBackground(UploadSettings... params) {
            mUploadSettings = params[0];

            UploadResult result = new UploadResult(
                    mUploadSettings, 
                    null, 
                    STATUS_INTERNAL_ERROR
                    );

            InputStream responseInStream;
            BufferedReader responseReader;
            HttpURLConnection connection;
            byte[] buffer = new byte[BUFFER_SIZE];
            for (int attempt=0; attempt < mUploadSettings.maxAttempts; attempt++) { 
                responseInStream = null;
                responseReader = null;
                connection = null;

                try {
                    URL url = new URL(mUploadSettings.url);                
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setDoInput(true);
                    connection.setDoOutput(true);
                    connection.setConnectTimeout(mUploadSettings.timeout);
                    connection.setReadTimeout(mUploadSettings.timeout);

                    //Without this, write operations to the output stream will be buffered in memory and crash depending on available memory and bandwidth.
                    connection.setChunkedStreamingMode(UPLOAD_CHUNK_SIZE);    
                    
                    //Always start with the proper user agent
                    if (TextUtils.isEmpty(mUploadSettings.userAgent)) {
                        connection.setRequestProperty("User-Agent", NetworkTools.getUserAgentString(FileUploadService.this.getApplicationContext()));
                    } else {
                        connection.setRequestProperty("User-Agent", mUploadSettings.userAgent);
                    }

                    //Add client supplied headers (cookies/etc)
                    if (mUploadSettings.headers != null) {
                        for (String key: mUploadSettings.headers.keySet()) {
                            connection.setRequestProperty(key, mUploadSettings.headers.get(key));
                        }
                    }
                    
                    //This will always be a mulitpart post
                    connection.setRequestProperty("Content-Type", String.format("multipart/form-data; boundary=%s", MULTIPART_BOUNDARY));
                    
                    //Connection opened now. No further modification to connection options or request headers allowed.
                    connection.connect();
                    BufferedOutputStream requestOutStream = new BufferedOutputStream(connection.getOutputStream());
                    
                    //Write normal post values
                    if (mUploadSettings.postParameters != null) {
                        for (String key: mUploadSettings.postParameters.keySet()) {
                            requestOutStream.write(String.format("--%s\r\n", MULTIPART_BOUNDARY).getBytes());
                            requestOutStream.write(String.format("Content-Disposition: form-data; name=\"%s\"\r\n\r\n", key).getBytes());
                            requestOutStream.write(mUploadSettings.postParameters.get(key).getBytes());
                            requestOutStream.write("\r\n".getBytes());
                        }
                    }
                    
                    //Upload files
                    if (mUploadSettings.multipartPostParameters != null) {
                        for (BRMultipartPostDataParameter parameter: mUploadSettings.multipartPostParameters) {
                            File inputFile = new File(parameter.fileName);
                            if (inputFile.exists()) {
                                long startTime = System.currentTimeMillis();
                                long now = System.currentTimeMillis();
                                long lastProgressTimestamp = now;
                                                                
                                int bytesRead = 0;
                                long bytesTotal = inputFile.length();
                                long bytesSent = 0;
                                
                                requestOutStream.write(String.format("--%s\r\n", MULTIPART_BOUNDARY).getBytes());
                                requestOutStream.write(String.format("Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n", parameter.fieldName, parameter.fileName).getBytes());
                                requestOutStream.write(String.format("Content-Type: %s\r\n\r\n", parameter.contentType).getBytes());

                                //Write the file to the server.
                                InputStream fileInStream = new BufferedInputStream(new FileInputStream(inputFile), BUFFER_SIZE);
                                while ((bytesRead = fileInStream.read(buffer)) >= 0 && !isCancelled()) {
                                    if (bytesRead > 0) {
                                        requestOutStream.write(buffer, 0, bytesRead);            
                                        bytesSent += bytesRead;                                        
                                    }
                                    
                                    //Only publish progress when UPDATE_INTERVAL is exceeded. Too much IPC overhead can effectively tie upload speed to CPU speed.
                                    now = System.currentTimeMillis();
                                    if (now - lastProgressTimestamp > UPDATE_INTERVAL) {                                        
                                        this.publishProgress(bytesSent, bytesTotal);
                                        lastProgressTimestamp = now;
                                    }
                                }
                                fileInStream.close();
                                requestOutStream.write("\r\n".getBytes());
                                
                                float duration = (float)(System.currentTimeMillis() - startTime) / 1000.0f;
                                Log.v(TAG, "Uploaded " + bytesSent + "B @ " + ((float)bytesSent / 1024.0f / duration) + "KB/s");
                            } else {
                                Log.w(TAG, "File did not exist: " + parameter.fileName);
                            }
                        }
                    } else {
                        Log.w(TAG, "No files were specified for upload.");
                    }
                    
                    //If aborted, do not complete boundary so server will reject this post.
                    if (!isCancelled()) requestOutStream.write(String.format("--%s--\r\n", MULTIPART_BOUNDARY).getBytes());                    
                    //Nothing left to send. 
                    requestOutStream.close();
                    
                    //Open response reader
                    responseInStream = connection.getInputStream();
                    result.httpStatus = connection.getResponseCode();
                    result.storeResponseHeaders(connection.getHeaderFields());
                    if (result.httpStatus != 200) {
                        result.statusCode = STATUS_SERVER_ERROR;
                    } else {
                        responseReader = new BufferedReader(new InputStreamReader(responseInStream), BUFFER_SIZE);
                    }
                } catch (ClientProtocolException e) {
                    Log.e(TAG, "ClientProtocolException during upload", e);
                } catch (MalformedURLException e) {
                    Log.e(TAG, "MalformedURLException during upload", e);
                } catch (IllegalStateException e) {
                    Log.e(TAG, "IllegalStateException during upload", e);
                } catch (SocketTimeoutException e) {
                    result.statusCode = STATUS_TIMEOUT;
                    Log.e(TAG, "Upload timeout", e);
                } catch (IOException e) {
                    Log.e(TAG, "IOException on upload", e);
                } 
                
                //If upload was OK, perform copy of feed.
                if (responseReader != null && !isCancelled()) {                    
                    StringBuilder builder = new StringBuilder();
                    String line;
                    try {
                        while((line = responseReader.readLine()) != null) {
                            builder.append(line);
                        }
                        responseReader.close();
                        result.statusCode = STATUS_SUCCESS;
                        result.jsonFeed = builder.toString();
                        if (connection != null) {
                            connection.disconnect();
                        }
                        return result;
                    } catch (IOException e) {
                        Log.e(TAG, "Exception when receiving response.", e);
                    } 
                }
                
                if (connection != null) {
                    connection.disconnect();
                }
                
                //If it makes it to this point, transfer has failed.
                //If retry attempts remain, wait retryDelay before attempting.
                if (attempt < mUploadSettings.maxAttempts - 1 && !isCancelled()) {
                    try {
                        Log.d(TAG, "Retrying in " + mUploadSettings.retryDelay + "ms");
                        syncWait(mUploadSettings.retryDelay);
                    } catch (InterruptedException e) {
                        result.statusCode = STATUS_INTERNAL_ERROR;
                        Log.w(TAG, "Upload thread interrupted before retry", e);
                    }
                }
                
                if (isCancelled()) {
                    result.statusCode = STATUS_CLIENT_ABORTED;
                    break;
                }
            }

            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLastUpdateTime = System.currentTimeMillis();
        }

        @Override        
        protected void onProgressUpdate (Long... values) {
            super.onProgressUpdate(values);
            /*
             * Send progress thus far to the listener.<br/>
             * Values array should be in the order: bytes sent, total bytes
             */
            if (mUploadSettings != null && values.length == 2) {
                long bytesSent = values[0];
                long bytesTotal = values[1];

                long currentTime = System.currentTimeMillis();
                long bytesPerSecond = (long)((float)(bytesSent - mLastUpdateBytes) / ((float)(currentTime - mLastUpdateTime) / 1000.0f));
                
                mLastUpdateBytes = bytesSent;
                mLastUpdateTime = currentTime;

                //Weight speed 9 parts running average, 1 part current rate after initial value is recorded.
                mAverageRate = (mAverageRate > 0) ? (mAverageRate * 9 + bytesPerSecond) / 10 : bytesPerSecond;  

                Bundle resultData = new Bundle();

                resultData.putInt(BUNDLE_REQUEST_ID, mUploadSettings.requestId);
                resultData.putFloat(BUNDLE_PROGRESS_PERCENT, (float) bytesSent / (float) bytesTotal);
                resultData.putLong(BUNDLE_PROGRESS_RATE, mAverageRate);
                mUploadSettings.resultReceiver.send(STATUS_PROGRESS_UPDATE, resultData);                
            }
        }

        @Override
        protected void onPostExecute(UploadResult result) {
            super.onPostExecute(result);
            uploadCompleted(result, this);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (mUploadSettings != null) {
                sendUploadResult(new UploadResult(mUploadSettings, null, STATUS_CLIENT_ABORTED));
            }
        }
    }

    /**
     * Settings to provide to AsyncTask
     */
    private class UploadSettings {
        public String url;
        public int timeout;
        public int maxAttempts;
        public int retryDelay;
        public boolean doNotSendDataBack;
        public ResultReceiver resultReceiver;
        public Map<String, String> headers;
        public Map<String, String> postParameters;
        public List<BRMultipartPostDataParameter> multipartPostParameters;
        public int requestId;
        public String key;
        public String userAgent;

        @SuppressWarnings("unchecked")
        public UploadSettings (Bundle extras) {
            url = extras.getString(EXTRA_URL);
            timeout = extras.getInt(EXTRA_TIMEOUT, DEFAULT_TIMEOUT);
            maxAttempts = extras.getInt(EXTRA_MAX_ATTEMPTS, DEFAULT_MAX_ATTEMPTS);
            retryDelay = extras.getInt(EXTRA_RETRY_DELAY, DEFAULT_RETRY_DELAY);
            doNotSendDataBack = extras.getBoolean(EXTRA_DO_NOT_SEND_DATA_BACK, DEFAULT_DO_NOT_SEND_DATA_BACK);
            resultReceiver = (ResultReceiver) extras.get(EXTRA_RECEIVER);
            requestId = extras.getInt(EXTRA_REQUEST_ID);
            userAgent = extras.getString(EXTRA_USER_AGENT);

            if (extras.containsKey(EXTRA_HEADERS)) {
                headers = (Map<String, String>) BRSerializer.deserializeFromByteArray(extras.getByteArray(EXTRA_HEADERS)); 
            }
            if (extras.containsKey(EXTRA_POST)) {
                postParameters = (Map<String, String>) BRSerializer.deserializeFromByteArray(extras.getByteArray(EXTRA_POST)); 
            }
            if (extras.containsKey(EXTRA_MULTIPART_POST)) {
                multipartPostParameters = (List<BRMultipartPostDataParameter>) BRSerializer.deserializeFromByteArray(extras.getByteArray(EXTRA_MULTIPART_POST));
            }

            key = createKey(url);            
        }
    }

    /**
     * Result from AsyncTask
     */
    private class UploadResult {
        public String jsonFeed;
        public int statusCode;
        public boolean doNotSendDataBack;
        public ResultReceiver resultReceiver;
        public int requestId;
        public int httpStatus;      
        public String key;
        public Map<String, List<String>> responseHeaders;

        public UploadResult (UploadSettings uploadSettings, String jsonFeed, int statusCode) {            
            this.jsonFeed = jsonFeed;
            this.statusCode = statusCode;
            if (uploadSettings != null) {
                this.resultReceiver = uploadSettings.resultReceiver;
                this.requestId = uploadSettings.requestId;                
                this.doNotSendDataBack = uploadSettings.doNotSendDataBack;
                this.key = uploadSettings.key;
            }
            this.httpStatus = 0;            
        }

        public void storeResponseHeaders(Map<String,List<String>> headers) {
            responseHeaders = headers;            
        }
    }

    /******Connection to calling object******/

    /**
     * Listen for notifications from FeedDownloadService upon download completion/faliure
     * @author adam.newman
     *
     */
    public interface FileUploadListener {
        /**
         * Handle the result of an upload operation
         * @param success
         * @param statusCode
         * @param resultData
         */
        void onFileUploadResult(boolean success, int statusCode, Bundle resultData);

        /**
         * Handle the result of a progress increment 
         * @param requestId
         * @param percentage
         * @param bytesPerSecond
         */
        void onFileUploadProgress(int requestId, float percentage, long bytesPerSecond);
    }

    /**
     * Client process' side of download communication extends ResultReceiver to provide IPC.
     * @author adam.newman
     *
     */
    public static class FileUploadResultReceiver extends ResultReceiver {

        WeakReference<FileUploadListener> mListener;

        public FileUploadResultReceiver(FileUploadListener listener) {
            this(new Handler(), listener);
        }

        public FileUploadResultReceiver(Handler handler, FileUploadListener listener) {
            super(handler);
            mListener = new WeakReference<FileUploadListener>(listener);           
        }

        protected void onReceiveResult (int statusCode, Bundle resultData) {
            FileUploadListener listener = null;
            if (mListener != null) {
                listener = mListener.get();
            } 

            if (mListener == null || listener == null) {
                Log.w(TAG, "FeedDownloadResultReceiver.onReceiveResult() - null listener error.");
                return;
            }

            switch (statusCode) {
                case FileUploadService.STATUS_PROGRESS_UPDATE:
                    listener.onFileUploadProgress(resultData.getInt(BUNDLE_REQUEST_ID), resultData.getFloat(BUNDLE_PROGRESS_PERCENT), resultData.getLong(BUNDLE_PROGRESS_RATE));
                    break;
                case FileUploadService.STATUS_SUCCESS:                
                    listener.onFileUploadResult(true, statusCode, resultData);
                    break;
                default:
                    listener.onFileUploadResult(false, statusCode, resultData);
                    break;
            }
        }        
    }

    /**
     * Create key used for upload task map and upload abort. 
     * 
     * @param url
     * @return
     */
    public static String createKey(String url) {
        return url;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}

