package com.bottlerocketapps.service;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.Loader;
import android.text.TextUtils;

import com.bottlerocketapps.service.FeedDownloadService.FeedDownloadListener;
import com.bottlerocketapps.tools.Log;
import com.bottlerocketapps.tools.NetworkTools;

/**
 * A loader that downloads a feed using the ContentDownloadService, and then 
 * creates a cursor to read the downloaded data after the ContentDownloadService
 * has written the result back to the ContentProvider 
 *   
 * This is a 5 step process:
 *   
 *   Step 1 (Optional):  Subclasses can implement their own "pre-step"
 *      mRemoteUrl must be set when this step is complete
 *   
 *   Step 2: Check the network state
 *   
 *   Step 3: Download a feed and write it to a ContentProvider using the ContentDownloadService
 *   
 *   Step 4: Load a cursor to read the results written in Step 3
 *      Subclasses will get a callback here to set the cursor query before this is run
 *      
 *   Step 5: Deliver the new cursor
 *      Subclasses will get a callback here to read/alter/wrap the cursor before delivery
 *      All cursors are wrapped with ContentCursor before delivery so error codes can be read
 *      Once delivered, any old cursor will be closed 
 *   
 * 
 * @author Josh
 *
 */
public class ContentLoader extends Loader<ContentCursor> implements FeedDownloadListener {

    private static final String TAG = "ContentLoader";
    
    private static final long NETWORK_TEST_TIMEOUT_MS = 2000;
    
    /**
     * The url of the remote feed that is going to be downloaded
     * 
     * Note: At this time, only GET feeds are supported (not POST) 
     */
    private String mRemoteUrl;
    
    /**
     * FeedDownloadService download extras
     * @see FeedDownloadService
     */
    private Bundle mDownloadExtras;
    
    /**
     * Once the feed has been downloaded, the ContentDownloadService will instatiated
     * an instance of the Writer class to write the feed to a ContentProvider
     */
    private Class<?> mWriterClass;

    /**
     * Extras/argument that are used to create the ContentWriter
     */
    private Bundle mWriterBundle;
    
    /**
     * The parameters of the local ContentProvider cursor query that will run after the
     * content has been downloaded and written to the ContentProvider
     */
    private LocalQuery mLocalQuery;
    
    
    /**
     * Keeps track of the download/load results
     */
    protected ContentLoaderResult mResult;

    
    /**
     * A reference to the valid, previously loaded cursor.  Will be redelivered instead of
     * recreating a new cursor
     */
    private ContentCursor mCursor = null;
    

    /**
     * I can't figure out what this does.  But CursorLoader does it, so we will too
     */
    private ForceLoadContentObserver mForceLoadObserver;

    private LoadCursorTask mLoadCursorTask;
    
    private Handler mHandler;

    private boolean mRerunQuery;
    
    /**
     * Struct for holding the local query used to create a Cursor
     * @author josh.ehlke
     *
     */
    public static class LocalQuery {
        Uri uri;
        String[] projection;
        String selection;
        String[] selectionArgs;
        String sortOrder;
        
        public String getSelection() {
            return selection;
        }
        
        public void setSelection(String selection) {
            this.selection = selection;
        }
        
        public String getSortOrder() {
            return this.sortOrder;
        }

        public void setSortOrder(String sortOrder) {
           this.sortOrder = sortOrder;
        }
        
        // TODO: Getters/setters for other fields
    }
    
    
    /**
     * Struct for holding the ContentDownloadService result locally
     * 
     * @author josh.ehlke
     *
     */
    public static class ContentLoaderResult {

        Bundle resultBundle;
        int requestId;
        int resultStatus;
        boolean success;

        public String getRawFeed() {
            return resultBundle.getString(FeedDownloadService.BUNDLE_FEED);
        }

        public boolean isSuccessful() {
            return success;
        }
    }


    public ContentLoader(Context context, String remoteUrl, Class<?> writerClass) {
        this(context, remoteUrl, writerClass, null, null);
    }
    
    public ContentLoader(Context context, String remoteUrl, Class<?> writerClass, Bundle writerInitBundle) {
        this(context, remoteUrl, writerClass, writerInitBundle, null);
    }
    
    public ContentLoader(Context context, String remoteUrl, Class<?> writerClass, Bundle writerBundle, Bundle downloadExtras) {
        super(context);
        
        mRemoteUrl = remoteUrl;
        mWriterClass = writerClass;
        mWriterBundle = writerBundle;
        mDownloadExtras = downloadExtras;
        
        mForceLoadObserver = new ForceLoadContentObserver();
        
        mHandler = new Handler();
    }
    
  
    

    

    /**
     * Lets subclasses get to the Handler
     * @return
     */
    protected Handler getHandler() {
        return mHandler;
    }
    
    
    @Override
    protected void onForceLoad() {
        Log.d(TAG, "onForceLoad");
        super.onForceLoad();
    }

    
    @Override
    final protected void onStartLoading() {
        
        Log.d(TAG, "onStartLoading");
        
        mResult = new ContentLoaderResult();
        
        if (needToPerformPreLoad()) {
            startPreLoad();
        } else {
            testNetwork();
        }
    }
    
    /**
     * Lets subclasses indicate they would like to do an extra pre-load
     * step before anything else happens
     * 
     * @return
     */
    protected boolean needToPerformPreLoad() {
        return false;
    }
    
    /**
     * Step 1
     * 
     * Lets subclasses do something before anything else is done
     */
    protected void startPreLoad() {
        return;
    }
    
    /**
     * Call back from subclasses to indicate their pre-load step has
     * completed.
     * 
     * If a failure is returned, the rest of the steps will be cancelled, and an empty ContentCursor
     * with a failure code will be delivered as the result
     * 
     * @param failureCode LOADER_SUCCESS if success, another value if a failure happened
     */
    protected void preloadFinished(int failureCode) {
        
        if (failureCode != ContentCursor.LOADER_SUCCESS) {
            
            ContentCursor failureCursor = createFailureCursor(failureCode);
        
            deliverResult(failureCursor);
            
        } else {
            // move on to step 2
            testNetwork();
        }
    }


    /**
     * Step 2
     * 
     * Check to see if the Network is connected.  If not, set a timer to check again in a little bit
     */
    protected void testNetwork() {
        
        if (NetworkTools.isNetworkConnected(getContext())) {
            //  Start the ContentLoader to being downloading
            beginDownload();
            
        } else {
            // Post a timer, and wait a second
            startNetworkTestTimeout();
        }
    }
    
    
    private void startNetworkTestTimeout() {
        stopNetworkTestTimeout();
        mHandler.postDelayed(mNetworkTestTimeoutRunnable, NETWORK_TEST_TIMEOUT_MS);
    }

    
    private void stopNetworkTestTimeout() {
        mHandler.removeCallbacks(mNetworkTestTimeoutRunnable);
    }
    
    
    private Runnable mNetworkTestTimeoutRunnable = new Runnable() {

        @Override
        public void run() {
            onNetworkTestTimeout();
        }
    };
    
    
    protected void onNetworkTestTimeout() {
        if (NetworkTools.isNetworkConnected(getContext())) {
            //  Start the ContentCursorLoader to being downloading
            beginDownload();
        } else {
            deliverResult(new ContentCursor(ContentCursor.LOADER_FAILURE_NO_CONNECTION));
        }
    }

    
    /**
     * Step 3
     * 
     * Downloads content using the ContentDownloadService
     * 
     */
    protected void beginDownload() {
        
        boolean downloadContent = false;
        
        if (ContentDownloadService.shouldDownloadContentAgain(getContext(), mRemoteUrl)) {
            downloadContent = true;
        }
        
        if (shouldSkipDownload()) {
            downloadContent = false;
        }
        
        if (downloadContent) {
            Log.d(TAG, "downloading...");
            deliverSearchingCursor();
            ContentDownloadService.downloadContent(getContext(), getId(), this, mRemoteUrl, mWriterClass, mWriterBundle, mDownloadExtras);
        } else {
            Log.d(TAG, "skipping download...");
            loadCursor();
        }
    }
    
    /**
     * Callback from the ContentDownloadService after the content has been downloaded and written to the ContentProvider
     */
    
    @Override
    public void onFeedDownloadResult(int requestId, boolean success, int resultCode, Bundle resultData) {
        Log.d(TAG, "onFeedDownloadResult");     
       
        mRerunQuery = true;
        
        mResult.requestId = requestId;
        mResult.resultBundle = resultData;
        mResult.resultStatus = resultCode;
        mResult.success = success;
        
        loadCursor();
    }
    
    
    
    /**
     * Allows subclass loaders to override the behavior of the ContentDownloadService cache,
     * allowing them to skip a download even if the basic cache is out of date.
     * 
     * ContentLoader subclasses can check their own content provider to see if recent information
     * is already stored in their content providers
     * 
     * @return
     */
    protected boolean shouldSkipDownload() {
        return false;
    }
    
    
    /**
     * Lets subclasses change the remote url after the loader has been created
     * @param remoteUrl
     */
    protected void setRemoteUrl(String remoteUrl) {
        
        if (TextUtils.equals(mRemoteUrl, remoteUrl)) {
            
            if (mRemoteUrl != null) {
                Log.w(TAG, "changing remote url");
                closeCursor();
            }
        }
        
        mRemoteUrl = remoteUrl;
    }
    
    /**
     * Step 4
     * 
     * This is called after the ContentDownloadService has been completed, and we are ready 
     * to load the content in a cursor.  (Or called if the download step is skipped).
     */
    private void loadCursor() {
        
        onPreLoadCursor();
        
        if (mLocalQuery == null || mLocalQuery.uri == null) {
            throw new RuntimeException("A LocalQuery must be set before starting the ContentLoader");
        }
   
        if (takeContentChanged()) {
            Log.d(TAG, "takeContentChanged returned true -- what does that mean?");
            closeCursor();
        }
        
        if (mCursor != null && !mRerunQuery) {
            
            Log.d(TAG, "redeliving existing cursor");
            
            if (mCursor.mReason == ContentCursor.LOADER_SEARCHING) {
                mCursor.mReason = ContentCursor.LOADER_SUCCESS;
            }
            
            deliverResult(mCursor);
            
        } else {
            
            mRerunQuery = false;
            
            mLoadCursorTask = new LoadCursorTask() {
                @Override protected void onPostExecute(Cursor result) {
                    onLoadCursorTaskFinished(this, result);
                }   
            };
            
            mLoadCursorTask.execute(mLocalQuery);
        }   
    }
    
    protected LocalQuery getLocalQuery() {
        return mLocalQuery;
    }
    
    
    /**
     * Specify the ContentProvider query to use after the content has been downloaded (or the download
     * was skipped). This query is returned as a Cursor
     * 
     * @param uri
     * @param projection
     * @param selection
     * @param selectionArgs
     * @param sortOrder
     */
    public void setLocalQuery(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        
        if (mLocalQuery != null) {
            Log.w(TAG, "changing the local query");
        }
        
        mLocalQuery = new LocalQuery();
        mLocalQuery.uri = uri;
        mLocalQuery.projection = projection;
        mLocalQuery.selection = selection;
        mLocalQuery.selectionArgs = selectionArgs;
        mLocalQuery.sortOrder = sortOrder;
    }
    
    
    /**
     * Lets Loader subclasses get the Writer result extras
     * @return
     */
    public Bundle getWriterResultExtras() {
        if (mResult != null && mResult.resultBundle != null) {
            return mResult.resultBundle.getBundle(ContentDownloadService.BUNDLE_EXTRAS);
        }
        return null;
    }
    
    
    /**
     * Lets subclasses do some something right before
     * the local cursor is loaded.   This can be used to 
     * modify the local query selection and other parameters if they aren't
     * known until after a download.
     */
    protected void onPreLoadCursor() {
        // TODO Auto-generated method stub
        
    }
    
    
    /**
     * Simple task that queries for a Cursor from a ContentProvider on a background thread (step 4)
     * @author josh.ehlke
     *
     */
    private class LoadCursorTask extends AsyncTask<LocalQuery, Integer, Cursor> {

        @Override
        protected Cursor doInBackground(LocalQuery... params) {
            
            LocalQuery localQuery = params[0];
            
            Cursor cursor = getContext().getContentResolver().query(
                    localQuery.uri, localQuery.projection, localQuery.selection, localQuery.selectionArgs, localQuery.sortOrder);
            
            if (cursor != null) {
                // Ensure the cursor window is filled
                cursor.getCount();
                cursor.registerContentObserver(mForceLoadObserver);
            }
            
            return cursor;
        }
    }
    

    /**
     * Callback when the AsyncTask that is creating/loading the Cursor query returns
     * 
     * @param loadCursorTask
     * @param result
     */
    private void onLoadCursorTaskFinished(LoadCursorTask loadCursorTask, Cursor result) {
        
        if (loadCursorTask == mLoadCursorTask) {
            mLoadCursorTask = null;
        } else {
            Log.w(TAG, "weird.");
        }
        
        Cursor modifiedCursor = onLoadCursorFinished(result);
        
        // All cursors delivered by a ContentLoader must be ContentCursors
        // Subclasses may have already wrapped the result in a ContentCursor
        ContentCursor contentCursor;
        if (!(modifiedCursor instanceof ContentCursor)) {
            contentCursor = new ContentCursor(result);
        } else {
            contentCursor = (ContentCursor)modifiedCursor;
        }
        
        deliverResult(contentCursor);
    }

    
    /**
     * Allows subclasses to modify the cursor or wrap it in a CursorWrapper 
     * before its delivered
     */
    protected Cursor onLoadCursorFinished(Cursor cursor) {
        return cursor;
    }
    
    /**
     * Delivers a cursor to the listeners to indicate that a longer 
     * search is happening.  If there was an existing cursor, that cursor
     * will be re-sent with a SEARCHING flag
     */
    protected void deliverSearchingCursor() {
        ContentCursor searchingCursor = createFailureCursor(ContentCursor.LOADER_SEARCHING);
        deliverResult(searchingCursor);
    }
    
    /**
     * Sends a ContentCursor with a failure code to the listeners
     * 
     * If a previously cached cursor exists, that cursor is sent with a failure code.
     * By doing this, listeners can choose to handle or ignore errors by displaying the old results 
     * 
     * @param failureCode
     * @return
     */
    private ContentCursor createFailureCursor(int failureCode) {
        
        if (mCursor != null) {
            // Send the existing results with a failure code
            // The loader listener can choose to use the existing results if it wishes
            // and ignore the error
            
            mCursor.mReason = failureCode;
            return mCursor;
            
        } else {
            
            return new ContentCursor(failureCode);
        }
    }

    
    /**
     * Step 5
     * 
     * Deliver the new cursor to the listeners
     * 
     * All cursors are always wrapped in a instance of the ContentCursor class.
     * Listeners are free to cast the Cursor to a ContentCursor and use its APIs
     * to first check for error codes if desired
     */
    @Override
    final public void deliverResult(ContentCursor newCursor) {
        
        if (!(newCursor instanceof ContentCursor)) {
            throw new RuntimeException("expected ContentCursor");
        }
        
        // Deliver the new cursor
        super.deliverResult(newCursor);
        
        // Sometimes the same cursor is being redelivered.  Don't close if that's happening
        if (mCursor != newCursor) {
            
            // Close the previous cursor if its still open
            closeCursor();
            
            // Save this cursor for future use
            mCursor = (ContentCursor)newCursor;
        }      
    }

    /**
     * Private to close and forget about a previous cursor
     * 
     * This should not be called until after a new cursor (or at least a "null" cursor) 
     * is delivered to the listeners. They must be told to not use the old cursor before its
     * closed out from underneath them
     */
    private void closeCursor() {
        
        Log.d(TAG, "closeCursor");
        
        if (mCursor != null) {
            if (!mCursor.isClosed()) {
                mCursor.close();
            }
            mCursor = null;
        }
    }

    
    @Override
    protected void onStopLoading() {
        Log.d(TAG, "onStopLoading");
        super.onStopLoading();
        
        // Attempt to stop the cursor loading task if it is running
        if (mLoadCursorTask != null) {
            mLoadCursorTask.cancel(true);
        }
    }

    @Override
    protected void onAbandon() {
        Log.d(TAG, "onAbandon");
        super.onAbandon();
    }


    @Override
    protected void onReset() {
        Log.d(TAG, "onReset");
        super.onReset();
        
        // Ensure the loader is stopped
        onStopLoading();
        
        closeCursor();
    }


    @Override
    public void onContentChanged() {
        Log.d(TAG, "onContentChanged");
        
        /* Called when Loader.ForceLoadContentObserver detects a change. The default 
         * implementation checks to see if the loader is currently started; 
         * if so, it simply calls forceLoad(); otherwise, it sets a flag so 
         * that takeContentChanged() returns true. */
        
        // Note from Josh: I don't really understand what to do here.  The CursorLoader class
        // sets a flag, but it doesn't seem to use it.  This will probabaly need to be revisted in 
        // the future
        
        super.onContentChanged();
    }

}
