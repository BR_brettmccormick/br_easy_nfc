package com.bottlerocketapps.service;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.os.Bundle;

/**
 * A Cursor returned by the ContentLoader
 * 
 * A "smart" cursor that wraps the actual result cursor, but adds a failure reason
 * to communicate why a cursor may have failed or to indicate that a loader is still 
 * searching/downloading some data
 * 
 * @author josh.ehlke
 *
 */
public class ContentCursor extends CursorWrapper {
    
    public static final int LOADER_SEARCHING = -1;
    public static final int LOADER_SUCCESS = 0;
    public static final int LOADER_FAILURE_NO_CONNECTION = 2;
    
    /**
     * Subclasses of ContentCursor should start their reason values here
     * to avoid conflicts
     */
    protected static final int LOADER_FAILURE_SUB_CLASS_BEGIN = 100;
    
    
    public static final String EXTRA_CONTENT_FAILURE_REASON = "extra_reason";
    
    protected int mReason = LOADER_SUCCESS;
    
    public ContentCursor(Cursor cursor) {
        super(cursor);
    }

    public ContentCursor(int reason) {
        super(null);
        mReason = reason;
    }
    
    public boolean isFailure() {
        return mReason > LOADER_SUCCESS;
    }
    
    public boolean isStillSearching() {
        return mReason == LOADER_SEARCHING;
    }  
    
    public int getFailure() {
        return mReason;
    }

    @Override
    public Bundle getExtras() {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_CONTENT_FAILURE_REASON, mReason);
        return bundle;
    }
    
    private boolean wrappingNullCursor() {
        return mReason != LOADER_SUCCESS;
    }

    @Override
    public boolean isClosed() {
        if (wrappingNullCursor()) {
            return true;
        }          
        return super.isClosed();
    }

    @Override
    public int getCount() {
        if (wrappingNullCursor()) {
            return 0;
        }
        return super.getCount();
    }
    
}
