package com.bottlerocketapps.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

/**
 * ContentDownloadService is a service that can be used to download text
 * from a remote service, and store it to ContentProvider
 * 
 * It inherits FeedDownloadService and works by overriding the cache methods
 * to write out to a content provider instead of to flat files/SharedPreferences
 * 
 * It also uses it own mechanism to see if the "cache is okay" to 
 * 
 * Note: It does not fully support multipart/post urls. In particular, the mechanism to determining the
 * last time a feed was downloaded will not work
 * 
 * 
 * @author Josh
 *
 */
public class ContentDownloadService extends FeedDownloadService {
    
    public static final String TAG = "ContentDownloadService";

    public static final String CANONICAL_NAME = FeedDownloadService.class.getCanonicalName();

    private static final String EXTRA_WRITER_CLASS = CANONICAL_NAME + ".writerClass";

    private static final String EXTRA_WRITER_BUNDLE = CANONICAL_NAME + ".writerBundle";

    private static final String HISTORY_FILE = "ContentDownloadServiceHistory";

    private static final long DEFAULT_TIME_BETWEEN_CONTENT_DOWNLOADS = 30 * 1000;   // TODO: Set to a reasonable amount

    public static void downloadContent(Context context, int requestId, FeedDownloadListener listener, String contentUrl, Class<?> writer) {
        downloadContent(context, requestId, listener, contentUrl, writer, null);
    }

    public static void downloadContent(Context context, int requestId, FeedDownloadListener listener, String contentUrl, Class<?> writer, Bundle writerBundle) {

        Intent intent = new Intent(context, ContentDownloadService.class);
        intent.putExtra(EXTRA_URL, contentUrl);
        intent.putExtra(EXTRA_REQUEST_ID, requestId);
        intent.putExtra(EXTRA_DO_NOT_SEND_DATA_BACK, true);
        intent.putExtra(EXTRA_WRITER_CLASS, writer);
        intent.putExtra(EXTRA_WRITER_BUNDLE, writerBundle);
        intent.putExtra(EXTRA_FORCE_DOWNLOAD, true);
        
        // This is a FeedDownload value
        intent.putExtra(EXTRA_MAX_ATTEMPTS, 2);

        if (listener != null) intent.putExtra(EXTRA_RECEIVER, new FeedDownloadResultReceiver(listener));  
        context.startService(intent);
    }
    
    public static void downloadContent(Context context, int requestId, FeedDownloadListener listener, String contentUrl, Class<?> writer, Bundle writerBundle, Bundle downloadExtras) {
        Intent intent = new Intent(context, ContentDownloadService.class);
        intent.putExtra(EXTRA_URL, contentUrl);
        intent.putExtra(EXTRA_REQUEST_ID, requestId);
        intent.putExtra(EXTRA_DO_NOT_SEND_DATA_BACK, true);
        intent.putExtra(EXTRA_WRITER_CLASS, writer);
        intent.putExtra(EXTRA_WRITER_BUNDLE, writerBundle);
        intent.putExtra(EXTRA_FORCE_DOWNLOAD, true);
        
        
        // This is a FeedDownload value
        intent.putExtra(EXTRA_MAX_ATTEMPTS, 2);

        if (downloadExtras != null) {
            intent.putExtras(downloadExtras);
        }

        if (listener != null) intent.putExtra(EXTRA_RECEIVER, new FeedDownloadResultReceiver(listener));  
        context.startService(intent);
    }
    
    public static boolean shouldDownloadContentAgain(Context context, String contentUrl) {
        return shouldDownloadContentAgain(context, contentUrl, DEFAULT_TIME_BETWEEN_CONTENT_DOWNLOADS);
    }
    
    public static boolean shouldDownloadContentAgain(Context context, String contentUrl, long timeBetweenDownloadsMs) {
        long lastDownload = getTimeOfLastDownload(context, contentUrl);
        boolean shouldDownload =  ((System.currentTimeMillis() - lastDownload) > timeBetweenDownloadsMs);
        return shouldDownload;
    }
    
    public static long getTimeOfLastDownload(Context context, String contentUrl) {
        String key = contentUrl;
        SharedPreferences settings = context.getSharedPreferences(HISTORY_FILE, Context.MODE_PRIVATE);
        return settings.getLong(key, 0);
    }
    
    public static boolean recordDownloadInHistory(Context context, String contentUrl) {
        String key = contentUrl;
        SharedPreferences.Editor editor =  context.getSharedPreferences(HISTORY_FILE, Context.MODE_PRIVATE).edit();
        editor.putLong(key, System.currentTimeMillis());
        return editor.commit();
    }
    
    public static boolean clearDownloadFromHistory(Context context, String contentUrl) {
        String key = contentUrl;
        SharedPreferences.Editor editor =  context.getSharedPreferences(HISTORY_FILE, Context.MODE_PRIVATE).edit();
        editor.remove(key);
        return editor.commit();
    }
    
    public static boolean clearAllDownloadHistory(Context context) {
        SharedPreferences.Editor editor =  context.getSharedPreferences(HISTORY_FILE, Context.MODE_PRIVATE).edit();
        editor.clear();
        return editor.commit();
    }

    @Override
    public boolean storeFeed(FeedDownloadService.DownloadResult result) {

        int code = ContentWriter.UNKNOWN_FAILURE;

        if (!(result instanceof DownloadResult)) {
            throw new RuntimeException("bad DownloadResult");
        }

        final DownloadResult downloadResult = (DownloadResult)result;

        try {
            ContentWriter writer = downloadResult.writerClass.newInstance();
            
            if (downloadResult.writerInitBundle != null) {
                writer.initialize(downloadResult.writerInitBundle);
            }
           
            if (downloadResult.extras == null) {
            	downloadResult.extras = new Bundle();
            }
            code = writer.go(this, result.jsonFeed, downloadResult.extras);

        } catch (InstantiationException e) {
            Log.w(TAG, "could not create writer", e);
        } catch (IllegalAccessException e) {
            Log.w(TAG, "could not create writer", e);
        }
        
        Log.d(TAG, "post writer");
        
        recordDownloadInHistory(this, result.url);

        return code == ContentWriter.SUCCESS;
    }
    


    @Override
    protected void readFromStaleCache(FeedDownloadService.DownloadResult result, FeedDownloadService.DownloadFeedTask downloadFeedTask) {
        // For the Content Download service, we have no cache.  So don't read from it, or change the result
        // Perhaps the caller meant to call a History method?
        Log.w(TAG, "ContentDownloadService does not cache things.");
        return;
    }


    @Override
    protected FeedDownloadService.DownloadSettings createDownloadSettings(Bundle extras) {
        return new DownloadSettings(extras);
    }

    protected static class DownloadSettings extends FeedDownloadService.DownloadSettings {

        public Class<ContentWriter> writerClass;
        public Bundle writerInitBundle;

        @SuppressWarnings("unchecked")
        public DownloadSettings(Bundle extras) {
            super(extras);
            writerClass = (Class<ContentWriter>) extras.getSerializable(EXTRA_WRITER_CLASS);
            writerInitBundle = extras.getBundle(EXTRA_WRITER_BUNDLE);
        }

    }

    @Override
    protected FeedDownloadService.DownloadResult createDownloadResult(FeedDownloadService.DownloadSettings downloadSettings, String jsonFeed, int statusCode) {
        return new DownloadResult(downloadSettings, jsonFeed, statusCode);
    }

    protected static class DownloadResult extends FeedDownloadService.DownloadResult {

        public Class<ContentWriter> writerClass;
        public Bundle writerInitBundle;

        public DownloadResult (FeedDownloadService.DownloadSettings downloadSettings, String jsonFeed, int statusCode) {
            super(downloadSettings, jsonFeed, statusCode);
            if (downloadSettings instanceof DownloadSettings) {
                writerClass = ((DownloadSettings)downloadSettings).writerClass;
                writerInitBundle = ((DownloadSettings)downloadSettings).writerInitBundle;
            }
        }
    }

    public abstract static class ContentWriter {

        public static final int SUCCESS = 0;
        public static final int UNKNOWN_FAILURE  = -1;
        public static final int PARSE_FAILURE = -2;

        /**
         * 
         * @param context
         * @param feed   The entire text of the downloaded feed
         * @param writerResultBundle  A bundle that the writer can write to do return simple result data back to the caller
         * @return
         */
        public abstract int go(Context context, String feed, Bundle extras);
        
        public abstract void initialize(Bundle bundle);

    }


}
