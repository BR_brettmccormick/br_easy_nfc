package com.bottlerocketapps.tools;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class NetworkTools {
    
    public static final String TAG = NetworkTools.class.getSimpleName();
    
    private static final int TYPE_WIMAX = 0x00000006;

    public static boolean isWifiConnected(Context context) {
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (!wifiManager.isWifiEnabled()) {
                return false;
            }
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            return wifiInfo.getSupplicantState().equals(SupplicantState.COMPLETED);
        } catch (SecurityException e) {
            Log.e(TAG, "Missing permission to check wifi.", e);
        }
        return false;
    }

    public static boolean isLTEConnected(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getNetworkType() == 0x0000000d;  // NETWORK_TYPE_LTE is introduced in API 11
    }

    public static boolean isWiMaxConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getNetworkInfo(TYPE_WIMAX);
        if (info == null) {
            return false;
        }
        return info.isConnected();
    }

    public static boolean is2GConnected(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        switch (telephonyManager.getNetworkType()) {

            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return true;

            default:
                return false;
        }
    }

    public static boolean is3GOrBetterConnected(Context context) {
        if (isNetworkConnected(context)) {
            if (is2GConnected(context)) {
                return isWifiConnected(context);
            }
            return true;
        }
        return false;
    }

    public static boolean is4GOrBetterConnected( Context context) {
        return (isWifiConnected(context) || isWiMaxConnected(context) || isLTEConnected(context));
    }
    
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr != null) {
            NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                return (activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public static HttpEntity dataForMultipartFormPostWithParameters(Map<String, String> parameters, List<BRMultipartPostDataParameter> multipartPostParameters, String stringBoundary) throws UnsupportedEncodingException {
        ByteArrayBuffer output = new ByteArrayBuffer(0);
        
        if (parameters != null && parameters.size() > 0) {
            for (String key : parameters.keySet()) {
                output = appendString(output, String.format("--%s\r\n", stringBoundary));
                output = appendString(output, String.format("Content-Disposition: form-data; name=\"%s\"\r\n\r\n", key));
                output = appendString(output, parameters.get(key));
                output = appendString(output, "\r\n");
            }
        }
        
        if (multipartPostParameters != null && multipartPostParameters.size() > 0) {
            for (BRMultipartPostDataParameter parameter : multipartPostParameters) {
                output = appendString(output, String.format("--%s\r\n", stringBoundary));
                output = appendString(output, String.format("Content-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n", parameter.fieldName, parameter.fileName));
                output = appendString(output, String.format("Content-Type: %s\r\n\r\n", parameter.contentType));
                output.append(parameter.data, 0, parameter.data.length);
                output = appendString(output, "\r\n");
            }
        }
        output = appendString(output, String.format("--%s--\r\n", stringBoundary));
        ByteArrayEntity bae = new ByteArrayEntity(output.toByteArray());
        
        return bae;
    }
    
    private static ByteArrayBuffer appendString(ByteArrayBuffer buffer, String string) {
        if (string == null) return buffer;
        try { 
            byte[] bytes = string.getBytes("UTF-8"); 
            buffer.append(bytes, 0, bytes.length); 
        } catch (UnsupportedEncodingException e) { 
            e.printStackTrace(); 
        }
        return buffer;
    }
    
    public static class BRMultipartPostDataParameter implements Serializable {
        private static final long serialVersionUID = 266105669609461806L;
        
        public String fieldName;
        public String fileName;
        public String contentType;
        public byte[] data;
    }
    
    public static String buildURL(String urlBase, Map<String, String> queryParameters) {
        if (urlBase == null) return null;
        
        if (queryParameters == null || queryParameters.size() == 0) return urlBase;
        
        StringBuilder url = new StringBuilder(urlBase + "?");
        for (String key : queryParameters.keySet()) {
            url.append(URLEncoder.encode(key) + "=" + URLEncoder.encode(queryParameters.get(key)) + "&");
        }        
        return url.subSequence(0, url.length() - 1).toString();
    }
    
    /**
     * Get the phone's web browser User-Agent string. Attempt to do this without creating the WebView and 
     * if that fails, create and retrieve it from a WebView instance.
     * 
     * @param context
     * @return
     */
    public static String getUserAgentString(Context context) {
        try {
            Constructor<WebSettings> constructor = WebSettings.class.getDeclaredConstructor(Context.class, WebView.class);
            constructor.setAccessible(true);
            try {
                WebSettings settings = constructor.newInstance(context, null);                
                return settings.getUserAgentString();
            } finally {
                constructor.setAccessible(false);
            }
        } catch (Exception e) {
            Log.w(TAG, "Could not use prefferred method to retrieve User Agent");
            try {
                return new WebView(context).getSettings().getUserAgentString();
            } catch (Exception e2) {
                Log.w(TAG, "Exception using secondary method of retrieving User Agent", e2);
                return "BR-Android";
            }
        }
    }
    
}
