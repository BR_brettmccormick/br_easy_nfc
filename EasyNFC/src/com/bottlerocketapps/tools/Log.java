package com.bottlerocketapps.tools;


/**
 * A wrapper class for the Android log library that lets Bottlerocket apps better
 * control Logging behavior or provide hooks for over-the-air reporting, etc
 * 
 * @author josh.ehlke
 *
 */
public class Log {
    
    private static boolean sLogging = true;
    private static boolean sDebugLogging = true /*BuildConfig.DEBUG */;
    
    private static boolean sPrintExceptionTraces = true;

    public static void initLogging(boolean enabled) {
        sLogging = enabled;
        sDebugLogging = enabled;
    }
    
    public static void d(String tag, String msg) {
        if (sDebugLogging) {
            android.util.Log.d(tag, msg);
        }
    }
    
    public static void d(String tag, String msg, Throwable e) {
        if (sDebugLogging) {
            android.util.Log.d(tag, msg, e);
        }
              
        if (sDebugLogging && sPrintExceptionTraces) {
            e.printStackTrace();
        }
    }
    public static void e(String tag, String msg) {
        if (sLogging) {
            android.util.Log.e(tag, msg);
        }
    }
    
    public static void e(String tag, String msg, Throwable e) {
        if (sLogging) {
            android.util.Log.e(tag, msg, e);
        }  
        
        if (sPrintExceptionTraces) {
            e.printStackTrace();
        }
    }
    public static void v(String tag, String msg) {
        if (sDebugLogging) {
            android.util.Log.v(tag, msg);
        }
    }
    
    public static void v(String tag, String msg, Throwable e) {
        if (sDebugLogging) {
            android.util.Log.v(tag, msg, e);
        }
        
        if (sDebugLogging && sPrintExceptionTraces) {
            e.printStackTrace();
        }
    }
    public static void i(String tag, String msg) {
        if (sLogging) {
            android.util.Log.i(tag, msg);
        }
        
    }
    
    public static void i(String tag, String msg, Throwable e) {
        if (sLogging) {
            android.util.Log.i(tag, msg, e);
        }
        
        if (sPrintExceptionTraces) {
            e.printStackTrace();
        }
    }
    
    public static void w(String tag, String msg) {
        if (sLogging) {
            android.util.Log.w(tag, msg);
        }
    }
    
    public static void w(String tag, String msg, Throwable e) {
        if (sLogging) {
            android.util.Log.w(tag, msg, e);
        }
        
        if (sPrintExceptionTraces) {
            e.printStackTrace();
        }
    }
    
    /* 
     * Log.wft was introduced in API level 8
     * 
    public static void wtf(String tag, String msg) {
        if (sLogging) {
            android.util.Log.wtf(tag, msg);
        }
    }
    
    public static void wtf(String tag, String msg, Throwable e) {
        if (sLogging) {
            android.util.Log.wtf(tag, msg, e);
        }
        
        if (sPrintExcetionTraces) {
            e.printStackTrace();
        }
    } */
}
