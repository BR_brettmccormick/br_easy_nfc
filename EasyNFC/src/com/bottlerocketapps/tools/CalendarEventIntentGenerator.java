package com.bottlerocketapps.tools;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.text.TextUtils;

/**
 * Create an intent which will send the user to a pre-populated calendar event ready to be added to any calendar
 * supported by their device. Tested with various devices from 2.2 to 4.0.
 * 
 * @author adam.newman
 */
public class CalendarEventIntentGenerator {
    
    /**
     * Generate a test calendar intent and use PackageManager.queryIntentActivities to determine if this device supports it.
     * @param context
     * @return
     */
    public static boolean isIntentSupported(Context context) {
        CalendarEvent testEvent = new CalendarEvent(System.currentTimeMillis() + 5 * 60 * 1000, System.currentTimeMillis() + 20 * 60 * 1000, false, true, "Test Event");
        Intent intent = generateIntent(context, testEvent);
        
        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> launchables=pm.queryIntentActivities(intent, 0);
        
        return launchables.size() > 0;
    }
    
    /**
     * Generate a calendar event creation intent using CalendarContract if on ICS or later. Fall
     * back to undocumented method on older versions of the OS.
     * 
     * @param context
     * @param event
     * @return
     */
    public static Intent generateIntent(Context context, CalendarEvent event) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return getNewIntent(context, event);
        } else {
            return getOldIntent(context, event);
        }
    }
    
    /**
     * Create a Google approved calendar event Intent.
     * @param context
     * @param event
     * @return
     */
    private static Intent getNewIntent(Context context, CalendarEvent event) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            return null;
        }
        Intent intent = new Intent(Intent.ACTION_INSERT)
        .setData(Events.CONTENT_URI)
        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, event.getStartTime())
        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, event.getEndTime())
        .putExtra(Events.AVAILABILITY, event.isBusy() ? Events.AVAILABILITY_BUSY : Events.AVAILABILITY_FREE)
        .putExtra(Events.TITLE, event.getTitle());
        
        if (!TextUtils.isEmpty(event.getDescription())) {
            intent.putExtra(Events.DESCRIPTION, event.getDescription());
        }
        if (!TextUtils.isEmpty(event.getLocation())) {
            intent.putExtra(Events.EVENT_LOCATION, event.getLocation());
        }
        if (!TextUtils.isEmpty(event.getInvitees())) {
            intent.putExtra(Intent.EXTRA_EMAIL, event.getInvitees());
        }               
        return intent;
    }
    
    /**
     * Create a pre-ICS unofficial calendar event Intent.
     * 
     * @param context
     * @param event
     * @return
     */
    private static Intent getOldIntent(Context context, CalendarEvent event) {
        Intent intent = new Intent(Intent.ACTION_EDIT);   //Must be edit not insert.
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime", event.getStartTime());
        intent.putExtra("allDay", event.isAllDay()); 
        intent.putExtra("endTime", event.getEndTime());
        intent.putExtra("title", event.getTitle());
        intent.putExtra("availability", event.isBusy() ? 0 : 1);
        
        if (!TextUtils.isEmpty(event.getDescription())) {
            intent.putExtra("description", event.getDescription());
        }
        if (!TextUtils.isEmpty(event.getLocation())) {
            intent.putExtra("eventLocation", event.getLocation());
        }
        if (!TextUtils.isEmpty(event.getInvitees())) {
            intent.putExtra(Intent.EXTRA_EMAIL, event.getInvitees());
        }
        
        return intent;
    }
    
    /**
     * Calendar event object for use with generateIntent
     * 
     * @author adam.newman
     */
    public static class CalendarEvent {
        private long startTime;
        private long endTime;
        private String title;
        private String location;
        private String description;
        private boolean allDay;
        private String invitees;
        private boolean busy;
                
        /**
         * Create a new CalendarEvent object.
         * 
         * @param startTime Start time for the event in milliseconds UTC
         * @param endTime   End time for the event in milliseconds UTC
         * @param allDay    Should the event last all day
         * @param busy      Should the time be treated as busy
         * @param title     The "What" of the event.
         */
        public CalendarEvent (long startTime, long endTime, boolean allDay, boolean busy, String title) {
            this.startTime = startTime;
            this.endTime = endTime;
            this.title = title;
            this.allDay = allDay;
            this.setBusy(busy);
        }
        
        public long getStartTime() {
            return startTime;
        }

        public void setStartTime(long startTime) {
            this.startTime = startTime;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public boolean isAllDay() {
            return allDay;
        }

        public void setAllDay(boolean allDay) {
            this.allDay = allDay;
        }
        
        public String getInvitees() {
            return invitees;
        }

        /**
         * Set invitees for the event
         * @param invitees  comma separated list of invitee email addresses
         */
        public void setInvitees(String invitees) {
            this.invitees = invitees;
        }

        public boolean isBusy() {
            return busy;
        }

        public void setBusy(boolean busy) {
            this.busy = busy;
        }
    }

}
