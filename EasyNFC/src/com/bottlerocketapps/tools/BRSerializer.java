package com.bottlerocketapps.tools;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import android.graphics.Bitmap;
import android.util.Log;

public class BRSerializer {
    public static final String TAG = BRSerializer.class.getSimpleName();
    
    public static  byte[] serializeToByteArray(Object o) { 
        if (o == null) return null;
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream(); 

        try { 
            ObjectOutput out = new ObjectOutputStream(bos); 
            out.writeObject(o); 
            out.close(); 

            // Get the bytes of the serialized object 
            byte[] buf = bos.toByteArray(); 

            return buf; 
        } catch(IOException ioe) { 
            Log.e(TAG, "Unable to serialize: ", ioe);
            return null; 
        } 
    } 

    public static Object deserializeFromByteArray(byte[] b) { 
        if (b == null) return null;
        
        try { 
            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(b)); 
            Object object = in.readObject(); //will throw an exception if byte array doesn't deserialize to this object
            in.close(); 

            return object; 
        } catch(ClassNotFoundException cnfe) {
            Log.e(TAG, "Unable to deserialize: ", cnfe);
            return null; 
        } catch(IOException ioe) {
            Log.e(TAG, "Unable to deserialize: ", ioe);
            return null; 
        } 
    }
    
    public static byte[] encodeBitmapToJpegByteArray(Bitmap bmp) {
        if (bmp == null) return null;

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        // Compress to JPEG for sending
        bmp.compress(Bitmap.CompressFormat.JPEG, 95, bao);
        
        return bao.toByteArray();
    }
    
    public static byte[] encodeBitmapToPngByteArray(Bitmap bmp) {
        if (bmp == null) return null;

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        // Compress to PNG for sending. Quality is ignored.
        bmp.compress(Bitmap.CompressFormat.PNG, 100, bao);
        
        return bao.toByteArray();
    }
    
    /**
     * Serialize the specified file into a ByteArray.
     * 
     * @param filePath
     * @return file contents as byte[] or null
     */
    public static byte[] serializeFileToByteArray(String filePath) {
        if (filePath == null) return null;
        
        File file = new File(filePath);

        byte[] b = new byte[(int) file.length()];
        FileInputStream fileInputStream = null;        
        try {
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(b);            
            return b;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File not found: " + filePath, e);
        } catch (IOException e) {
            Log.e(TAG, "Error reading the file: " + filePath, e);            
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, "Exception closing file: " + filePath, e);
                }
            }
        }
        
        return null;
    }
}
