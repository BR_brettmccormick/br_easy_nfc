package com.bottlerocketapps.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

/**
 * Retain a configuration obtained from the web in a singleton obtained by calling getInstance().
 * Configuration is stored encrypted in a shared preferences file for persistence.
 * If the encryption seed parameter is not supplied to set and get config, then it is only really obfuscated.
 *  
 * @author adam.newman
 */
public class WebConfigurationManager {
    
    private static final String TAG = WebConfigurationManager.class.getSimpleName(); 
    
    private static final String PREFS_NAME = "WebConfig";
    private static final String PREF_CONFIG = "Config";    
    //A little bit of overkill since there will likely be a padding exception when decrypting with the wrong key. 
    private static final String PREF_KNOWN_CLEARTEXT = "KnownCleartext";    
    
    /**
     * Change this for each app. I recommend 63 random alphanumeric here: https://www.grc.com/passwords.htm
     * 
     */
    private static final String ENCRYPTION_SEED = "2IzuwPCkfkyJ97lrt4BqvyoRMZgCY46DWoTj4qjSiMTrceMMeb3GJok3oroNRn2";
    
    private WebConfiguration mConfig;
    
    /**
     * Private constructor to prevent normal instantiation
     */
    private WebConfigurationManager() {
        mConfig = null;
    }
  
    /**
     * SingletonHolder is loaded on the first execution of Singleton.getInstance() 
     * or the first access to SingletonHolder.INSTANCE, not before.
     */
    private static class SingletonHolder { 
        public static final WebConfigurationManager instance = new WebConfigurationManager();
    }
  
    /**
     * Get the instance or create it. (inherently thread safe Bill Pugh pattern)
     * @return
     */
    public static WebConfigurationManager getInstance() {
        return SingletonHolder.instance;
    }
    
    /**
     * Retrieve the configuration instance or load from shared preferences using the default insecure seed
     * 
     * @param context
     * @param configClass
     * @return
     */
    public WebConfiguration getConfiguration(Context context, Class<?> configClass) {
        return getConfiguration(context, configClass, ENCRYPTION_SEED);
    }
    
    /**
     * Retrieve the configuration instance or load from shared preferences using the supplied encryption seed
     * 
     * @param context
     * @param configClass
     * @param encryptionSeed
     * @return
     */
    public WebConfiguration getConfiguration(Context context, Class<?> configClass, String encryptionSeed) {
        if (mConfig != null) return mConfig;
        
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        try {
            mConfig = (WebConfiguration) configClass.newInstance();
            String encryptionCheck = StringEncryption.decrypt(encryptionSeed, settings.getString(PREF_KNOWN_CLEARTEXT, null));
            if (PREF_KNOWN_CLEARTEXT.equals(encryptionCheck)) {
                String decryptedData = StringEncryption.decrypt(encryptionSeed, settings.getString(PREF_CONFIG, null));
                if (decryptedData != null && mConfig.deserializeFromString(decryptedData)) {
                    return mConfig;
                } else {
                    Log.v(TAG, "Configuration was missing or invalid.");
                }
            } else {
                Log.v(TAG, "Encryption seed has been modified or no previous configuration existed.");
            }
        } catch (IllegalAccessException e) {
            Log.e(TAG, "Failed to deserialize configuration", e);            
        } catch (InstantiationException e) {
            Log.e(TAG, "Failed to deserialize configuration", e);
        }
        return null;
    }
    
    /**
     * Store the configuration instance to shared preferences using the default insecure seed.
     * 
     * @param context
     * @param config Configuration object to be stored. Use null to clear configuration.
     */
    public void setConfiguration(Context context, WebConfiguration config) {
        setConfiguration(context, config, ENCRYPTION_SEED);
    }
    
    /**
     * Store the configuration instance to shared preferences using the supplied encryption seed.
     * 
     * @param context
     * @param config
     * @param encryptionSeed
     */
    public void setConfiguration(Context context, WebConfiguration config, String encryptionSeed) {
        mConfig = config;
        
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        Editor editor = settings.edit();        
        String encryptedData = null;
        
        if (config != null) {
            encryptedData = StringEncryption.encrypt(encryptionSeed, config.serializeToString());
        }
        
        if (encryptedData != null) {
            editor.putString(PREF_KNOWN_CLEARTEXT, StringEncryption.encrypt(encryptionSeed, PREF_KNOWN_CLEARTEXT));
            editor.putString(PREF_CONFIG, encryptedData);                        
        } else {
            editor.remove(PREF_CONFIG);
        }
        editor.commit();
    }
    
    /**
     * The custom configuration object must implement this interface. 
     * It also must have a nullary constructor (no arguments) for Class.newInstance() to work in getConfiguration
     * @author adam.newman
     */
    public interface WebConfiguration {
        /**
         * Use the string feed to populate the object must be compatible with format of serializeToString()<br/><br/>
         * <strong>Note: Must handle a null feed.</strong>
         * @param feed  String representation of the object
         * @return      Boolean success
         */
        public boolean deserializeFromString(String feed);
        
        /**
         * Convert the object to a string representation JSON/XML/Base64 encoded string of a map in bytes?/Whatever 
         * must be compatible with format of deserializeFromString()
         * @return
         */
        public String serializeToString();        
    }
}
